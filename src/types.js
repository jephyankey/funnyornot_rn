
import { Platform } from 'react-native';

export const HEADER_HEIGHT = Platform.OS === 'android' ? 50 : 40;
export const IOS_STATUS_BAR_HEIGHT = 20;
export const TRENDING_REFRESH = 'trending_refresh';
export const APP_TO_BACKGROUND = 'to_background';
export const APP_TO_FOREGROUND = 'to_foreground';
export const TAB_OR_VIEW_CHANGED = 'tab_changed';

//ERROR TYPES
export const CONTACTS_PERMISSION_DENIED = 'permission_denied';
export const FAIL_TO_READ_CONTACTS = 'failed_to_read_contacts';
export const NO_INTERNET = 'no_internet';
export const NO_INTERNET2 = 'no_internet2'; 
export const LOAD_FAIL = 'load_fail';
export const EMPTY_RESULT = 'empty_result';
export const NO_COMMENTS = 'no_comments';
export const NO_CONTACTS_ON_APP = 'no_contacts_on_app';
export const NO_FOLLOWING_ON_APP = 'no_following_on_app';
export const NO_FOLLOWERS_ON_APP = 'no_followers_on_app';
export const NO_CONTACTS_TO_INVITE = 'no_contacts_to_invite';
export const NO_ONE_DURING_SEARCH = 'no_one_during_search';


//SETTINGS VALUES
//remember that setting value must match server route value
export const PRIVATE_DP_SETTING = 'dpscope';
export const INVISIBILITY_SETTING = 'visibility';
export const NOTIFICATION_SETTING = 'notifications';
//these settings do not have a server side component (for now at least)
export const APP_VERSION = 'app_verions';
export const AFFILIATE_COUNTRIES = 'user_affiliate_countries';
export const USER_COUNTRY = 'user_country';
export const USER_ID = 'user_id';
export const USER_NAME = 'user_name';
export const USER_PHONE = 'user_phone';
export const USER_AVATAR = 'avatar';
export const PREVIOUSLY_RUN = 'previously_run';
export const THEME = 'theme';
export const DEVICE_ID = 'device_id';

//EVENTS
export const SHOW_TOAST = 'show_toast';
export const HIDE_TOAST = 'hide_toast';

//generic success and fail constants that can be used with all evetns
export const SUCCESS = 'success';
export const FAIL = 'fail';

export const BLOCK_ACTION = 'block_action'; //used for when a user blocks/unblocks
export const FOLLOW_ACTION = 'follow_action'; //follows or unfollows
export const BLOCKED = 'blocked';
export const UNBLOCKED = 'unblocked';
export const FOLLOWED = 'followed';
export const UNFOLLOWED = 'unfollowed';
export const COMMENT_SUBMITTED = 'comment_submitted';
export const NEW_POST_SUBMITTED = 'new_post_submitted';
export const POST_SUBMISSION_COMPLETED = 'submission_completion';
export const NEW_AVATAR_UPLOADED = 'new_avatar_uploaded';
export const NEW_THEME_CHOSEN = 'new_theme_chosen';
export const INADEQUATE_FEED = 'feed_inadequate';

export const VIDEO_JUST_STARTED_PLAYING = 'video_started_playing';
export const SCROLLING_THROUGH_FEED = 'scrolling_tru_feed';
export const REMOVE_ALL_VIDEOS = 'remove_all_videos';
export const STOP_ALL_VIDEOS = 'external_player_launched';
export const PARALLAX_SCROLL = 'paralllax_scroll';

//possible values for Feed component
export const HOME = 'home';
export const FAVOURITES = 'favourites';
export const ALLTIME = 'alltime';
export const EXPLORE = 'explore';
export const TRENDING = 'trending'; //trending feature has been removed for now
export const PROFILE = 'profile';

//possible values for the diffrent types of recommendations
export const RECOMMENDED_FOLLOWS = 'follows';
export const RECOMMENDED_JOKES = 'recommended_jokes';

//some general values i cant seem to fit anywhere
export const UP_DIRECTION = 'up';
export const DOWN_DIRECTON = 'down';
