import React, { Component } from 'react';
import {
    View, Dimensions, StyleSheet, AsyncStorage, StatusBar,
    TouchableNativeFeedback, Animated, Keyboard, Platform, TouchableHighlight,
    TouchableOpacity
}
    from 'react-native';
import { TabViewAnimated, TabViewPagerScroll } from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/Ionicons';

import { connect } from 'react-redux';
import { loadAllSettings } from './actions';

import { Properties } from './common';

import HomePage from './pages/tabs/Home';
import ExplorePage from './pages/tabs/Explore';
import TabProfilePage from './pages/tabs/TabProfile';
import PostPage from './pages/tabs/Post';
import MenuPage from './pages/tabs/Menu';

import {
    USER_ID, USER_NAME, USER_PHONE, USER_COUNTRY, AFFILIATE_COUNTRIES, USER_AVATAR,
    //new events
    NEW_THEME_CHOSEN, TAB_OR_VIEW_CHANGED

} from './types';
import { EventEmitter } from './SERVICES';


const AnimatedIcon = Animated.createAnimatedComponent(Icon);


class MyTabBar extends Component {

    static appbarElevation = 0;

    static propTypes = {
        style: View.propTypes.style,
    };

    state = {

        index: 0,
        routes: [
            { key: '0', icon: Platform.OS === 'android' ? 'md-home' : 'ios-home-outline' },
            { key: '1', icon: Platform.OS === 'android' ? 'md-globe' : 'ios-globe-outline' },
            { key: '2', icon: Platform.OS === 'android' ? 'ios-create' : 'ios-create-outline' },
            { key: '3', icon: Platform.OS === 'android' ? 'ios-contact' : 'ios-contact-outline' },
            { key: '4', icon: Platform.OS === 'android' ? 'md-menu' : 'ios-menu-outline' },
        ],
        tab_height: Platform.OS === 'ios' ? 50 : 45, //set this color when user chooses a new theme
        top_width: 1,
        // post_keyboard_shown: false
    };

    componentDidMount() {
        //which type of touchable to use for the tabbar??
        if (Platform.OS === 'android') {
            if (Platform.Version <= 19) //android level 4.4 and below
                this.TOUCHTYPE = 'HIGHLIGHT';
            else
                this.TOUCHTYPE = 'NATIVE';
        }
        else {
            //ios
            this.TOUCHTYPE = 'OPACITY';
        }

        //LOAD ALL SETTINGS AT THIS POINT
        AsyncStorage.multiGet([USER_ID, USER_NAME, USER_PHONE, USER_COUNTRY, AFFILIATE_COUNTRIES, USER_AVATAR])
            .then(settings => {
                this.props.loadAllSettings(settings);
            });
        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, (new_color) => {
            if (Platform.OS === 'android')
                StatusBar.setBackgroundColor(new_color, true);
            this.setState({ new_color });
        });

        // this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.showingKeyboard.bind(this));
        // this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.hidingKeyboard.bind(this));
    }

    handleChangeTab(index) {
        this.setState({
            index,
        });

        //when tabs change, pause all VIDEOS
        EventEmitter.dispatch(TAB_OR_VIEW_CHANGED);

        if (Platform.OS === 'ios') {
            if (index === 3)
                StatusBar.setBarStyle('light-content');
            else
                StatusBar.setBarStyle('dark-content');
        }

        //dismiss the keyboard, (sometimes when u show the keyboard on the post page but fail to hide it before )
        //moving to another page
        Keyboard.dismiss();
    }


    renderIcon({ position, navigationState }) {
        return ({ route, index }) => {
            const inputRange = navigationState.routes.map((x, i) => i);
            const filledOpacity = position.interpolate({
                inputRange,
                outputRange: inputRange.map(i => i === index ? 1 : 0),
            });
            const outlineOpacity = position.interpolate({
                inputRange,
                outputRange: inputRange.map(i => i === index ? 0 : 1),
            });
            return (
                <View style={styles.iconContainer}>
                    <AnimatedIcon
                        name={route.icon}
                        size={Platform.OS === 'ios' ? 30 : 27}
                        color={Properties.themeColor}
                        style={[styles.icon, { opacity: filledOpacity }]}
                    />
                    <AnimatedIcon
                        name={route.icon}
                        size={Platform.OS === 'ios' ? 30 : 27}
                        color={'rgb(204, 204, 204)'}
                        style={[styles.icon, { opacity: outlineOpacity }]}
                    />
                </View>
            );
        };
    }


    renderFooter(props) {
        return (
            <View style={[styles.tabbar, { height: this.state.tab_height }, { borderTopWidth: this.state.top_width }]}>
                {props.navigationState.routes.map((route, index) => {
                    return this.renderTabBar(props, route, index);
                })}
            </View>);
    }

    renderTabBar(props, route, index) {
        if (this.TOUCHTYPE === 'HIGHLIGHT')
            return (
                <TouchableHighlight underlayColor={'rgba(214,214,214,0.3)'} key={route.key} onPress={() => props.jumpToIndex(index)} >
                    <Animated.View style={styles.tab}>
                        {this.renderIcon(props)({ route, index })}
                    </Animated.View>
                </TouchableHighlight>);

        else if (this.TOUCHTYPE === 'NATIVE')
            return (
                <TouchableNativeFeedback key={route.key} onPress={() => props.jumpToIndex(index)} >
                    <Animated.View style={styles.tab}>
                        {this.renderIcon(props)({ route, index })}
                    </Animated.View>
                </TouchableNativeFeedback>);
        return (
            <TouchableOpacity key={route.key} onPress={() => props.jumpToIndex(index)} >
                <Animated.View style={styles.tab}>
                    {this.renderIcon(props)({ route, index })}
                </Animated.View>
            </TouchableOpacity>
        );
    }


    renderScene({ route }) {
        switch (route.key) {
            case '0':
                return <HomePage />;
            case '1':
                return <ExplorePage />;
            case '2':
                return <PostPage />;
            case '3':
                return <TabProfilePage />;
            case '4':
                return <MenuPage />;
            default:
                return null;
        }
    }

    _renderPager = (props) => {
        switch (Platform.OS) {
            case 'ios':
                return (
                    <TabViewPagerScroll
                        {...props}
                        animationEnabled={false}
                        swipeEnabled={false}
                    />
                );
            default:
        }
    };

    render() {
        if (Platform.OS === 'android') {
            return (
                <TabViewAnimated
                    style={[styles.container, this.props.style, { backgroundColor: 'transparent' }]}
                    navigationState={this.state}
                    renderScene={this.renderScene.bind(this)}
                    renderFooter={this.renderFooter.bind(this)}
                    onRequestChangeTab={this.handleChangeTab.bind(this)}
                    initialLayout={initialLayout}
                />
            );
        }
        return (
            <TabViewAnimated
                style={[styles.container, this.props.style, { backgroundColor: 'transparent' }]}
                configureTransition={() => { return null }}
                navigationState={this.state}
                renderScene={this.renderScene.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                onRequestChangeTab={this.handleChangeTab.bind(this)}
                initialLayout={initialLayout}
                renderPager={this._renderPager}
            />
        );
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
        // this.keyboardDidShowListener.remove();
        // this.keyboardDidHideListener.remove();
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    tabbar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        elevation: 0,
        borderTopColor: '#eee',
        backgroundColor: 'rgba(255, 255, 255, 0.98)',
        paddingTop: 0,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },
    tab: {
        justifyContent: 'center',
        paddingHorizontal: 5
    },
    iconContainer: {
        height: Platform.OS === 'ios' ? 50 : 45,
        width: Platform.OS === 'ios' ? 50 : 45,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        position: 'absolute',
        textAlign: 'center',
        left: 0,
        right: 0,
        top: 10, 
        bottom: 10
    },
});

const initialLayout = {
    height: 0,
    width: Dimensions.get('window').width,
};


export default connect(null, { loadAllSettings })(MyTabBar);
