import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { AsyncStorage, StatusBar, Platform, Text } from 'react-native';
import { Properties } from './common';
import { THEME, PREVIOUSLY_RUN } from './types';


//----------testing new video elemtnen
import MyVideoPlayer from './nativecomponents/VideoPlayer';
//------------------------------------

class Landing extends Component {

    componentWillMount() {
        AsyncStorage.multiGet([PREVIOUSLY_RUN, THEME])
            .then(data => {
                Properties.mainColor = data[1][1];
                if (Platform.OS === 'android') {
                    StatusBar.setBackgroundColor(Properties.statusBarColor, true);
                }
                if (data[0][1]) {
                    Actions.tabbar({ type: 'replace', color: Properties.themeColor });
                }
                else {
                    Actions.initialSetup({ type: 'replace' });
                }
            });
    }


    render() {
        return null;
        // return <MyVideoPlayer source={'file:/storage/emulated/0/funnyornot/1487183641_1_silhouette.mp4'} width={200} height={200} />;
    }
}

export default Landing;
