import { 
    LOADING_FEED_DATA,
    FEED_DATA_RECEIVED,
    REPLACE_FEED_DATA,
    ADD_NEW_POST
} from './types';


//looks like i dont even use this action at all?????
export const updateLoaderState = ({ prop, value }) => {
    return {
        type: LOADING_FEED_DATA,
        payload: { prop, value }
    };
};

export const updateFeedData = (data, type) => {
    return {
        type: FEED_DATA_RECEIVED,
        payload: { data, type }
    };
};

export const replaceFeedData = (data, type) => {
    return {
        type: REPLACE_FEED_DATA,
        payload: { data, type }
    };
};


export const addNewPost = (data) => {
    return {
        type: ADD_NEW_POST,
        payload: data
    };
};
