export const COUNTRY_DETECTED = 'country_detected';
export const USER_INFO_CHANGED = 'user_info_changed';
export const FETCHING_FEED_DATA = 'fetching_feed_data'; //for initial data load
export const LOADING_FEED_DATA = 'loading_feed_data'; //for subsequent data loads
export const FEED_DATA_RECEIVED = 'received_feed_data';
export const REPLACE_FEED_DATA = 'replace_feed_data';
export const USERNAME_VERIFIED = 'username_verified';
export const ADD_NEW_POST = 'add_new_post';

//SETTINGS
export const LOAD_ALL_SETTINGS = 'load_all_settings';
export const UPDATE_AFFILIATE_COUNTRIES = 'update_affiliate_countries';
export const SET_AFFILIATE_COUNTRIES = 'set_affiliate_countries';
export const ADD_TO_AFFILIATES = 'add_to_affiliates';
export const REMOVE_FROM_AFFILIATES = 'remove_from_affiliates';
export const CHANGE_USERS_COUNTRY = 'change_users_country';
