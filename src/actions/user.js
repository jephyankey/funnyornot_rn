
import { 
    COUNTRY_DETECTED,
    USER_INFO_CHANGED,
    USERNAME_VERIFIED,
} from './types';


export const countryDetected = (data) => {
    return {
        type: COUNTRY_DETECTED,
        payload: data
    };
};

export const changeCountry = (countries) => {
    return {
        type: COUNTRY_DETECTED,
        payload: { ...countries }
    };
};

export const updateUserInfo = ({ prop, value }) => {
    return {
        type: USER_INFO_CHANGED,
        payload: { prop, value }
    };
};

export const usernameVerified = (user_id, existing_user) => {
    return {
        type: USERNAME_VERIFIED,
        payload: { user_id, existing_user }
    };
};



