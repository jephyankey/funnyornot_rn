import { 
    UPDATE_AFFILIATE_COUNTRIES,
    SET_AFFILIATE_COUNTRIES, 
    LOAD_ALL_SETTINGS,
    CHANGE_USERS_COUNTRY
} from './types';


export const loadAllSettings = (settings: Array) => {
    return {
        type: LOAD_ALL_SETTINGS,
        payload: settings
    };
};

export const updateAffiliateCountries = (country, what) => {
    return {
        type: UPDATE_AFFILIATE_COUNTRIES,
        payload: { country, what }
    };
};

export const setAffiliateCountries = (countries) => {
    return {
        type: SET_AFFILIATE_COUNTRIES,
        payload: countries
    };
};

/**
 * used to set new country in SETTINGS
 */
export const changeUsersCountry = (country_code) => {
    return {
        type: CHANGE_USERS_COUNTRY,
        payload: country_code
    };
};
