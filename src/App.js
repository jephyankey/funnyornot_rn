import React, { Component, PropTypes } from 'react';
import { Platform } from 'react-native';
import codePush from 'react-native-code-push';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import OneSignal from 'react-native-onesignal';
import ActionSheet from '@expo/react-native-action-sheet';

import { MyRouter } from './Router';
import allReducers from './reducers';

import { Toast } from './pages/base';

import { InitService, Config, Utilities, DatabaseService } from './SERVICES';



export default class App extends Component {

  static childContextTypes = {
    actionSheet: PropTypes.func,
  };

  componentDidMount() {
    const config = new Config();
    const utilities = new Utilities();
    const dbService = new DatabaseService(utilities);
    this.initService = new InitService(config, utilities, dbService);

    codePush.sync({ installMode: codePush.InstallMode.ON_NEXT_RESUME });

    //CONFIGURE PUSH NOTIFICATIONS, IF THEY'VE NOT BEEN DONE
    OneSignal.addEventListener('ids', this._pushNot.bind(this));
  }
  _pushNot(device_info) {
    this.initService.configurePushNotifications(device_info);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('ids', this._pushNot);
  }

  getChildContext() {
    return {
      actionSheet: () => this.actionSheetRef,
    };
  }

  _render() {
    const store = createStore(allReducers, {}, applyMiddleware(ReduxThunk));

    if (Platform.OS === 'ios') {
      return (
        <ActionSheet ref={component => this.actionSheetRef = component}>
          <Toast>
            <Provider store={store} >
              <MyRouter />
            </Provider>
          </Toast>
        </ActionSheet>
      );
    }
    return (
      <ActionSheet ref={component => this.actionSheetRef = component}>
        <Provider store={store} >
          <MyRouter />
        </Provider>
      </ActionSheet>
    );
  }

  render() {
    return this._render();
  }

}
