import React, { Component } from 'react';
import { AsyncStorage, StatusBar, Platform, } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import { connect } from 'react-redux';
import { loadAllSettings } from './actions';

import HomePage from './pages/tabs/Home';
import ExplorePage from './pages/tabs/Explore';
import TabProfilePage from './pages/tabs/TabProfile';
import PostPage from './pages/tabs/Post';
import MenuPage from './pages/tabs/Menu';
import TabBar from './TabBar';

import {
    USER_ID, USER_NAME, USER_PHONE, USER_COUNTRY, AFFILIATE_COUNTRIES, USER_AVATAR,
    //new events
    NEW_THEME_CHOSEN, REMOVE_ALL_VIDEOS
} from './types';
import { EventEmitter } from './SERVICES';



class Tabs extends Component {

    componentDidMount() {
        //LOAD ALL SETTINGS AT THIS POINT
        AsyncStorage.multiGet([USER_ID, USER_NAME, USER_PHONE, USER_COUNTRY, AFFILIATE_COUNTRIES, USER_AVATAR])
            .then(settings => {
                this.props.loadAllSettings(settings);
            });
        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, (new_color) => {
            if (Platform.OS === 'android')
                StatusBar.setBackgroundColor(new_color, true);
            this.setState({ new_color });
        });
    }

    //TODO: find an alternative for new tabs system
    handleChangeTab(index) {
        //when tabs change, pause all VIDEOS
        EventEmitter.dispatch(REMOVE_ALL_VIDEOS, null);
        this.setState({
            index,
        });
    }


    render() {
        return (
            <ScrollableTabView
                renderTabBar={() =>
                    <TabBar tabs={['md-home', 'md-globe', 'ios-post', 'ios-contact', 'md-menu']} />
                }
                tabBarPosition='overlayBottom'
            >
                <HomePage tabLabel="md-home" />
                <ExplorePage tabLabel="md-globe" />
                <PostPage tabLabel="ios-create" />
                <TabProfilePage tabLabel="ios-contact" />
                <MenuPage tabLabel="md-menu" />
            </ScrollableTabView>
        );
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
    }
}



export default connect(null, { loadAllSettings })(Tabs);
