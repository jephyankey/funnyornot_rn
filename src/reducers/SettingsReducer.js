import { Reducer } from 'redux';
import { SettingsStore } from './interfaces';
import {
    LOAD_ALL_SETTINGS,
    UPDATE_AFFILIATE_COUNTRIES,
    SET_AFFILIATE_COUNTRIES,
    ADD_TO_AFFILIATES,
    REMOVE_FROM_AFFILIATES,
    CHANGE_USERS_COUNTRY
} from '../actions/types';

//this is the initial state used for the initial setup,
//i.e. first time user launches app
const INITIAL_STATE: SettingsStore = {
    user_id: '',
    user_name: '',
    user_phone: '',
    user_country: '',
    affiliate_countries: [],
    user_avtar: '',
    theme: '' //currently not used here
};


//take a state and action and return a new state
export default function (state: SettingsStore = INITIAL_STATE, action): Reducer {
    switch (action.type) {

        case LOAD_ALL_SETTINGS:
            return {
                ...state,
                user_id: action.payload[0][1],
                user_name: action.payload[1][1],
                user_phone: action.payload[2][1],
                user_country: action.payload[3][1],
                affiliate_countries: action.payload[4][1].split(','),
                user_avatar: action.payload[5][1]
            };
        
        case CHANGE_USERS_COUNTRY:
            return {
                ...state,
                user_country: action.payload
            };

        case SET_AFFILIATE_COUNTRIES:
            return {
                ...state,
                affiliate_countries: action.payload
            };

        case UPDATE_AFFILIATE_COUNTRIES:
            if (action.payload.what === ADD_TO_AFFILIATES) {
                //TODO MUTATING, NOT GOOD FOR REDUX PATTERN
                state.affiliate_countries.push(action.payload.country);
                return {
                    ...state,
                    affiliate_countries: state.affiliate_countries
                };
            }
            else if (action.payload.what === REMOVE_FROM_AFFILIATES) {
                //TODO MUTATING, NOT GOOD FOR REDUX PATTERN
                const delete_index = state.affiliate_countries.indexOf(action.payload.country);
                state.affiliate_countries.splice(delete_index, 1);
                return {
                    ...state,
                    affiliate_countries: state.affiliate_countries
                };
            }
            break;

        default:
            return state;
    }
}
