import { Reducer } from 'redux';
import { FeedStore } from './interfaces';
import {
    LOADING_FEED_DATA,
    FEED_DATA_RECEIVED,
    REPLACE_FEED_DATA,
    ADD_NEW_POST
} from '../actions/types';


import { HOME, EXPLORE, FAVOURITES, ALLTIME } from '../types';

//this is the initial state used for the initial setup,
//i.e. first time user launches app
const INITIAL_STATE: FeedStore = {
    home_feed_data: [],
    explore_feed_data: [],
    alltime_feed_data: [],
    favourites_data: [],
    loading_home_data: true,
    loading_explore_data: true,
    loading_alltime_data: true,
    loading_favourites_data: true
};


//take a state and action and return a new state
export default function (state: FeedStore = INITIAL_STATE, action): Reducer {
    let a;
    switch (action.type) {

        case LOADING_FEED_DATA:
            return {
                ...state,
                [action.payload.prop]: action.payload.value
            };

        case FEED_DATA_RECEIVED:
            //same case for home, explore, alltime
            if (action.payload.type === HOME)
               return {
                    ...state,
                    home_feed_data: state.home_feed_data.concat(action.payload.data),
                    //if we have received data, then set all loading indicators to false
                    loading_home_data: false,
                };
            else if (action.payload.type === EXPLORE)
                return {
                    ...state,
                    explore_feed_data: state.explore_feed_data.concat(action.payload.data),
                    //if we have received data, then set all loading indicators to false
                    loading_explore_data: false,
                };
            else if (action.payload.type === ALLTIME)
                return {
                    ...state,
                    alltime_feed_data: state.alltime_feed_data.concat(action.payload.data),
                    //if we have received data, then set all loading indicators to false
                    loading_alltime_data: false,
                };
            else if (action.payload.type === FAVOURITES)
                return {
                    ...state,
                    favourites_data: state.favourites_data.concat(action.payload.data),
                    //if we have received data, then set all loading indicators to false
                    loading_favourites_data: false,
                };
            break;

        case REPLACE_FEED_DATA:
            if (action.payload.type === HOME)
                return {
                    ...state,
                    home_feed_data: action.payload.data,
                    loading_home_data: false,
                };
            else if (action.payload.type === EXPLORE)
                return {
                    ...state,
                    explore_feed_data: action.payload.data,
                    loading_explore_data: false,
                };
            else if (action.payload.type === ALLTIME)
                return {
                    ...state,
                    alltime_feed_data: action.payload.data,
                    loading_alltime_data: false,
                };
            else if (action.payload.type === FAVOURITES)
                return {
                    ...state,
                    favourites_data: action.payload.data,
                    loading_favourites_data: false,
                };
            break;

        case ADD_NEW_POST:
            a = state.home_feed_data.slice(0);
            a.push(action.payload);
            return {
                ...state,
                home_feed_data: a //[...[action.payload], ...state.home_feed_data]
            };

        default:
            return state;
    }
}
