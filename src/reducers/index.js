import { combineReducers } from 'redux';
import UserReducer from './UserReducer';
import FeedReducer from './FeedReducer';
import SettingsReducer from './SettingsReducer';


export default combineReducers({
    user: UserReducer,
    feed: FeedReducer,
    settings: SettingsReducer
});
