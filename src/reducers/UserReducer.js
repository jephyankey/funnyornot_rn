import { Reducer } from 'redux';
import { UserStore } from './interfaces';
/**
 * reducer contains data relevant to current user
 * generally also used during FIRST TIME SETUP OF APPLICATION
 */
import { 
    COUNTRY_DETECTED,
    USER_INFO_CHANGED,
    USERNAME_VERIFIED
} from '../actions/types';



//this is the initial state used for the initial setup,
//i.e. first time user launches app
const INITIAL_STATE: UserStore = {
    user_id: null,
    affiliate_countries: '',
    country_code: 'US',
    country: 'United States',
    phone_number: '',
    phone_code: '+1',
    phone: '',
    phone_verified: false,
    email: '',
    email_verified: false,
    username: '',
    checking_username: false,
    typed_v_code: null,
    actual_v_code: null,
    verifying_username: false,
    initializing_msg: 'Getting a few things ready...',
    existing_user: false
};

//take a state and action and return a new state
export default function (state:UserStore = INITIAL_STATE, action) : Reducer {
    switch (action.type) {
        case COUNTRY_DETECTED:
            return { 
                ...state,
                country: action.payload.country, 
                country_code: action.payload.country_code,
                phone_code: action.payload.phone_code
            };
        case USER_INFO_CHANGED:
            return {
                ...state,
                [action.payload.prop]: action.payload.value
            };
        case USERNAME_VERIFIED:
            return {
                ...state,
                user_id: action.payload.user_id,
                existing_user: action.payload.existing_user,
                verifying_username: false
            };
        default:
            return state;
    }
}
