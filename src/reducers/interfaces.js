/**
 * this file defines types that make it easier to work with reducers
 * and state and the like
 * for each new reducer, define a store interface for it,
 * and then add that interface to the CombinedStore
 */
export interface FeedStore {
    home_feed_data: Array<{}>,
    explore_feed_data: Array<{}>,
    alltime_feed_data: Array<{}>,
    favourites_data: Array<{}>,
    loading_home_data: boolean,
    loading_explore_data: boolean,
    loading_alltime_data: boolean,
    loading_favourites_data: boolean
}


export interface UserStore {
    user_id: string,
    affiliate_countries: string,
    country_code: string,
    country: string,
    phone_number: string,
    phone_code: string,
    phone: string,
    phone_verified: boolean,
    email: string,
    email_verified: boolean,
    username: string,
    checking_username: boolean,
    typed_v_code: string,
    actual_v_code: string,
    verifying_username: boolean,
    initializing_msg: string
}

export interface SettingsStore{
    user_id: string,
    user_name: string,
    user_phone: string,
    user_country: string,
    affiliate_countries: [],
    user_avatar: string,
}

export interface CombinedStore {
    user: UserStore,
    feed: FeedStore,
    settings: SettingsStore,
}
