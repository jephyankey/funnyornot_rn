import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import Landing from './Landing';

import SettingUp from './pages/firstSetup/SettingUp';
import EnterPhone from './pages/firstSetup/EnterPhone';
import CountryList from './pages/firstSetup/CountryList';
import EnterVCode from './pages/firstSetup/EnterVCode';
import EnterUsername from './pages/firstSetup/EnterUsername';
import Initializing from './pages/firstSetup/Initializing';


import MyTabBar from './TabRoutes';
// import MyTabBar from './Tabs';

import CommentsPage from './pages/subpages/Comments';
import ProfilePage from './pages/subpages/Profile';
import SettingsPage from './pages/subpages/Settings';
import ContactsPage from './pages/subpages/Contacts';
import FollowingPage from './pages/subpages/Following';
import FollowersPage from './pages/subpages/Followers';
import AlltimePage from './pages/subpages/AllTime';
import MediaPage from './pages/subpages/Media';
import FavouritesPage from './pages/subpages/Favourites';

//settings
import UsernameSettingsPage from './pages/settings/Username';
import FeedbackPage from './pages/settings/Feedback';
import ThemesPage from './pages/settings/Themes';
import WebPage from './pages/settings/WebPage';


export class MyRouter extends Component {
    
    render() {
        // this.first = true;
        const { general, navbarTitle } = styles;

        return (
            <Router sceneStyle={general} titleStyle={navbarTitle} >
                <Scene key="landing" hideNavBar initial component={Landing} />
                <Scene key="initialSetup" hideNavBar>
                    <Scene key="settingUp" component={SettingUp} />
                    <Scene key="enterPhone" component={EnterPhone} />
                    <Scene key="countryList" component={CountryList} />
                    <Scene key="enterVCode" component={EnterVCode} />
                    <Scene key="enterUsername" component={EnterUsername} />
                    <Scene key="initializing" component={Initializing} />
                </Scene>
                <Scene key="tabbar">
                    <Scene
                        hideNavBar
                        key="home"
                        component={MyTabBar}
                        />
                </Scene>
                {/**MENU */}
                <Scene key="commentsPage" hideNavBar component={CommentsPage} />
                <Scene key="profilePage" hideNavBar component={ProfilePage} />
                <Scene key="settingsPage" hideNavBar component={SettingsPage} />
                <Scene key="contactsPage" hideNavBar component={ContactsPage} />
                <Scene key="followingPage" hideNavBar component={FollowingPage} />
                <Scene key="followersPage" hideNavBar component={FollowersPage} />
                <Scene key="alltimePage" hideNavBar component={AlltimePage} />
                <Scene key="mediaPage" hideNavBar component={MediaPage} />
                <Scene key="themesPage" hideNavBar component={ThemesPage} />
                {/**SETTINGS */}
                <Scene key="usernameSettingsPage" hideNavBar component={UsernameSettingsPage} />
                <Scene key="feedbackPage" hideNavBar component={FeedbackPage} />
                <Scene key="favouritesPage" hideNavBar component={FavouritesPage} />
                {/** about page sub pages */}
                <Scene key="webPage" hideNavBar component={WebPage} />
            </Router>
        );
    }

}


const styles = {
    general: {
        backgroundColor: 'white'
    },
    navbarTitle: {
        marginTop: 5,
        fontSize: 20,
        color: 'white',
        fontWeight: '500',
        alignSelf: 'center',
        justifyContent: 'center',
        textAlign: 'left',
        paddingLeft: 20,
    }
};
