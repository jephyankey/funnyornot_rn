import React, { Component, PropTypes } from 'react';
import { Platform, View, TouchableOpacity, TouchableNativeFeedback, TouchableHighlight } from 'react-native';
import { Properties } from './common';
import Icon from 'react-native-vector-icons/Ionicons';


const propTypes = {
    selected: PropTypes.bool,
    sceneKey: PropTypes.string
};


class TabIcon extends Component {
    state = {
        index: 0,
        routes: [
            { key: '0', icon: Platform.OS === 'android' ? 'md-home' : 'ios-home-outline' },
            { key: '1', icon: Platform.OS === 'android' ? 'md-globe' : 'ios-globe-outline' },
            { key: '2', icon: Platform.OS === 'android' ? 'ios-create' : 'ios-create-outline' },
            { key: '3', icon: Platform.OS === 'android' ? 'ios-contact' : 'ios-contact-outline' },
            { key: '4', icon: Platform.OS === 'android' ? 'md-menu' : 'ios-menu-outline' },
        ]
    }

    componentDidMount() {
        //which type of touchable to use for the tabbar??
        if (Platform.OS === 'android') {
            if (Platform.Version <= 19) //android level 4.4 and below
                this.TOUCHTYPE = 'HIGHLIGHT';
            else
                this.TOUCHTYPE = 'NATIVE';
        }
        else {
            //ios
            this.TOUCHTYPE = 'OPACITY';
        }
    }


    renderIcon() {
        return (
            <View>
                <Icon
                    name={this.state.routes[this.props.sceneKey].icon}
                    color={this.props.selected ? Properties.themeColor : '#aaa'}
                    size={Platform.OS === 'ios' ? 30 : 27}
                />
            </View>
        );
    }

    render() {
        return (
            <View>
                <Icon
                    name={this.state.routes[this.props.sceneKey].icon}
                    color={this.props.selected ? Properties.themeColor : '#aaa'}
                    size={Platform.OS === 'ios' ? 30 : 27}
                />
            </View>
        );
    }
}

    TabIcon.propTypes = propTypes;


    const styles = {
        tab: {
            height: Platform.OS === 'ios' ? 50 : 45,
            width: Platform.OS === 'ios' ? 50 : 45,
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: 5
        },
    }


    export default TabIcon;