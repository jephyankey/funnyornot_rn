
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  View, TextInput, ScrollView, TouchableOpacity,
  Animated, Easing, Keyboard, Dimensions, Platform
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon from '../../common/Icon';

import { addNewPost } from '../../actions';

import {
  MainContent, Button, Spinner, SpinnerWithContainerIos, MyText, Properties, StatusBarIos
} from '../../common';
import { FeedCard, FeedCardImage, FeedCardVideo } from '../base';
import {
  FileService, Config, Utilities, PostService,
  DatabaseService, UserService, ContactService, EventEmitter
} from '../../SERVICES';
import {
  //event results
  SUCCESS, FAIL, IOS_STATUS_BAR_HEIGHT, NEW_THEME_CHOSEN
} from '../../types';


const FULL_KEYBOARD_H = 0.8 * Dimensions.get('window').height;
const PARTIAL_KEYBOARD_H = 0.5 * Dimensions.get('window').height;



class PostPage extends Component {

  attachment_options_shown: boolean = false;

  state = {
    txt_input_height: FULL_KEYBOARD_H,
    height: 0,
    text: '',
    media_url: null,
    image_h: null,
    image_w: null,
    submit_disabled: true,
    show_remove_button: false,
    new_post_metadata: null,
    new_post_media_type: 't',
    new_post_data: null,
    temp_media_data: null, //this is a temporary object that stores data used to 
    //render the image/video view even before the preview//
    //which comes after posting
    show_preview: false,
    show_keyboard_dismiser: false
  };

  constructor(props) {
    super(props);
    this.config = new Config();
    this.utilities = new Utilities();
    this.dbService = new DatabaseService(this.utilities);
    this.fileService = new FileService(this.config, this.utilities);
    this.userService = new UserService(this.config, this.utilities, this.dbService);
    this.contactService = new ContactService(this.config, this.utilities, this.dbService);
    this.postService = new PostService(this.config, this.utilities, this.userService, this.fileService, this.contactService);


    //animations of attachment        
    this.animated_attachments_value = new Animated.Value(0);
    this.animated_input_value = new Animated.Value(0);
    this.slide_attachments_in = Animated.spring(
      this.animated_attachments_value,
      {
        toValue: 0.5,
        duration: 150,
        friction: 5,
        tension: 60
      }
    );
    this.slide_attachments_out = Animated.timing(
      this.animated_attachments_value,
      {
        toValue: 1,
        duration: 150,
        easing: Easing.linear
      }
    );
    this.slide_attachments_out_quick = Animated.timing(
      this.animated_attachments_value,
      {
        toValue: 1,
        duration: 50,
        easing: Easing.linear
      }
    );
    this.slide_input_down = Animated.timing(
      this.animated_input_value,
      {
        toValue: 100,
        duration: 150,
        easing: Easing.linear
      }
    );
    this.slide_input_up = Animated.timing(
      this.animated_input_value,
      {
        toValue: 0,
        duration: 150,
        easing: Easing.linear
      }
    );
    this.slide_vals =
      Platform.OS === 'android' ?
        this.animated_attachments_value.interpolate({
          inputRange: [0, 0.5, 1],
          outputRange: [0, Dimensions.get('window').width, 2 * Dimensions.get('window').width]
        })
        :
        this.animated_attachments_value.interpolate({
          inputRange: [0, 0.5, 1],
          outputRange: [-3 * Dimensions.get('window').width, 3 * Dimensions.get('window').width, 4 * Dimensions.get('window').width]
        });
  }


  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardWillShow', () => {
      //THIS IS NOT FIRING ON ANDROID, DON'T KNOW Y YET
      if (this.state.media_url) /** if there is already some media on the post page, we make the textinput even smaller */
        this.setState({ txt_input_height: 0.5 * PARTIAL_KEYBOARD_H });
      else
        this.setState({ txt_input_height: PARTIAL_KEYBOARD_H });
      if (Platform.OS === 'ios')
        this.setState({ show_keyboard_dismiser: true });
    });
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      //THIS HOWEVER SEEMS TO FIRE ON ANDROID (WEIRD MUCH???)
      if (Platform.OS === 'ios' && this.state.show_keyboard_dismiser) {
        this.setState({ show_keyboard_dismiser: false });
      }
      this.setState({ txt_input_height: FULL_KEYBOARD_H });

      this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
        this.forceUpdate();
      });
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
  }

  onType(text) {
    if (text.trim().length > 2) {
      this.setState({ text, submit_disabled: false });
    }
    else {
      const disable = this.state.media_url === null;
      this.setState({ text, submit_disabled: disable });
    }
  }

  textInputFocused() {
    if (this.attachment_options_shown)
      this.slideOut();
  }

  showAttachmentOptions() {
    if (!this.attachment_options_shown)
      this.slideIn();
    else
      this.slideOut();
    this.setState({
      show_preview: false
    });
  }

  slideIn() {
    this.animated_attachments_value.setValue(0);
    this.animated_input_value.setValue(0);
    Animated.parallel([
      this.slide_attachments_in.start(() => {
        this.attachment_options_shown = true;
      }),
      this.slide_input_down.start()
    ]);
  }
  slideOut() {
    this.animated_attachments_value.setValue(0.5);
    this.animated_input_value.setValue(100);
    Animated.parallel([
      this.slide_attachments_out.start(() => {
        this.attachment_options_shown = false;
      }),
      this.slide_input_up.start()
    ]);
  }

  slideOutQuick() {
    this.animated_attachments_value.setValue(0.5);
    this.animated_input_value.setValue(100);
    Animated.parallel([
      this.slide_attachments_out_quick.start(() => {
        this.attachment_options_shown = false;
      }),
      this.slide_input_up.start()
    ]);
  }


  openGallery(mode = 1) {
    this.setState({ show_preview: false, post_mode: mode });
    this.fileService.openGallery(mode)
      .then(uri_data => {
        let metadata = null;
        if (mode === 1 || mode === 2)
          metadata = this.utilities.resizeImage(uri_data.width, uri_data.height);

        let media_type = null;
        if (mode === 1)
          media_type = 'i';
        else if (mode === 2)
          media_type = 'v';
        else if (mode === 3)
          media_type = 'a';

        //if attachment options are shown, hide them
        if (this.attachment_options_shown)
          this.slideOut();

        this.setState({
          media_url: uri_data.uri,
          image_h: metadata ? metadata.width : null,
          image_w: metadata ? metadata.height : null,
          submit_disabled: false,
          show_remove_button: true,
          new_post_metadata: `{ "width": ${uri_data.width}, "height": ${uri_data.height} }`,
          new_post_media_type: media_type,

          temp_media_data: {
            FOR_POST_PREVIEW: true,
            metadata: `{ "width": ${uri_data.width}, "height": ${uri_data.height} }`,
            media_url: uri_data.uri,
            media_type,
            poster_image: null,
          }
        });
      })
      .catch(err => {
        this.utilities.handleErrors(err, 'ERROR FETCHING IMAGE FROM GALLERY');
        if (err !== 'CANCELLED')
          this.utilities.showToast('Sorry, could not load file.', 3000, 'ios-sad-outline');
      });
  }


  removeMedia() {
    const disable = !(this.state.text.trim().length > 2); //i.e if there is text more than 2 characters, then dont disable
    //the post button just because we are removing the media content
    this.setState({
      submit_disabled: disable,
      media_url: null,
      show_remove_button: false,
      new_post_media_type: 't',
      new_post_metadata: null,
      temp_media_data: null,
      txt_input_height: PARTIAL_KEYBOARD_H
    });
    //scroll to the top of the page, this helps in the case where there is some typed text which is hidden
    //when we remove the media, the text input is a bit messed up, scrolling to the top helps fix this
    if (Platform.OS === 'ios')
      this.scroll_ref.scrollToPosition(0, 0, true);
    else
      this.scroll_ref.scrollTo({ x: 0, y: 0, animated: true });
  }


  post() {
    if (this.state.submit_disabled)
      return;

    this.setState({ posting: true });
    Keyboard.dismiss();

    /**
     * time out has some UI implications but i'm feeling too lazy to explain
     * something about showing a giant loader, since prepending new post 
     * and updating the feed page tends to freeze the UI for a few seconds'
     */
    // this.props.moveToHome();
    const new_post_data = {
      NEW_POST: true,
      post_id: Math.random(),
      name: 'You',
      how_long_ago: 'just now',
      reposter_id: this.props.user_id,
      poster_id: this.props.user_id,
      media_type: this.state.new_post_media_type,
      media_url: this.state.media_url,
      metadata: this.state.new_post_metadata,
      reposted: false,
      post_text: this.state.text.trim(),
      funny: 0,
      not_funny: 0,
      favourited: null,
      comments: 0,
      stars: 0
      //............TODO NOT DEALT WITH VIDEOS YET..........
    };

    this.postService.saveNewItem(new_post_data)
      .then(() => this.postingComplete(SUCCESS, new_post_data))
      .catch(() => this.postingComplete(FAIL, new_post_data));
  }


  postingComplete(result, data) {
    if (result === SUCCESS) {
      this.utilities.showToast('POSTED! Yeah, you did it...');
      this.setState({
        media_url: null,
        text: '',
        submit_disabled: true,
        posting: false,
        show_preview: true,
        new_post_data: data
      });

      //NOW SEND A NOTIFICATION TO ALL MY PEOPLE
      this.postService.sendNotification();
    }
    else if (result === FAIL) {
      this.utilities.showToast('This is not funny! Posting failed...', 3000, 'ios-sad-outline');
      this.setState({
        posting: false,
      });
    }
  }

  _onTextInputViewScroll() {
    if (this.attachment_options_shown)
      this.slideOutQuick();
  }


  closePreview() {
    this.setState({ show_preview: false });
  }

  dismissKeyboard() {
    this.setState({ show_keyboard_dismiser: false });
    Keyboard.dismiss();
  }


  renderAttachButton() {
    if (Platform.OS === 'android') {
      return (
        <Button
          iconOnly
          icon={'clip'}
          iconPack={'funnyornot'}
          iconSize={20}
          textColor={'#aaa'}
          styles={{ backgroundColor: 'transparent', borderWidth: 0 }}
          tapped={this.showAttachmentOptions.bind(this)}
        />
      );
    }
    return (
      <Button
        iconOnly
        icon={'ios-images'}
        iconPack={'ionicons'}
        iconSize={25}
        textColor={Properties.themeColor}
        styles={{ backgroundColor: 'transparent', borderWidth: 0 }}
        tapped={this.openGallery.bind(this, 1)}
      />
    );
  }
  renderAttachmentOptions() {
    return (
      <Animated.View style={[styles.attachmentsView, { transform: [{ translateX: this.slide_vals }] }]}>
        <View style={styles.innerAttachmentsView}>
          <TouchableOpacity onPress={() => this.openGallery(1)} style={styles.attachMe}>
            <Icon2 name={'ios-image-outline'} size={40} color={'#aaa'} />
            <MyText styles={styles.attachmentText}>IMAGE</MyText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.openGallery(2)} style={styles.attachMe}>
            <Icon2 name={'ios-videocam-outline'} size={40} color={'#aaa'} />
            <MyText styles={styles.attachmentText}>VIDEO</MyText>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.openGallery(3)} style={styles.attachMe}>
            <Icon2 name={'ios-musical-notes-outline'} size={40} color={'#aaa'} />
            <MyText styles={styles.attachmentText}>AUDIO</MyText>
          </TouchableOpacity>
        </View>
      </Animated.View>
    );
  }

  renderLoader() {
    if (this.state.posting) {
      return (
        <View style={styles.posting}>
          {
            Platform.OS === 'android' ?
              <Spinner size={200} color={'#ccc'} />
              :
              <SpinnerWithContainerIos size={'large'} color={Properties.themeColor} />
          }
        </View>
      );
    }
  }


  renderMediaUrl() {
    if (this.state.media_url) {
      if (this.state.new_post_media_type === 'i')
        return <FeedCardImage feedItem={this.state.temp_media_data} />;
      else if (this.state.new_post_media_type === 'v' || this.state.new_post_media_type === 'a')
        return <FeedCardVideo feedItem={this.state.temp_media_data} />;
    }
  }


  renderContent() {
    if (this.state.show_preview) {
      return (
        <View style={styles.preview}>
          <ScrollView style={{ marginBottom: 50 }} ref={(scrollView) => { this.scroll_ref = scrollView; }}>
            <View style={styles.previewMessage}>
              <Icon name={'thumb-up'} color={'#999'} size={25} />
              <MyText styles={styles.previewText}>
                This is a preview of your new post. We hope its funny... or not. Either way, YOU are a legend!
              </MyText>
            </View>
            <FeedCard feedItem={this.state.new_post_data} />
          </ScrollView>
        </View>
      );
    }
    //normal scrollview works for android, but we might need a keyboardaware one for ios
    //<KeyboardAwareScrollView ref={(scrollView) => { this.scroll_ref = scrollView; }}>
    if (Platform.OS === 'android')
      return (
        <ScrollView
          ref={(scrollView) => { this.scroll_ref = scrollView; }}
          onScroll={this._onTextInputViewScroll.bind(this)}>
          <View style={styles.media}>
            {this.renderMediaUrl()}
          </View>
          <Animated.View style={{ marginTop: this.animated_input_value }} >
            <TextInput
              multiline
              placeholder={'Write something funny'}
              placeholderTextColor={'#BBB'}
              underlineColorAndroid={'transparent'}
              onChangeText={this.onType.bind(this)}
              onFocus={this.textInputFocused.bind(this)}
              style={[styles.textInput, { height: this.state.txt_input_height }]}
              value={this.state.text}
            />
          </Animated.View>
        </ScrollView >
      );
    return (
      <KeyboardAwareScrollView
        ref={(scrollView) => { this.scroll_ref = scrollView; }}
        onScroll={this._onTextInputViewScroll.bind(this)}>
        <View style={styles.media}>
          {this.renderMediaUrl()}
        </View>
        <Animated.View style={{ marginTop: this.animated_input_value }} >
          <TextInput
            multiline
            placeholder={'Write something funny'}
            placeholderTextColor={'#BBB'}
            underlineColorAndroid={'transparent'}
            onChangeText={this.onType.bind(this)}
            onFocus={this.textInputFocused.bind(this)}
            style={[styles.textInput, { height: this.state.txt_input_height }]}
            value={this.state.text}
          />
        </Animated.View>
      </KeyboardAwareScrollView>
    );
  }

  renderStatusBarIos() {
    if (this.props.tabbedVersion) {
      if (Platform.OS === 'ios') {
        return <StatusBarIos useThemeForStatusBar />;
      }
    }
  }

  render() {
    return (
      <MainContent>
        <View style={styles.page}>
          <View style={styles.btnView}>
            <View style={styles.removeMediaView}>
              {
                this.state.show_keyboard_dismiser ?
                  <Button
                    icon={'md-close'}
                    iconPack={'ionicons'}
                    iconSize={27}
                    textColor={Properties.themeColor}
                    styles={{ borderWidth: 0, paddingTop: 7 }}
                    tapped={this.dismissKeyboard.bind(this)}
                  /> :
                  null
              }
              {
                this.state.show_preview ?
                  <Button
                    icon={Platform.OS === 'android' ? 'md-close-circle' : 'ios-close-circle'}
                    text={'Preview'}
                    iconSize={Platform.OS === 'android' ? 30 : 25}
                    textColor={'#aaa'}
                    styles={styles.closePreviewButton}
                    tapped={this.closePreview.bind(this)}
                  /> :
                  this.state.media_url ?
                    <Button
                      iconOnly
                      icon={Platform.OS === 'android' ? 'md-close-circle' : 'ios-close-circle'}
                      iconSize={Platform.OS === 'android' ? 30 : 25}
                      textColor={'#aaa'}
                      styles={styles.removeMediaButton}
                      tapped={this.removeMedia.bind(this)}
                    />
                    : null
              }
            </View>
            <View style={styles.attachSubmitView}>
              {this.renderAttachButton()}
              <Button
                text={'POST'}
                icon={'ios-paper-plane'}
                iconSize={25}
                tapped={this.post.bind(this)}
                disabled={this.state.submit_disabled}
                disabledTextColor={'#aaa'}
                textColor={Properties.themeColor}
                styles={{ backgroundColor: 'transparent', borderWidth: 0 }}
              />
            </View>
          </View>
          {this.renderContent()}
          {this.renderAttachmentOptions()}
          {this.renderLoader()}
          {this.renderStatusBarIos()}
        </View>
      </MainContent>
    );
  }


}


const styles = {
  page: {
    flex: 1,
    backgroundColor: '#fff',
    //make room for status bar in ios
    paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT : null
  },
  btnView: {
    flexDirection: 'row',
    height: 50,
    paddingRight: 20,
  },
  textInput: {
    fontSize: 17,
    paddingLeft: 17,
    paddingRight: 17,
    paddingBottom: Platform.OS === 'android' ? 25 : 10,
    paddingTop: 20,
    textAlignVertical: 'top',
  },
  media: {
    alignItems: 'center'
  },
  removeMediaView: {
    flex: 1,
    alignItems: 'flex-start',
    paddingTop: Platform.OS === 'ios' ? 4 : null
  },
  removeMediaButton: {
    backgroundColor: 'transparent',
    borderWidth: 0
  },
  attachSubmitView: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  posting: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  preview: {
    flex: 1,
    backgroundColor: Properties.greyBackColor,
  },
  closePreviewButton: {
    borderWidth: 0.5,
    borderColor: '#ccc',
    borderRadius: 2,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: 'transparent',
    alignItems: 'center',
    marginLeft: 5,
    marginBottom: 0,
    alignSelf: 'center',
    marginTop: 8
  },
  previewText: {
    padding: 10,
    lineHeight: 20,
    paddingRight: 20
  },
  previewMessage: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10
  },
  attachmentsView: {
    position: 'absolute',
    top: Platform.OS === 'android' ? 50 : 50 + IOS_STATUS_BAR_HEIGHT,
    left: Platform.OS === 'android' ? -Dimensions.get('window').width : -3 * (Dimensions.get('window').width),
    width: Dimensions.get('window').width,
    alignItems: 'center'
  },
  innerAttachmentsView: {
    flexDirection: 'row',
    backgroundColor: Properties.greyBackColor,
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 80,
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
    borderRadius: 50,
    width: 0.95 * Dimensions.get('window').width,
  },
  attachmentOption: {
    marginRight: 40,
    marginLeft: 20
  },
  attachmentText: {
    color: '#AAA',
    lineHeight: 20,
    fontSize: 13
  },
  attachMe: {
    justifyContent: 'center',
    alignItems: 'center'
  },

  container: {
    marginTop: 60,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingTop: 20,
  },

};


function mapStateToProps(state) {
  return {
    avatar_url: state.settings.user_avatar,
    user_id: state.settings.user_id
  };
}

export default connect(mapStateToProps, { addNewPost })(PostPage);
