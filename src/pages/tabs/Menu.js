import React, { Component } from 'react';
import { View, ScrollView, Platform, Share } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { MainContent, Properties, MenuSection, MenuItem, StatusBarIos } from '../../common';

import {
  AvatarService, iAvatarService,
  Utilities,
  DatabaseService,
  Config,
  ShareService
} from '../../SERVICES';
import { IOS_STATUS_BAR_HEIGHT } from '../../types';


class MenuPage extends Component {

  avatarService: iAvatarService;

  constructor(props) {
    super(props);
    this.avatarService = new AvatarService(new Config(), new Utilities(), new DatabaseService());
    this.shareService = new ShareService();
  }

  state = {
    avatar_url: null
  };


  goToPage(page) {
    switch (page) {
      case 'settings':
        Actions.settingsPage();
        break;
      case 'contacts':
        Actions.contactsPage();
        break;
      case 'following':
        Actions.followingPage();
        break;
      case 'followers':
        Actions.followersPage();
        break;
      case 'alltime':
        Actions.alltimePage();
        break;
      case 'images':
        Actions.mediaPage({ which: 'images' });
        break;
      case 'videos':
        Actions.mediaPage({ which: 'videos' });
        break;
      case 'invite':
        Actions.invitePage();
        break;
      case 'favourites':
        Actions.favouritesPage();
        break;
      default:
    }
  }

  inviteFriends() {
    if (Platform.OS === 'android') {
      this.shareService.inviteFriends();
    }
    else {
      Share.share(
        {
          title: 'Invite a friend to FunnyorNot',
          message: "You won't stop laughing! Check out FunnyorNot on the app store: https:/url-to-appstore"
        },
        {
          tintColor: Properties.themeColor
        });
    }
  }


  //status bar in ios
  renderStatusBarIos() {
    if (Platform.OS === 'ios')
      return <StatusBarIos />;
  }

  render() {
    return (
      <View style={{ flex: 1}}>
        <ScrollView>
          <View style={styles.page}>
            <View style={{ backgroundColor: '#fff', paddingBottom: 55 }}>
              <MenuSection title={'PEOPLE'} border={false}>
                <MenuItem
                  onTap={this.goToPage.bind(this, 'contacts')}
                  icon={Platform.OS === 'android' ? 'ios-contact' : 'ios-contact-outline'}
                  text={'Contacts'}
                />
                <MenuItem
                  onTap={this.goToPage.bind(this, 'following')}
                  icon={Platform.OS === 'android' ? 'md-contacts' : 'ios-contacts-outline'}
                  text={'Following'} />
                <MenuItem
                  onTap={this.goToPage.bind(this, 'followers')}
                  icon={Platform.OS === 'android' ? 'ios-people' : 'ios-people-outline'}
                  text={'Followers'}
                />
              </MenuSection>
              <MenuSection title={'FEED'}>
                <MenuItem onTap={this.goToPage.bind(this, 'alltime')} icon={'ios-star-outline'} text={'Jokes of Fame'} />
                <MenuItem onTap={this.goToPage.bind(this, 'favourites')} icon={'ios-heart-outline'} text={'My Favourites'} />
                <MenuItem onTap={this.goToPage.bind(this, 'images')} icon={'ios-images-outline'} text={'Images'} />
                <MenuItem onTap={this.goToPage.bind(this, 'videos')} icon={'ios-videocam-outline'} text={'Videos'} />
              </MenuSection>
              <MenuSection title={'OTHER OPTIONS'}>
                <MenuItem
                  onTap={this.goToPage.bind(this, 'settings')}
                  icon={Platform.OS === 'android' ? 'ios-settings' : 'ios-settings-outline'}
                  text={'Settings'}
                />
                <MenuItem
                  onTap={this.inviteFriends.bind(this)}
                  icon={Platform.OS === 'android' ? 'md-share' : 'ios-share-outline'}
                  text={'Invite Friends'}
                />
              </MenuSection>
            </View>
          </View>
        </ScrollView>
        {this.renderStatusBarIos()}
      </View>
    );
  }

}


const styles = {
  page: {
    flex: 1,
    paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT : null
  },
  profilePicView: {
    paddingTop: 20,
    paddingBottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatarContainer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: Properties.greyBackColor,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50
  }
};


export default MenuPage;
