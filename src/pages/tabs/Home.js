import React, { Component } from 'react';
import { AppState, View, Platform, AsyncStorage } from 'react-native';
import Feed from '../base/Feed';
import {
  HOME, INADEQUATE_FEED, APP_TO_BACKGROUND, APP_TO_FOREGROUND,
  DEVICE_ID, USER_ID
} from '../../types';
import { RecommendedFeed } from '../base/RecommendedFeed';
import OneSignal from 'react-native-onesignal';

import {
  EventEmitter,
  Config, Utilities, DatabaseService, ContactService, UserService, FileService
} from '../../SERVICES';
import { HeaderParallax } from '../../common';

class HomePage extends Component {

  state = {
    show_welcome_page: false
  }

  componentDidMount() {
    const config = new Config();
    const utilities = new Utilities();
    const dbService = new DatabaseService(utilities);
    this.contactService = new ContactService(config, utilities, dbService);
    this.userService = new UserService(config, utilities, dbService);
    this.fileService = new FileService(config, utilities);
    let HAS_UPDATED_ONCE = false;

    AppState.addEventListener('change', () => {
      if (AppState.currentState === 'background') {
        //update friend list
        if (!HAS_UPDATED_ONCE) {
          HAS_UPDATED_ONCE = true;
          this.userService.getCurrentUsersCountCode()
            .then(country_code => {
              this.contactService.fetchContacts()
                .then(contacts => {
                  this.contactService.keepInRAM(contacts);
                  return this.contactService.findAndSaveFriends(country_code);
                })
            });
        }
        if (Platform.OS === 'android')
          this.fileService.cleanUpMediaCache();

        EventEmitter.dispatch(APP_TO_BACKGROUND);
      }
      else if (AppState.currentState === 'active') {
        EventEmitter.dispatch(APP_TO_FOREGROUND);
      }
    });

    this.inadequate_feed_subscription = EventEmitter.subscribe(INADEQUATE_FEED, () => {
      this.setState({ show_welcome_page: true });
    });
  }

  render() {
    if (this.state.show_welcome_page) {
      return (
        <View style={{ flex: 1 }}>
          <RecommendedFeed />
          <HeaderParallax title={'Home'} pageMode={HOME} />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <Feed pageMode={HOME} />
        <HeaderParallax title={'Home'} pageMode={HOME} />
      </View>
    );
  }

  componentWillUnmount() {
    EventEmitter.unsubscribe(INADEQUATE_FEED, this.inadequate_feed_subscription);
  }

}

export default HomePage;
