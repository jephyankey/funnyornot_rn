import React, { Component } from 'react';
import { connect } from 'react-redux';
import ProfilePage from '../subpages/Profile';
import { CombinedStore } from '../../reducers/interfaces';

import {
  Utilities,
  Config,
  DatabaseService,
  AvatarService
} from '../../SERVICES';



class TabProfilePage extends Component {

  constructor(props) {
    super(props);
    const config = new Config();
    const utilities = new Utilities();
    const dbService = new DatabaseService(utilities);
    this.avatarService = new AvatarService(config, utilities, dbService);
  }

  render() {
    return (
      <ProfilePage
        avatar_url={this.props.avatar}
        name={'You'}
        profile_id={this.props.profile_id}
        tabbedVersion />
    );
  }
}


function mapStateToProps(state: CombinedStore) {
  return {
    avatar: state.settings.user_avatar,
    name: 'You',
    profile_id: state.settings.user_id
  };
}

export default connect(mapStateToProps, {})(TabProfilePage);
