import React, { Component } from 'react';
import { View } from 'react-native';
import Feed from '../base/Feed';
import { EXPLORE } from '../../types';
import { HeaderParallax } from '../../common';



class ExplorePage extends Component {

  render() {
    return (
    <View style={{ flex: 1 }}>
      <Feed pageMode={EXPLORE} />
      <HeaderParallax title={'Explore'} pageMode={EXPLORE} />
    </View>);
  }

}

export default ExplorePage;
