//imports
import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';
import { Properties, GalleryItem } from '../../common';

import { SwipeMedia } from '../base';


export class Gallery extends Component {

    state = {
        modal_visible: false,
        initial_slide_id: 0
    };

    propTypes: {
        data: React.propTypes.Array.isRequired;
    }

    showSwipable(id) {
        this.setState({ modal_visible: true, initial_slide_id: id });
    }

    closeSwipable() {
        this.setState({ modal_visible: false });
    }

    renderImages() {
        const data = this.props.data;
        const renderingObject = [];
        for (let i = 0; i < data.length; i++) {
            const url = data[i].media_type === 'i' ? data[i].media_url : data[i].poster_image;
            renderingObject.push(
                <GalleryItem
                    key={data[i].post_id}
                    imageSource={url}
                    mediaType={data[i].media_type}
                    onTap={this.showSwipable.bind(this, data[i].post_id)}
                    />
            );
        }
        return renderingObject;
    }

    render() {
        return (
            <View style={styles.gallery}>
                {this.renderImages()}
                <SwipeMedia
                    modalVisible={this.state.modal_visible}
                    data={this.props.data}
                    initialSlideId={this.state.initial_slide_id}
                    onClose={this.closeSwipable.bind(this)}
                    />
            </View>
        );
    }
}

const window = Dimensions.get('window').width;


const styles = {
    gallery: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: Properties.greyBackColor,
        paddingLeft: 0.01 * window,
        paddingTop: 0.01 * window,
        flexWrap: 'wrap',
        marginBottom: 30
    },
};
