import React, { Component } from 'react';
import { Animated, Platform, Easing, View, Text, Dimensions, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { EventEmitter } from '../../SERVICES';
import { HIDE_TOAST, SHOW_TOAST, IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT, NEW_THEME_CHOSEN } from '../../types';
import { Properties } from '../../common';


export class Toast extends Component {

    constructor(props) {
        super(props);
        this.slide_value = new Animated.Value(0);
        this.slide_out = Animated.timing(
            this.slide_value,
            {
                toValue: 0,
                duration: 200,
                easing: Easing.linear
            }
        );
        this.slide_out_instant = Animated.timing(
            this.slide_value,
            {
                toValue: 0,
                duration: 1,
                easing: Easing.linear
            }
        );
        this.slide_in = Animated.spring(
            this.slide_value,
            {
                toValue: 1,
                duration: 200,
                easing: Easing.linear
            }
        );
        this.slide_vals = this.slide_value.interpolate({
            inputRange: [0, 1],
            outputRange: [-100, Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT + HEADER_HEIGHT : HEADER_HEIGHT]
        });
    }

    state = {
        toast_text: '',
        icon: 'ios-happy-outline'
    }

    componentDidMount() {
        this.show_toast_subscription = EventEmitter.subscribe(SHOW_TOAST, ({ message, duration, icon }) => {
            this.showToast(message, duration, icon);
        });
        this.hide_toast_subscription = EventEmitter.subscribe(HIDE_TOAST, () => {
            this.hideToast();
        });
        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
            this.forceUpdate();
        });

        setTimeout(() => { //otherwise it wil not get the correct theme
            this.forceUpdate();
        }, 1000);
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(SHOW_TOAST, this.show_toast_subscription);
        EventEmitter.unsubscribe(HIDE_TOAST, this.hide_toast_subscription);
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
    }


    showToast(text, duration = 3000, icon) {
        if (this.toast_timer)
            clearTimeout(this.toast_timer);
        this.slide_out_instant.start();
        this.setState({ toast_text: text, icon });
        this.slide_in.start();
        this.toast_timer = setTimeout(() => {
            this.slide_out.start();
        }, duration);
    }

    hideToast() {
        this.slide_out.start();
        if (this.toast_timer)
            clearTimeout(this.toast_timer);
    }


    render() {
        return (
            <View style={{ flex: 1, padding: 0, margin: 0 }}>
                {this.props.children}
                <Animated.View style={[styles.toastWidth, { transform: [{ translateY: this.slide_vals }] }]}>
                    <TouchableWithoutFeedback onPress={this.hideToast.bind(this)}>
                        <View style={{ ...styles.toast, shadowColor: Properties.themeColor }}>
                            <Icon name={this.state.icon} size={35} color={Properties.themeColor} style={{ marginRight: 15 }} />
                            <View>
                                <Text style={styles.toastText}>{this.state.toast_text}</Text>
                            </View >
                        </View >
                    </TouchableWithoutFeedback>
                </Animated.View>
            </View>
        );
    }
}


const styles = {
    toastWidth: {
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    toast: {
        width: Dimensions.get('window').width,
        paddingVertical: 10,
        paddingHorizontal: 30,
        // borderRadius: 20,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .95)',
        minHeight: 50,
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 2,
        shadowOpacity: 0.4
    },
    toastText: {
        color: '#333',
        fontSize: 16
    }
};


