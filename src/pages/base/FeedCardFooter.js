import React, { Component, PropTypes } from 'react';
import { Platform, View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';
import Icon8 from '../../common/Icon';

import {
    Utilities, iUtilities,
    UserService, iUserService,
    Config, iConfig,
    PostService, iPostService,
    DatabaseService,
    EventEmitter
} from '../../SERVICES';

import { COMMENT_SUBMITTED, SUCCESS } from '../../types';


class FeedCardFooter extends Component {

    my_stars: number; //the number of funnys or notfunnyies chosen by current user for a post
    funny_without_this_user: number;
    not_funny_without_this_user: number;
    run: number = 0;

    userService: iUserService;
    config: iConfig;
    utilities: iUtilities;
    postService: iPostService;

    static contextTypes = {
        actionSheet: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.config = new Config();
        this.utilities = new Utilities();
        this.userService = new UserService(this.config, this.utilities, new DatabaseService());
        this.postService = new PostService(this.config, this.utilities, this.userService, null);
    }

    // proptypes: {
    //     // onStar: React.PropTypes.func.isRequired //function that runs to show a star in the view when 3 stars 
    //     //given to a post
    // }

    state = {
        funnies_to_show: this.props.feedItem.funny,
        stars: this.props.feedItem.stars,
        funnyORnot: this.props.feedItem.funnyORnot,
        favourited: !(this.props.feedItem.favourited === null), //i.e. if its null, then set favourited to false, else true
        total_comments: this.props.feedItem.comments
    }

    componentDidMount() {
        this.run++;
        //in this case the user had previously given some +ve stars to this post, remove them
        if (this.props.feedItem.stars >= 0) {
            //remove this user's contribution to funny count
            this.funny_without_this_user = this.props.feedItem.funny - this.props.feedItem.stars;
            //not funny count should not be affected
            this.not_funny_without_this_user = this.props.feedItem.not_funny;
        }
        //user had previously said this is not funny or given -ve stars
        else {
            //so remove his numbers from the not funny count
            //also remember, we can only give one -ve star
            this.not_funny_without_this_user = this.props.feedItem.not_funny - 1;
            //funny count remains unaffected
            this.funny_without_this_user = this.props.feedItem.funny;
        }

        //subscribe to a user adding a comment, so we can increase the total comments number
        //shown in the view
        this.new_comment_subscription = EventEmitter.subscribe(COMMENT_SUBMITTED, ({ parent_id, submit_state }) => {
            if (parent_id === this.props.feedItem.post_id && submit_state === SUCCESS) {
                this.setState({ total_comments: this.state.total_comments + 1 });
            }
        });
    }


    showOptions() {
        if (this.props.feedItem.NEW_POST) {
            this.utilities.showToast('This is disabled in preview mode');
            return;
        }
        const options = Platform.OS === 'ios' ? ['Not Funny', 'Block This Post', 'Report Abuse', 'Cancel'] :
            ['Not Funny', 'Block This Post', 'Report Abuse'];
        const textStyle = { color: '#555', fontSize: 15, fontWeight: '400' };
        const cancelButtonIndex = 3;

        this.context.actionSheet().showActionSheetWithOptions({
            options,
            textStyle,
            cancelButtonIndex: Platform.OS === 'ios' ? cancelButtonIndex : null,
            destructiveButtonIndex: 2,
        },
            (index) => {
                if (index === 0) {
                    this.funnyOrNot('notfunny');
                }
                else if (index === 1) {
                    this.postService.blockPost(this.props.feedItem.post_id);
                }
                else if (index === 2) {
                    this.postService.reportAbuse(this.props.feedItem.post_id);
                }
            });
    }

    setAsFunnyOrNot() {
        this.postService.setAsFunnyOrNot(this.props.feedItem.post_id, this.state.stars, this.props.feedItem.poster_id);
    }

    funnyOrNot(itis) {
        if (this.props.feedItem.NEW_POST) {
            this.utilities.showToast('This is disabled in preview mode');
            return;
        }
        //deliberately using == AND NOT === because the .current_users_id and poster_id may not have the 
        //the same type (e.g string vs int) event though they might have the same values
        if (this.props.current_users_id == this.props.feedItem.poster_id) {
            if (itis === 'funny')
                this.utilities.showToast('This is your own post. Who are you trying to deceive?');
            else
                this.utilities.showToast("Ok, so why did you post this if you knew it wasn't funny?");
            return;
        }
        if (this.state.funnyORnot) {
            if (this.state.stars === -1) {
                this.utilities.showToast('You already said this is not funny!');
            }
            else {
                if (itis === 'notfunny')
                    this.utilities.showToast('You already said this is funny');
                else
                    this.utilities.showToast('Hehehe! You cannot change your rating', 3000, 'ios-happy-outline');
            }
            return;
        }
        /**
         * the user is changing his funnies, so remove any existing ones
         * and then add the new one
         */

        //now take in the new no of stars the user is inputting
        if (itis === 'funny') {
            //set the number of stars
            if (this.state.stars === -1 || this.state.stars === 0)
                this.my_stars = 1;
            else if (this.state.stars === 1)
                this.my_stars = 2;
            else if (this.state.stars === 2)
                this.my_stars = 3;
            else if (this.state.stars === 3)
                this.my_stars = 0;

            //change total no of funnys in the View
            this.setState({
                funnies_to_show: this.funny_without_this_user + this.my_stars,
                stars: this.my_stars
            });
            //if and else block will show or remove the star from the feedcardheader (in the list of indicators)
            // if (this.my_stars === 3)
            //     this.props.onStar();
            // else
            //     this.props.onStar(false);

            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                //send http request to update stars in dbase
                if (this.state.stars !== 0) {
                    this.setAsFunnyOrNot();
                    this.setState({ funnyORnot: true });
                }
            }, 12000);
        }
        else if (itis === 'notfunny') {
            //set the number of stars
            this.my_stars = -1;

            //change total no of funnys in the View (should use setState) 
            //if only the user had previously chosen positive stars necessary
            this.setState({
                funnies_to_show: this.funny_without_this_user,
                stars: this.my_stars,
                funnyORnot: true
            });
            this.utilities.showToast('Not Funny! Good to know.');

            clearTimeout(this.timeout);
            /**
             * this time out is not necessary to the logic of the application
             * but, without it, this.setAsfunnyornot runs immediately after this.setState above and misses
             * the correct value of stars (i.e. this.state.stars)
             */
            setTimeout(() => {
                //send http request to update stars in dbase
                this.setAsFunnyOrNot();
            }, 1000);
        }
    }


    showComments() {
        if (this.props.feedItem.NEW_POST) {
            this.utilities.showToast('You cannot view comments while in preview mode');
            return;
        }
        Actions.commentsPage({
            totalComments: this.state.total_comments,
            parentId: this.props.feedItem.post_id,
            fromPage: this.props.feedItem.feed
        });
    }

    favourite() {
        if (this.props.feedItem.NEW_POST) {
            this.utilities.showToast('You cannot add to favourites while in preview mode');
            return;
        }
        if (this.state.favourited === false) { // -> then we want to add to favourites
            //optimistic update
            this.setState({ favourited: true });
            this.postService.addToFavourites(this.props.feedItem.post_id)
                .catch(() => {
                    //revert optimistic update
                    this.setState({ favourited: false });
                    this.utilities.showToast('Could not add to favourites');
                });
        }
        else { // -> then we want to remove from favourites
            //optimistic update
            this.setState({ favourited: false });
            this.postService.removeFromFavourites(this.props.feedItem.post_id)
                .catch(() => {
                    //revert optimistic update
                    this.setState({ favourited: true });
                    this.utilities.showToast('Could remove from favourites');
                });
        }
    }


    render() {
        return (
            <View style={styles.feedcardFooter}>
                <TouchableOpacity onPress={this.funnyOrNot.bind(this, 'funny')}>
                    <View style={styles.view}>
                        <View style={styles.iconView}>
                            <Icon8
                                name={this.state.stars < 0 ? 'sad' :
                                    this.state.stars === 0 ? 'neutral' :
                                        this.state.stars === 1 ? 'smile' :
                                            this.state.stars === 2 ? 'laugh' :
                                                this.state.stars === 3 ? 'lol' : null}
                                size={25}
                                color={'#bbb'}
                            />
                        </View>
                        <Text style={styles.text}>{this.state.funnies_to_show}</Text>
                    </View>
                </TouchableOpacity >

                <TouchableOpacity onPress={this.favourite.bind(this)}>
                    <View style={[styles.view, { marginTop: 2 }]}>
                        <View style={styles.iconView}>
                            <Icon8
                                name={this.state.favourited ? 'heart_filled' : 'heart'}
                                size={25}xs
                                color={'#bbb'}
                            />
                        </View>
                    </View>
                </TouchableOpacity >

                <TouchableOpacity onPress={this.showComments.bind(this)}>
                    <View style={[styles.view, { marginTop: 2 }]}>
                        <View style={styles.iconView}>
                            <Icon8 name={'comment'} size={25} color={'#bbb'} />
                        </View>
                        <Text style={styles.text}>{this.state.total_comments}</Text>
                    </View>
                </TouchableOpacity >

                <TouchableOpacity style={styles.moreBtn} onPress={this.showOptions.bind(this)}>
                    <Icon8 name={Platform.OS === 'android' ? 'more-vert' : 'more-horiz'} size={30} color={'#bbb'} />
                </TouchableOpacity>
            </View>
        );
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(COMMENT_SUBMITTED, this.new_comment_subscription);
    }
}


const styles = {
    feedcardFooter: {
        paddingTop: 5,
        marginLeft: 16,
        marginRight: 16,
        flexDirection: 'row',
        alignItems: 'center'
    },
    moreBtn: {
        position: 'absolute',
        right: 0,
        flexGrow: 1,
        // paddingTop: Platform.OS === 'ios' ? 4 : 0,
    },

    view: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        marginRight: 50
    },
    text: {
        fontSize: 15,
        color: '#bbb'
    },
    iconView: {
        marginRight: 5,
    }

};


function mapStateToProps(state) {
    return {
        current_users_id: state.settings.user_id
    };
}

export default connect(mapStateToProps, {})(FeedCardFooter);
