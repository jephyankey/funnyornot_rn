//imports
import React, { Component } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { Properties, MyText, Button } from '../../common';
import { Utilities } from '../../SERVICES';



export class ContactItem extends Component {

    propTypes: {
        onTap: React.PropTypes.func,
        mainText: React.PropTypes.string,
        subText: React.PropTypes.string,
        btnText: React.PropTypes.string
    }


    constructor(props) {
        super(props);
        this.utilities = new Utilities();
    }

    render() {
        return (
            <View style={styles.contactView}>
                <View style={styles.avatarView}>
                    <Icon name={'ios-contact-outline'} size={60} color={'#bbb'} />
                </View>
                <View style={styles.border}>
                    <View style={styles.contactDetails}>
                        <MyText styles={styles.contactName}>
                            {this.utilities.truncateString(this.props.mainText, 20)}
                        </MyText>
                        {
                            this.props.subText ?
                                <MyText styles={styles.phoneNo}>{this.props.subText}</MyText>
                                :
                                <MyText /> //formattingpurposes
                        }
                    </View>
                    <View style={styles.inviteButton}>
                        <Button
                            text={this.props.btnText}
                            styles={styles.buttonStyles}
                            textStyles={{ fontSize: 12, lineHeight: 15 }}
                            weight='400'
                            tapped={this.props.onTap}
                            />
                    </View>
                </View>
            </View>
        );
    }
}


const styles = {
    contactView: {
        flexDirection: 'row',
    },
    avatarView: {
        alignSelf: 'flex-start',
        // marginLeft: 12,
        marginRight: 12,
        paddingTop: 6,
        paddingBottom: 6
    },
    border: {
        flex: 1,
        borderBottomWidth: 0.5,
        borderBottomColor: '#ccc',
        flexDirection: 'row',
        paddingRight: 10,
        alignItems: 'center',
        paddingTop: 6,
        paddingBottom: 6
    },
    inviteButton: {
        justifyContent: 'center'
    },
    contactDetails: {
        flex: 1,
        paddingTop: -5
    },
    contactName: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 15
    },
    phoneNo: {
        color: '#999',
        fontSize: 16
    },
    buttonStyles: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: Properties.darkBlue,
    }
};
