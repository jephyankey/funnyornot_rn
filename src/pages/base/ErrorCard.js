//imports
import React, { Component } from 'react';
import { Platform, Text, Image, View, Dimensions } from 'react-native';

import { Button, MyText, Properties } from '../../common';
import Icon from '../../common/Icon';

import {
    NO_COMMENTS, NO_INTERNET, NO_INTERNET2, LOAD_FAIL, EMPTY_RESULT,
    NO_CONTACTS_ON_APP, NO_CONTACTS_TO_INVITE, NO_FOLLOWING_ON_APP,
    NO_FOLLOWERS_ON_APP, NO_ONE_DURING_SEARCH,

    NEW_THEME_CHOSEN,
    HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT
} from '../../types';


import { EventEmitter } from '../../SERVICES';



export class ErrorCard extends Component {

    propTypes: {
        errorState: React.PropTypes.object.isRequired,
        onRetry: React.PropTypes.func,
        specialErrorMsg: React.PropTypes.string,
    }

    componentDidMount() {
        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
            this.forceUpdate();
        });
    }

    render() {
        /**
         * actual width and hieght of the image, 
         * provide by looking at the image directly
         */
        let width = 644;
        let height = 500;
        const max_w = Dimensions.get('window').width * 0.5; //80% of device width
        const ratio = max_w / width;
        if (ratio < 1) {
            //this means the actual width is bigger than the device width
            width *= ratio;
            height *= ratio;
        }

        if (this.props.errorState === NO_INTERNET) {
            return (
                <View style={styles.overall1}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            We've lost our way!{'\n'}Please check your internet connection.
                        </Text>
                        <Button 
                            text={'RETRY'} 
                            tapped={this.props.onRetry} 
                            styles={{ borderWidth: Platform.OS === 'ios' ? 1 : 0 }} 
                            textColor={Platform.OS === 'ios' ? Properties.themeColor : '#fff'} 
                            />
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === NO_INTERNET2) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            We've lost our way!{'\n'}Please check your internet connection.
                        </Text>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === LOAD_FAIL) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <MyText styles={styles.text}>
                            This is not funny! An error has occured at our end. {'\n'} We need to kill one of our programmers!
                        </MyText>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === EMPTY_RESULT) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <MyText styles={styles.text}>
                            There is nothing to show here yet
                        </MyText>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === NO_COMMENTS) {
            return (
                <View style={styles.centerPage}>
                    <Icon name={'comment'} size={120} color={'#bbb'} />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            Be the first to comment on this post!
                        </Text>
                    </View>
                </View>
            );
        }

        //contacts page
        else if (this.props.errorState === NO_CONTACTS_ON_APP) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            You have no contacts who use FunnyorNot
                        </Text>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === NO_FOLLOWING_ON_APP) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            You are not yet following anyone
                        </Text>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === NO_FOLLOWERS_ON_APP) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            You do not yet have any followers
                        </Text>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === NO_CONTACTS_TO_INVITE) {
            return (
                <View style={styles.overall}>
                    <Image
                        source={require('../../imgs/errors/no_internet.png')}
                        style={{ width, height }}
                        />
                    <View style={styles.textView}>
                        <Text style={styles.text}>
                            We could not find any valid contacts
                        </Text>
                    </View>
                </View>
            );
        }
        else if (this.props.errorState === NO_ONE_DURING_SEARCH) {
            return (
                <View style={styles.noneInSearch}>
                    <Text style={styles.text}>
                        No one matches your search
                        </Text>
                </View>
            );
        }
        return <View><Text>some error we have not found yet. if you are seeing this, kindly let us know throught the feedback option. Thanks!!</Text></View>;
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
    }
}

const styles = {
    overall1: {
        flex: 1,
        marginTop: Platform.OS === 'android' ? -(HEADER_HEIGHT+30) : -(HEADER_HEIGHT),
        alignItems: 'center',
        justifyContent: 'center'
    },
    overall: {
        marginTop: Platform.OS === 'android' ? -(HEADER_HEIGHT) : -(HEADER_HEIGHT),
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    overall2: {
        alignItems: 'center'
    },
    noneInSearch: {
        alignItems: 'center',
        flex: 1,
        paddingTop: 50
    },
    centerPage: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: Platform.OS === 'android' ? (HEADER_HEIGHT) : (HEADER_HEIGHT+IOS_STATUS_BAR_HEIGHT),
    },
    text: {
        textAlign: 'center',
        fontSize: 14,
        marginTop: 20,
        marginBottom: 50,
        color: '#aaa',
        fontWeight: 'bold'
    }
};
