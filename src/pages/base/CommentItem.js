//imports
import React, { Component } from 'react';
import { View, TouchableOpacity, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Icon from '../../common/Icon';
import { MyText, Properties } from '../../common';
import { Avatar } from '../base';

import {
    Utilities,
    Config,
    DatabaseService,
    AvatarService,
    EventEmitter
} from '../../SERVICES';
import { COMMENT_SUBMITTED, SUCCESS, FAIL } from '../../types';


export class CommentItem extends Component {

    propTypes: {
        data: React.PropTypes.object.isRequired,
    }

    state = {
        submit_success: null,
    }

    constructor(props) {
        super(props);
        const config = new Config();
        this.utilities = new Utilities();
        const dbService = new DatabaseService(this.utilities);
        this.avatarService = new AvatarService(config, this.utilities, dbService);
    }

    componentDidMount() {
        this.comment_submit_subscription = EventEmitter.subscribe(COMMENT_SUBMITTED, ({ new_comment_id, submit_state }) => {
            if (this.props.data.comment_id === new_comment_id) {
                if (submit_state === SUCCESS)
                    this.setState({ submit_success: true });
                else if (submit_state === FAIL)
                    this.setState({ submit_success: false });
            }
        });
    }

    goToProfile(user) {
        let my_name = '';
        if (user.name) {
            my_name = user.name;
        }
        else {
            my_name = user.username === 'You' ? 'You' : `@${user.username}`;
        }
        Actions.profilePage({
            profile_id: user.poster_id,
            avatar_url: user.avatar_url,
            name: my_name
        });
    }


    reportComment(comment_id) {
         Alert.alert(
            'Report comment?',
            `This comment is abusive, vulgar, or inappropriate in some other way, and should be taken down.`,
            [
                { text: 'NOPE', onPress: () => { }, style: 'cancel' },
                {
                    text: 'YUP',
                    onPress: () => {
                        this.utilities.showToast('Comment Reported!');
                        //send a request to backend using http and comment_id
                    }
                }
            ]
        );
    }


    renderBottomIcons() {
        if (!this.props.data.new_comment) {
            return (
                <TouchableOpacity onPress={this.reportComment.bind(this, this.props.data.comment_id)}>
                    <Icon name={'flag-o'} size={18} color={'#ccc'} />
                </TouchableOpacity>
            );
        }
    }

    renderCheckMark() {
        if (this.state.submit_success === true)
            return <Icon name={'done-all'} size={20} color={'green'} />;
        if (this.state.submit_success === false)
            return <Icon name={'warning'} size={20} color={'red'} />;
        else if (this.props.data.new_comment)
            return <Icon name={'done'} size={20} color={'#ccc'} />;
    }


    render() {
        const data = this.props.data;
        const avatar = data.avatar_url || null;
        return (
            <View style={styles.commentView}>
                <View style={styles.avatarView}>
                    <Avatar size={40} url={avatar} /*onTap={this.goToProfile.bind(this, data)}*/ />
                </View>
                <View style={styles.commentSubView}>
                    <View style={styles.indicators}>
                        <MyText styles={styles.commentor}>{data.name || data.username}</MyText>
                        <MyText styles={styles.date}>{data.how_long_ago}</MyText>
                    </View>
                    <View style={styles.finalCommentBox}>
                        <MyText styles={styles.comment}>{decodeURIComponent(data.comment)}</MyText>
                        {this.renderCheckMark()}
                    </View>
                    <View style={styles.reportView}>
                        {this.renderBottomIcons()}
                    </View>
                </View>
            </View>
        );
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(COMMENT_SUBMITTED, this.comment_submit_subscription);
    }
}



const styles = {
    commentView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15
    },
    commentSubView: {
        flex: 1
    },
    commentor: {
        fontSize: 14,
        color: '#000',
        fontWeight: 'bold',
        // flex: 1,
        lineHeight: 20,
        marginRight: 20
    },
    date: {
        color: '#aaa',
        lineHeight: 20,
        fontSize: 12
    },
    finalCommentBox: {
        padding: 0,
        margin: 0,
        paddingTop: 0,
    },
    comment: {
        fontSize: 14,
        lineHeight: 20,
        color: '#000'
    },
    indicators: {
        flexDirection: 'row'
    },
    reportView: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
    avatarView: {
        alignSelf: 'flex-start',
        paddingTop: 5
    }
};
