//imports
import React, { Component } from 'react';
import { Platform, View, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';

import { MyText, Button, Properties } from '../../common';

import { Config, Utilities, DatabaseService, UserService } from '../../SERVICES';


export class RecommendedFollower extends Component {

    constructor(props) {
        super(props);
        const config = new Config();
        this.utilities = new Utilities();
        const dbService = new DatabaseService(this.utilities);
        this.userService = new UserService(config, this.utilities, dbService);
    }


    state = {
        is_following: false
    }

    goToProfile(user) {
        Actions.profilePage({
            profile_id: user.user_id,
            avatar_url: user.avatar_url,
            name: `@${user.username}`
        });
    }

    followOrUnfollow() {
        const f = this.props.user;
        if (this.state.is_following === true) {
            //OPTIMISTIC UI UPDATES
            this.setState({ is_following: false });
            this.utilities.showToast(`You have stopped following @${f.username}`);
            //actual request to stop following the user is made here
            this.userService.unfollow(f.user_id, `@${f.username}`, f.avatar_url)
                .catch(() => {
                    this.setState({ is_following: true });
                    this.utilities.showToast('Nope! Something went wrong. Still following.');
                });
        }
        else if (this.state.is_following === false) {
            //OPTIMISTIC UI UPDATES
            this.setState({ is_following: true });
            this.utilities.showToast(`You are now following @${f.username}`);
            //actual request to follow the user is now made
            this.userService.follow(f.user_id, `@${f.username}`, f.avatar_url)
                .catch(() => { //if something go es wrong, revert changes
                    this.setState({ is_following: false });
                    this.utilities.showToast('Nope! Something went wrong. You cant follow.');
                });
        }
    }

    renderAvatar(user) {
        let url = null;
        if (user.avatar_url)
            if (user.avatar_url.substr(0, 5) === 'http:') {
                url = user.avatar_url.replace('http', 'https');
            }

        return (
            <TouchableOpacity
                onPress={this.goToProfile.bind(this, user)}
                style={styles.avatar}>
                {
                    user.avatar_url ?
                        <Image
                            style={{ width: 90, height: 90, borderRadius: 45, borderWidth: 3, borderColor: '#bbb' }}
                            source={{ uri: url || user.avatar_url }}
                            resizeMode="cover"
                        />
                        :
                        <Image
                            style={{ width: 90, height: 90, borderRadius: 45, borderWidth: 3, borderColor: '#bbb' }}
                            source={require('../../imgs/default_avatar.png')}
                            resizeMode="cover"
                        />
                }
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.onePerson}>
                {this.renderAvatar(this.props.user)}
                <MyText>@{this.props.user.username}</MyText>
                <Button
                    icon={this.state.is_following ? 'user-minus' : 'user-plus'}
                    iconPack={'funnyornot'}
                    iconSize={15}
                    text={this.state.is_following ? 'Unfollow' : 'Follow'}
                    textColor={Properties.themeColor}
                    styles={styles.followButton}
                    textColor={'#999'}
                    tapped={this.followOrUnfollow.bind(this)}
                />
            </View>
        );
    }

}


const styles = {
    onePerson: {
        marginVertical: 20,
        marginLeft: 10,
        marginRight: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    followButton: {
        borderWidth: 0.5,
        backgroundColor: 'transparent',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderColor: '#bbb',
        borderRadius: 3,
        marginTop: 5,
        marginBottom: 1
    }
};
