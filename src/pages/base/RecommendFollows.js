//imports
import React, { Component } from 'react';
import { ScrollView, View } from 'react-native';

import { MyText } from '../../common';
import { RecommendedFollower } from './RecommendedFollower';


export class RecommendFollows extends Component {


    renderListOfPeople() {
        const renderingObj = [];
        for (let i = 0; i < this.props.list.length; i++) {
            const user = this.props.list[i];
            renderingObj.push(
                <RecommendedFollower user={user} key={user.user_id} />
            );
        }
        return renderingObj;
    }

    render() {
        return (
            <View style={styles.allView}>
                <MyText styles={{ color: '#bbb' }}>SOME FUNNY PEOPLE</MyText>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    {this.renderListOfPeople()}
                </ScrollView>
            </View>
        );
    }

}



const styles = {
    allView: {
        backgroundColor: '#eee',
        paddingHorizontal: 10,
        paddingTop: -7,
        paddingBottom: 5
    }
};

