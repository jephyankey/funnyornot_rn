//imports
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    changeUsersCountry,
} from '../../actions';

import { MyText, Properties } from '../../common';

import { Utilities } from '../../SERVICES';


class AffiliateCountry extends Component {


    state = {
        isChecked: this.props.isChecked,
    }

    componentWillMount() {
        this.utilities = new Utilities();
    }


    changeCheckedState() {
        //change setting, if it was checked (1), now set it to 0, vice versa
        const val = this.state.isChecked === '1' ? '0' : '1';

        if (val === '0') //attempting to remove 
            if (this.props.data.country_code === this.props.own_country) {
                this.utilities.showToast(`You cannot uncheck ${this.props.data.country}`);
                return;
            }

        this.setState({ isChecked: val });
        //add to redux steeting story
        if (val === '1') { //add to list
            this.props.updateAffiliateCountries(this.props.data.country_code, ADD_TO_AFFILIATES);
        }
        else {
            this.props.updateAffiliateCountries(this.props.data.country_code, REMOVE_FROM_AFFILIATES);
        }
    }



    render() {
        const { item, text, checkbox } = styles;

        return (
            <TouchableOpacity style={item} onPress={this.changeCheckedState.bind(this)}>
                <View style={{ flex: 1 }}>
                    <MyText styles={text}>{this.props.data.country}</MyText>
                </View>
                <View style={checkbox}>
                    {
                        this.state.isChecked === '1' ?
                            <Icon name={'check-box'} color={Properties.darkBlue} size={24} />
                            :
                            <Icon name={'check-box-outline-blank'} color={'#aaa'} size={24} />
                    }
                </View>
            </TouchableOpacity >
        );
    }
}



const styles = {
    item: {
        paddingTop: 14,
        paddingBottom: 14,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    text: {
        color: '#000',
        fontSize: 17
    },
    checkbox: {
        paddingRight: 15,
        paddingLeft: 20
    }
};


function mapStateToProps(state) {
    return {
        own_country: state.settings.user_country
    };
}

export default connect(mapStateToProps, { updateAffiliateCountries })(AffiliateCountry);
