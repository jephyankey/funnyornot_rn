import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import MyVideoPlayer from '../../nativecomponents/VideoPlayer';

export class VideoPlayer extends Component {

    constructor(props) {
        super(props);
        this.old = 1;
    }

    state = {
        player_state: 'TRUE',
    }

    event(event) {
        // console.log("EVENT:", event.nativeEvent);
    }

    videoTapped() {
        if (this.state.player_state === 'PLAY')
            this.setState({ player_state: 'PAUSE' });
        else 
            this.setState({ player_state: 'PLAY' });
    }

    render() {
        return (
            <TouchableOpacity onPress={this.videoTapped.bind(this)}>
                <MyVideoPlayer
                    state={this.state.player_state}
                    width={this.props.width}
                    height={this.props.height}
                    source={this.props.source}
                    onChange={this.event.bind(this)}
                />
            </TouchableOpacity>
        );
    }
}
