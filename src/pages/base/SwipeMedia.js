import React, { Component } from 'react';
import { Modal, View, Platform } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import Swiper from 'react-native-swiper';

import { FeedCardVideo, FeedCardImage } from '../base';
import { Button } from '../../common';
import { TAB_OR_VIEW_CHANGED } from '../../types';
import { EventEmitter } from '../../SERVICES';



export class SwipeMedia extends Component {

    scrolled() {
        EventEmitter.dispatch(TAB_OR_VIEW_CHANGED);
    }

    
    /**
     * props: data, modal_visible, initialSlideId, onClose
     */
    render() {
        const renderingObj = [];
        let initial = 0;
        let d = null;
        for (let i = 0; i < this.props.data.length; i++) {
            d = this.props.data[i];
            if (d.post_id === this.props.initialSlideId)
                initial = i;
            if (d.media_type === 'i')
                renderingObj.push(<View key={d.post_id} style={styles.slideItem}><FeedCardImage feedItem={d} /></View>);
            else if (d.media_type === 'v') {
                renderingObj.push(
                    <View key={d.post_id} style={styles.slideItem}>
                        <FeedCardVideo key={d.post_id} feedItem={d} />
                    </View>
                );
            }
        }

        return (
            <Modal
                animationType={'slide'}
                transparent
                visible={this.props.modalVisible}
                onRequestClose={this.props.onClose}
                >
                <View style={styles.modalBack}>
                    <Swiper
                        onMomentumScrollEnd={this.scrolled.bind(this)}
                        showButtons={false}
                        index={initial}
                        showsPagination={false}
                        loop={false}>
                        {renderingObj}
                    </Swiper>
                    <View style={styles.modalHeader}>
                        <Button
                            iconOnly
                            icon={Platform.OS === 'android' ? 'md-close-circle' : 'ios-close-circle'}
                            iconSize={35}
                            textColor={'#999'}
                            styles={styles.closeButton}
                            tapped={this.props.onClose}
                            />
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = {
    modalBack: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.9)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalHeader: {
        position: 'absolute',
        flex: 1,
        top: 5,
        right: 5
    },
    slideItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    closeButton: {
        backgroundColor: 'transparent',
    },
    videoIcon: {
        position: 'absolute',
        left: 10,
        top: 30,
    }
};

