import React, { Component } from 'react';
import { Image, TouchableOpacity, TouchableWithoutFeedback, View, Platform } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import { Properties } from '../../common';
import { Utilities } from '../../SERVICES';


export class Avatar extends Component {


    constructor(props) {
        super(props);
        this.utilities = new Utilities();
    }

    styleAvatar() {
        if (this.props.forPeople)
            return styles.avatarContainer2;
        return styles.avatarContainer;
    }


    renderDefaultAvatar() {
        const s = this.props.size;
        if (this.props.forProfile) {
            return (
                <Image
                    style={{ width: s, height: s, borderRadius: s / 2 }}
                    source={require('../../imgs/default_avatar.png')}
                    resizeMode="cover"
                />
            );
        }
        return (
            <Image
                style={{ width: s, height: s, borderRadius: s / 2 }}
                source={require('../../imgs/default_avatar.png')}
                resizeMode="cover"
            />
        );
    }

    render() {
        const s = this.props.size;
        if (this.props.forProfile) {
            return (
                <TouchableWithoutFeedback onPress={this.props.onTap}>
                    <View style={styles.imageBox} >
                        {
                            this.props.url ?
                                <Image
                                    style={{ width: s, height: s, borderRadius: s / 2 }}
                                    source={{ uri: this.utilities.httpsFromHttp(this.props.url) }}
                                    resizeMode="cover"
                                />
                                :
                                this.renderDefaultAvatar()
                        }
                    </View>
                </TouchableWithoutFeedback>
            );
        }

        return (
            <TouchableOpacity
                onPress={this.props.onTap}
                style={{
                    ...this.styleAvatar(),
                    width: s,
                    borderRadius: s / 2,
                }}>
                {
                    this.props.url ?
                        <Image
                            style={{ width: s, height: s, borderRadius: s / 2 }}
                            source={{ uri: this.utilities.httpsFromHttp(this.props.url) }}
                            resizeMode="cover"
                        />
                        :
                        this.renderDefaultAvatar()
                }
            </TouchableOpacity>
        );
    }
}


const styles = {
    avatarContainer: {
        backgroundColor: Properties.greyBackColor,
        marginRight: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarContainer2: {
        backgroundColor: Properties.greyBackColor,
        justifyContent: 'center',
        alignItems: 'center'
    },
    nullAvatar: {
        backgroundColor: '#fff',
        marginRight: 15,
    },
    imageBox: {
        // width and height: 161, ie avatar dimensions + (width of border *2)
        width: 161,
        height: 161,
        borderWidth: 3,
        borderColor: '#eee',
        borderRadius: 80.5,
        padding: 0,
        backgroundColor: '#ccc',
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        width: 155,
        height: 155,
        borderRadius: 77.5
    },
};


