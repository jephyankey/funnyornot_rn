import React, { Component } from 'react';
import {
    View, Image, Dimensions, Platform,
    TouchableWithoutFeedback, TouchableOpacity
} from 'react-native';
import Video from 'react-native-video';

import Icon2 from 'react-native-vector-icons/Ionicons';
import Icon8 from '../../common/Icon';
import { Spinner, Properties } from '../../common';

import {
    EventEmitter,
    Utilities,
    Config,
    FileService,
    ShareService
} from '../../SERVICES';

import {
    VIDEO_JUST_STARTED_PLAYING, SCROLLING_THROUGH_FEED,
    APP_TO_BACKGROUND, APP_TO_FOREGROUND, TAB_OR_VIEW_CHANGED
} from '../../types';


//when using Prince's player
//these states can actually be sent to the native palyer to effect the state
const VIDEO_PAUSED = 'PAUSE';
const VIDEO_PLAYING = 'PLAY';
const VIDEO_STOPPED = 'STOP';

const VIDEO_READY = 'video_ready';
const VIDEO_LOADING = 'loading';
const EXTERNAL_PLAYER_LAUNCHED = 'external_player';
const VIDEO_PREPARING = 'preparing';

//media errors
const IO_ERROR = 'MEDIA IO ERROR';




export class FeedCardVideo extends Component {

    width: number = 0;
    height: number = 0;

    constructor(props) {
        super(props);
        this.config = new Config();
        this.utilities = new Utilities();
        this.fileService = new FileService(this.config, this.utilities);
        this.shareService = new ShareService();
    }

    state = {
        id: Math.random(),
        video_state: VIDEO_READY,
        app_gone_2_background: false,
        has_played_before: false,
        video_url: null,
        video_released: false,
        error: false,

        video_paused: null
    }


    componentWillMount() {
        if (this.props.feedItem.media_type === 'v') {
            const meta = JSON.parse(this.props.feedItem.metadata);
            const { width, height } = this.utilities.resizeImage(meta.width, meta.height);
            this.width = width;
            this.height = height;
        }

        this.another_vid_playing_subsc = EventEmitter.subscribe(VIDEO_JUST_STARTED_PLAYING, (playing_id) => {
            if (this.state.video_state === VIDEO_PLAYING) {
                if (playing_id !== this.state.id) { //this is not the video that just started playing
                    this.setState({ video_state: VIDEO_PAUSED, video_paused: true });
                }
            }
        });

        //subscribe to the video_scrolling EventEmitter
        //this way we can check if this video is out of view and pause it,
        //from memory anyway when they are offscreen
        this.scrolling_feed_subsc = EventEmitter.subscribe(SCROLLING_THROUGH_FEED, () => {
            if (this.state.video_state === VIDEO_PLAYING || this.state.video_state === VIDEO_PREPARING
                || this.state.video_state === VIDEO_LOADING || this.state.VIDEO_PAUSED) {
                this.videoContainerView.measure((x, y, width, height, px, py) => {
                    //pause the video as soon as a bit of it (20%) is offscreen, either above or below
                    if (py < -1 * 0.2 * height || py > Dimensions.get('window').height - (0.8 * height)
                        || px < -1 * 0.2 * width || px > Dimensions.get('window').width - (0.8 * width)) {
                        if (this.state.video_state === VIDEO_PLAYING)
                            this.setState({ video_state: VIDEO_PAUSED, video_paused: true });
                    }
                    //hide the video (so the pause button is shown and memory is freed) when it is totally offscreen
                    if (py <= -1 * height || py >= Dimensions.get('window').height
                        || px <= -1 * width || px >= Dimensions.get('window').width) {
                        if (this.state.video_state === VIDEO_PAUSED || this.state.video_state === VIDEO_PLAYING
                            || this.state.video_state === VIDEO_STOPPED) {
                            // this.setState({ video_released: true }); 
                            this.setState({ video_state: VIDEO_READY, has_played_before: false, video_paused: true });
                        }
                    }
                });
            }
        });


        this.app_to_back_subsc = EventEmitter.subscribe(APP_TO_BACKGROUND, () => {
            if (this.state.video_state === VIDEO_PLAYING) {
                //if using react-native-video, then vid will automatically he VIDEO_STOPPED
                //when app goes to background
                this.setState({ video_state: VIDEO_STOPPED });
            }
            else if (this.state.video_state === VIDEO_LOADING) {
                this.setState({ app_gone_2_background: true });
            }
        });

        this.app_to_fore_subsc = EventEmitter.subscribe(APP_TO_FOREGROUND, () => {
            this.setState({ video_state: VIDEO_READY, app_gone_2_background: false });
        });

        this.tab_or_view_changed_subsc = EventEmitter.subscribe(TAB_OR_VIEW_CHANGED, () => {
            if (this.state.video_state === VIDEO_PLAYING) {
                this.setState({ video_state: VIDEO_PAUSED, video_paused: true });
            }
        });


        if (this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST)
            this.playFirstTime();
        //check if the file already exists on disk
        else {
            this.fileService.fileExistsOnDisk(this.props.feedItem.filename)
                .then(url_of_file => {
                    if (url_of_file !== 'FALSE' && Platform.Version > 18) {
                        if (this.props.setExternalVideoPlayerUrl && this.props.showPlayerIcon) {
                            this.props.setExternalVideoPlayerUrl(url_of_file);
                            //now show the icon to launch the external player
                            this.props.showPlayerIcon();
                        }
                    }
                });
        }
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(VIDEO_JUST_STARTED_PLAYING, this.another_vid_playing_subsc);
        EventEmitter.unsubscribe(SCROLLING_THROUGH_FEED, this.scrolling_feed_subsc);
        EventEmitter.unsubscribe(APP_TO_BACKGROUND, this.app_to_back_subsc);
        EventEmitter.unsubscribe(APP_TO_FOREGROUND, this.app_to_fore_subsc);
        EventEmitter.unsubscribe(TAB_OR_VIEW_CHANGED, this.tab_or_view_changed_subsc);
    }


    playFirstTime() {
        if (this.state.video_state === VIDEO_LOADING) /*IMPORTANT, SOMETIMES EVEN CRASH WITHOUT THIS*/
            return;  /** CASE: the video is downloading but the confused/crazy/impatient/etc/:) user keeps tapping it */

        if (this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST) {
            //if there is no filename for some reason, (e.g. preview of a new post), then load video directly
            this.setState({ video_url: this.props.feedItem.media_url, video_state: VIDEO_PREPARING });
        }
        else {
            this.setState({ video_state: VIDEO_LOADING });
            this.fileService.getMediaFile(this.props.feedItem.media_url, this.props.feedItem.filename)
                .then(url => {
                    //for android versions 4.4 and up
                    if (Platform.Version > 18) {
                        if (this.props.setExternalVideoPlayerUrl && this.props.showPlayerIcon) {
                            //set the url that will be used to launch the external video player,
                            //provided the props was passed down
                            this.props.setExternalVideoPlayerUrl(url);
                            //now show the icon to launch the external player
                            this.props.showPlayerIcon();
                        }
                        //play the video if it is still in view, video playback will trigger change 
                        //of state to VIDEO_PLAYING once it starts
                        this._playVideoIfInView(() => {
                            this.setState({ video_url: url, video_state: VIDEO_PREPARING, video_paused: false });
                        });
                    }
                    else { //for android versions 4.3 and below, launch external player
                        this._playVideoIfInView(() => {
                            if (this.props.feedItem.media_type === 'v')
                                this.shareService.launchExternalPlayer(url);
                            else
                                this.shareService.launchExternalPlayerAudio(url);
                            setTimeout(() => {
                                // timeout means we wait for external player to show, before changing the loader to the player icon
                                //makes for a better ux
                                this.setState({ video_state: EXTERNAL_PLAYER_LAUNCHED });
                            }, 500);
                        });
                    }
                })
                .catch(() => {
                    if (Platform.Version > 18) {
                        /**
                         * if we are unable to obtain file, set the video url directly to the 
                         * server url and allow the video player to handle it
                         */
                        //remember that in this case we do not need to set the external player url first
                        //cos the FeedCard component already sets the http url as the default
                        this._playVideoIfInView(() => {
                            this.setState({ video_url: this.props.feedItem.media_url, video_state: VIDEO_PREPARING, video_paused: false });
                            if (this.props.showPlayerIcon) {
                                this.props.showPlayerIcon();
                            }
                        });
                    }
                    else {
                        //for android versions 4.3 and below, launch external player
                        this._playVideoIfInView(() => {
                            if (this.props.feedItem.media_type === 'v')
                                this.shareService.launchExternalPlayer(this.props.feedItem.media_url);
                            this.shareService.launchExternalPlayerAudio(this.props.feedItem.media_url);
                            setTimeout(() => {
                                //timeout means we wait for external player to show, before changing the loader to the player icon
                                //makes for a better ux
                                this.setState({ video_state: EXTERNAL_PLAYER_LAUNCHED });
                            }, 500);
                        });
                    }
                });
        }
    }

    _playVideoIfInView(callback: Function) {
        this.videoContainerView.measure((x, y, width, height, px, py) => {
            // please note: if the video is completely out of view,
            // by the time this function is called, py, py, height, etc.. will all be undefined
            // cos the ListView will have removed it from the UI

            //if more than 70% of the video is offscreen, or the app has since moved into the background, do not play
            if (py === undefined || py < -1 * 0.7 * height || (py > Dimensions.get('window').height - (0.3 * height))
                || this.state.app_gone_2_background) {
                this.setState({ video_state: VIDEO_READY, video_paused: true });
            }
            else {
                callback(); /** this callback shoulf cause the video to start playing which shd set the new state to VIDEO_PLAYING */
            }
        });
    }
    /**
     * called by the video when it starts i.e. onStart received
     */
    videoStarted() {
        this.setState({ video_state: VIDEO_PLAYING, has_played_before: true });
        this._warnOtherVideostoStop();
    }
    _warnOtherVideostoStop() {
        EventEmitter.dispatch(VIDEO_JUST_STARTED_PLAYING, this.state.id);
    }


    videoTapped() {
        if (this.state.has_played_before)
            this.playOrPause();
        else
            this.playFirstTime();
    }

    videoError() {
        this.setState({ video_state: VIDEO_READY });
        this.utilities.showToast('Could not load the video!');
    }

    playOrPause() {
        //if we are about to play the video, then warn others (except the one about to play) to PAUSE themeselves
        if (this.state.video_state !== VIDEO_PLAYING) {
            this._warnOtherVideostoStop();
        }

        this.setState({
            video_paused: this.state.video_state === VIDEO_PLAYING ? true : false,
            video_state: this.state.video_state === VIDEO_PLAYING ? VIDEO_PAUSED : VIDEO_PLAYING,
        });
    }


    renderVideo() {
        if (this.state.video_state === VIDEO_PREPARING || this.state.video_state === VIDEO_PLAYING
            || this.state.video_state === VIDEO_PAUSED || this.state.video_state === VIDEO_STOPPED
            || this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST) {
            if (this.state.video_url) {
                return (
                    <View collapsable={false}>
                        <Video
                            style={{ width: this.state.video_state === VIDEO_PREPARING ? 0 : this.width, height: this.height }}
                            source={{ uri: this.state.video_url }}
                            repeat
                            paused={this.state.video_paused}
                            resizeMode="contain"
                            onError={this.videoError.bind(this)}
                            playInBackground={false}
                            onLoad={this.videoStarted.bind(this)}
                        />
                    </View>
                );
            }
            else if (this.state.audio_url) {
                return (
                    <View collapsable={false}>
                        <Video
                            style={{ width: 0, height: 0 }}
                            source={{ uri: this.state.video_url }}
                            repeat
                            paused={this.state.video_paused}
                            resizeMode="contain"
                            onError={this.videoError.bind(this)}
                            playInBackground={false}
                            onLoad={this.videoStarted.bind(this)}
                        />
                    </View>
                );
            }
        }
    }

    renderVideoIcon() {
        if (this.state.video_state === VIDEO_READY || this.state.video_state === VIDEO_PAUSED
            || this.state.video_state === VIDEO_STOPPED)
            return (
                <View style={{ ...styles.videoIcon, left: (this.width / 2) - 27, top: (this.height / 2) - 27 }}>
                    <Icon8 name={'play-juke'} size={50} color={'#fff'} style={{ margin: 0, padding: 0, backgroundColor: 'transparent' }} />
                </View>
            );
    }

    renderLoader() {
        if (this.state.video_state === VIDEO_LOADING || this.state.video_state === VIDEO_PREPARING) {
            return (
                <View
                    style={{
                        position: 'absolute',
                        left: (Dimensions.get('window').width / 2) - (this.props.feedItem.media_type === 'v' ? 30 : 15),
                        top: this.props.feedItem.media_type === 'v' ? (this.height / 2) - 30 : 15
                    }}
                >
                    <Spinner size={this.props.feedItem.media_type === 'v' ? 50 : 25} color={'#bbb'} />
                </View>
            );
        }
        /*else if (this.state.error) {
            return (
                <View
                    style={{
                        position: 'absolute',
                        left: (Dimensions.get('window').width / 2) - (this.props.feedItem.media_type === 'v' ? 27 : 15),
                        top: this.props.feedItem.media_type === 'v' ? (this.height / 2) - 27 : 15,
                        padding: -4
                    }}
                >
                    <Icon name={'error'} size={this.props.feedItem.media_type === 'v' ? 60 : 30} color={'#999'} />
                </View>
            );
        }*/
    }

    renderMedia() {
        if (this.props.feedItem.media_type === 'v') {
            if (this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST) {
                /**
                 * this is teh case where a video has been loaded and HAS NOT YET BEEN POSTED.
                 * we show the preview by loading the local filename
                 * remember this is different from the post preview in which the user is shown 
                 * a preview version of his post
                 */
                return (
                    <TouchableWithoutFeedback onPress={this.videoTapped.bind(this)}>
                        <View>{this.renderVideo()}</View>
                    </TouchableWithoutFeedback>
                );
            }
            return (
                <TouchableWithoutFeedback onPress={this.videoTapped.bind(this)}>
                    <View ref={(view) => { this.videoContainerView = view; }} style={{ backgroundColor: Properties.greyBackColor }}>
                        <Image
                            style={{ width: this.width, height: this.height }}
                            source={{ uri: this.props.feedItem.poster_image }}
                        >
                            {this.renderVideo()}
                            {this.renderVideoIcon()}
                        </Image>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        //audio
        return (
            <View
                ref={(view) => { this.videoContainerView = view; }}
                style={{
                    ...styles.audioView,
                    borderTopColor: this.state.video_state === VIDEO_PLAYING ? Properties.themeColor : '#ccc'
                }}>
                <TouchableOpacity onPress={this.videoTapped.bind(this)}>
                    <Icon2 name={this.state.video_state === VIDEO_PLAYING ? 'ios-pause' : 'ios-play'} size={30} color={'#ccc'} />
                </TouchableOpacity>
                {this.renderVideo()}
                <Icon2 name={'ios-musical-notes'} size={30} color={'#ccc'} />
            </View>
        );
    }

    render() {
        return (
            <View
                style={{
                    width: Dimensions.get('window').width,
                    alignItems: 'center'
                }}>
                {this.renderMedia()}
                {this.renderLoader()}
            </View>
        );
    }

}


const styles = {
    videoIcon: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderRadius: 30,
    },
    audioView: {
        borderTopWidth: 2,
        borderTopColor: '#ccc',
        paddingTop: 5,
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimensions.get('window').width - 80,
    }
};
