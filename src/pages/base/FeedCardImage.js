import React, { Component } from 'react';
import { Image } from 'react-native';

import { Utilities } from '../../SERVICES';



export class FeedCardImage extends Component {

    render() {
        const utilities = new Utilities();
        const meta = JSON.parse(this.props.feedItem.metadata);
        const { width, height } = utilities.resizeImage(meta.width, meta.height);

        return (
            <Image
                style={{ width, height, backgroundColor: '#efefef' }}
                source={{ uri: this.props.feedItem.media_url }}
                resizeMode="contain"
            />
        );
    }
}
