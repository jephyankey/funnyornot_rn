//imports
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon8 from '../../common/Icon';
import { Actions } from 'react-native-router-flux';


import { Properties } from '../../common';
import { FeedCardImage, FeedCardVideo, Avatar } from '../base';
import FeedCardFooter from '../base/FeedCardFooter';

import {
    AvatarService, iAvatarService,
    Utilities,
    DatabaseService,
    Config,
    UserService,
    ShareService,
    EventEmitter
} from '../../SERVICES';
import { NEW_THEME_CHOSEN } from '../../types';


const INITIAL_TEXT_HEIGHT = 350;



export class FeedCard extends Component {

    avatarService: iAvatarService;
    random_avatar_url: string;

    constructor(props) {
        super(props);
        const config = new Config();
        this.utilities = new Utilities();
        const dbService = new DatabaseService(this.utilities);
        this.avatarService = new AvatarService(config, this.utilities, dbService);
        this.userService = new UserService(config, this.utilities, dbService);
        this.shareService = new ShareService();
    }

    state = {
        // starred: null,
        is_following: this.props.feedItem.feed !== 'explore',
        show_player_icon: null,
        url_for_external_player: this.props.feedItem.media_url, //this is the url that will be used when the external player
        // is launched. it is either the file url or the http url(set here by default). It needs to be set by the video componentDidMount
        //hence a prop should be passed down to the video component to set this as soon as it downloads the video
        //if the video fails to download, then it shd not be set by the video component, and hence 
        // this component will use the default http url

        show_text_overlay: 'NOT-YET', //can have values 0. NOT-YET, 1. SHOW, 2. NEVER
        // 0 - should not be shown (cos page not completed rendering)
        // 1 - should be shown
        // 2 - should not be shown ever again (user clicked to expand)
        text_height: INITIAL_TEXT_HEIGHT
    }


    componentDidMount() {
        this.setState({ starred: this.props.feedItem.stars === 3 });

        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
            this.forceUpdate();
        });
    }
    componentWillUnmount() {
        //unsubscribe from theme update
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
    }



    truncateName(name) {
        return (name.length > 26) ? `${name.substr(0, 24)}...` : name;
    }

    whichNameToShow() {
        if (this.props.feedItem.reposted || !(this.props.feedItem.name))
            return this.truncateName(`@${this.props.feedItem.username}`);
        return this.truncateName(this.props.feedItem.name);
    }

    repostedOrNot() {
        if (this.props.feedItem.reposted) {
            return (
                <View style={styles.feedcardHeader}>
                    <Icon name={'md-repeat'} color={'#ccc'} size={20} />
                    <Text style={styles.repostedThis}>
                        {`${this.truncateName(this.props.feedItem.name)} reposted this`}
                    </Text>
                </View>
            );
        }
    }

    goToProfile() {
        if (this.props.feedItem.feed === 'profile') {
            this.utilities.showToast('Hmmm.... Look around, You are already where you are trying to go!');
        }
        else {
            Actions.profilePage({
                profile_id: this.props.feedItem.poster_id,
                avatar_url: this.props.feedItem.avatar_url,
                name: this.props.feedItem.reposted || this.props.feedItem.feed === 'recommendations' ?
                    `@${this.props.feedItem.username}` : this.props.feedItem.name
            });
        }
    }

    playerIcon() {
        return (
            <TouchableOpacity onPress={this.launchExternalPlayer.bind(this)}>
                <Icon8 name={'expand2'} size={30} color={'#ccc'} style={[styles.indicator, { padding: 0, margin: 0 }]} />
            </TouchableOpacity>
        );
    }

    showPlayerIcon() {
        this.setState({ show_player_icon: true });
    }

    launchExternalPlayer() {
        this.shareService.launchExternalPlayer(this.state.url_for_external_player);
    }

    /**
     * prop passed down to video component to set the url for the external player
     * provided it downloads teh file successfully
     */
    setExternalVideoPlayerUrl(new_url) {
        this.setState({ url_for_external_player: new_url });
    }

    renderMedia() {
        if (this.props.feedItem.media_url) {
            /* this condition (ABOVE) is here cos i realixed some posts are tagged as images
            Even though they do not have a url. INVESTIGATE!!!*/
            if (this.props.feedItem.media_type === 'i') {
                return (
                    <View style={styles.media}>
                        <TouchableWithoutFeedback>
                            <FeedCardImage feedItem={this.props.feedItem} />
                        </TouchableWithoutFeedback>
                    </View>
                );
            }
            else if (this.props.feedItem.media_type === 'v' || this.props.feedItem.media_type === 'a') {
                return (
                    <View style={styles.media}>
                        <FeedCardVideo
                            feedItem={this.props.feedItem}
                            showPlayerIcon={this.showPlayerIcon.bind(this)}
                            setExternalVideoPlayerUrl={this.setExternalVideoPlayerUrl.bind(this)}
                        />
                    </View>
                );
            }
        }
    }


    renderText() {
        if (this.props.feedItem.post_text) {
            return (
                <View style={[styles.feedTextView, { maxHeight: this.state.text_height }]}>
                    <Text style={styles.feedText} onLayout={this._layoutTextWithProperHeight.bind(this)}>
                        {decodeURIComponent(this.props.feedItem.post_text)}
                    </Text>
                    {this._renderTextOverlay()}
                </View>
            );
        }
    }
    _renderTextOverlay() {
        if (this.state.show_text_overlay === 'SHOW') {
            return (
                <TouchableWithoutFeedback onPress={this._showFullText.bind(this)}>
                    <View style={styles.textOverlay}>
                        <View>
                            <View style={{ height: 10, opacity: 0.65, backgroundColor: 'white' }} />
                            <View style={{ height: 25, opacity: 0.85, backgroundColor: 'white' }} />
                            <View style={{ height: 25, opacity: 0.90, backgroundColor: 'white' }} />
                            <View style={{ height: 35, opacity: 0.95, backgroundColor: 'white' }} />
                        </View>
                        <View style={styles.expandIconView}>
                            <Icon name={'md-arrow-down'} size={20} color={Properties.themeColor} />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        return null;
    }
    _layoutTextWithProperHeight(event) {
        const { height } = event.nativeEvent.layout;
        if (this.state.show_text_overlay === 'NOT-YET' && height > this.state.text_height) {
            this.setState({ show_text_overlay: 'SHOW' });
        }
    }
    _showFullText() {
        this.setState({ show_text_overlay: 'NEVER', text_height: null });
    }


    followButton() {
        if (!this.state.is_following)
            return (
                <TouchableOpacity onPress={this.followOrUnfollow.bind(this)} style={styles.followButton}>
                    <Icon8 name={'user-plus'} size={20} color={'#ccc'} />
                </TouchableOpacity>
            );
        return (
            <TouchableOpacity onPress={this.followOrUnfollow.bind(this)} style={styles.unfollowButton}>
                <Icon8 name={'user-minus'} size={20} color={'#fff'} />
            </TouchableOpacity>
        );
    }

    followOrUnfollow() {
        const f = this.props.feedItem;
        if (f.name === 'You') {
            this.utilities.showToast('Why are you trying to follow yourself? Are you okay?');
        }
        else if (this.state.is_following === true) {
            //OPTIMISTIC UI UPDATES
            this.setState({ is_following: false });
            // this.updateFollowers('sub');
            this.utilities.showToast(`You have stopped following ${f.name}`);
            //actual request to stop following the user is made here
            this.userService.unfollow(f.poster_id, f.name, f.avatar_url)
                .catch(() => {
                    this.setState({ is_following: true });
                    this.updateFollowers('add');
                    this.utilities.showToast('Nope! Something went wrong. Still following.');
                });
        }
        else if (this.state.is_following === false) {
            //OPTIMISTIC UI UPDATES
            this.setState({ is_following: true });
            // this.updateFollowers('add');
            this.utilities.showToast(`You are now following ${f.name}`);
            //actual request to follow the user is now made
            this.userService.follow(f.poster_id, f.name, f.avatar_url)
                .catch(() => { //if something go es wrong, revert changes
                    this.setState({ is_following: false });
                    this.updateFollowers('sub');
                    this.utilities.showToast('Nope! Something went wrong. You cant follow.');
                });
        }
    }


    renderHeader() {
        const f = this.props.feedItem.feed;
        const {
            feedcardHeader, feedcardHeaderTrend, indicator,
            nameView, rank,
            name, date, indicatorsView,
        } = styles;

        return (
            <View style={f === 'trending' || f === 'rank-alltime' ? feedcardHeaderTrend : feedcardHeader}>
                <View>
                    {
                        f === 'trending' || f === 'rank-alltime' ?
                            <View>
                                <Text style={rank}>{this.props.feedItem.rank}</Text>
                            </View>
                            :
                            <Avatar size={45} url={this.props.feedItem.avatar_url} onTap={this.goToProfile.bind(this)} />

                    }
                </View>
                <View style={nameView}>
                    <TouchableOpacity onPress={this.goToProfile.bind(this)}>
                        <Text style={name}>{this.whichNameToShow()}</Text>
                    </TouchableOpacity>
                    <Text style={date}>{this.props.feedItem.how_long_ago}</Text>
                </View>
                <View style={indicatorsView}>
                    {/*{this.state.starred ?
                        <Icon name={'md-star-outline'} size={25} color={'#aaa'} style={indicator} /> : null}*/}
                    {f === 'explore' ? this.followButton() : null}
                    {this.state.show_player_icon ? this.playerIcon() : null}
                    {this.props.feedItem.NEW_POST ?
                        <Icon name={'md-done-all'} size={25} color={'green'} style={indicator} /> : null}
                </View>
            </View>
        );
    }


    render() {
        const { feedcard, feedcardTrend, feedcardContent, feedcardAlltime } = styles;

        return (
            <View
                style={this.props.feedItem.feed === 'trending' ?
                    feedcardTrend : this.props.feedItem.feed === 'rank-alltime' ? feedcardAlltime : feedcard}
            >
                {this.repostedOrNot()}
                {this.renderHeader()}
                <View style={feedcardContent}>
                    {this.renderMedia()}
                    {this.renderText()}
                </View>
                <FeedCardFooter feedItem={this.props.feedItem} />
            </View>
        );
    }
}



const styles = {
    feedcard: {
        paddingTop: 13,
        paddingBottom: 13,
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginBottom: 15,
        overflow: 'hidden'
    },
    feedcardTrend: {
        paddingBottom: 13,
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginBottom: 15,
        overflow: 'hidden'
    },
    feedcardAlltime: {
        paddingBottom: 13,
        flexDirection: 'column',
        backgroundColor: '#fff',
        marginBottom: 15,
        overflow: 'hidden'
    },
    feedcardHeader: {
        marginLeft: 16,
        marginRight: 16,
        flexDirection: 'row',
    },
    feedcardHeaderTrend: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 16,
        paddingRight: 16,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Properties.greyBackColor
    },
    nameView: {
        flexDirection: 'column',
        flex: 1,
    },
    name: {
        fontWeight: 'bold',
        color: '#000',
        fontSize: 14,
        fontFamily: Platform.OS === 'android' ? 'Roboto' : null,
    },
    date: {
        fontFamily: Platform.OS === 'android' ? 'Roboto' : null,
        color: '#888',
        fontSize: 12,
    },
    indicatorsView: {
        alignSelf: 'center',
        marginRight: 3,
        flexDirection: 'row',
        alignItems: 'center'
    },
    indicator: {
        marginLeft: 15
    },
    feedcardContent: {
        marginTop: 10,
        backgroundColor: 'white'
    },
    media: {
        alignItems: 'center',
        marginBottom: 10
    },
    feedText: {
        fontFamily: Platform.OS === 'android' ? 'Roboto' : null,
        color: '#000',
        fontSize: 16,
        lineHeight: 24,
        paddingLeft: 16,
        paddingRight: 16,
    },
    feedTextView: {
        overflow: 'hidden',
        marginBottom: 10,
        backgroundColor: 'white'
    },
    repostedThis: {
        fontFamily: Platform.OS === 'android' ? 'Roboto' : null,
        color: '#aaa',
        fontSize: 14,

        marginBottom: 10,
        marginLeft: 8
    },
    rank: {
        fontFamily: Platform.OS === 'android' ? 'Roboto' : null,
        color: '#ccc',
        fontWeight: 'bold',
        fontSize: 50,
        marginRight: 15
    },
    followButton: {
        borderWidth: 0.7,
        borderColor: '#ccc',
        borderRadius: 4,
        padding: 4,
        marginLeft: 10,
        height: 30,
        width: 30

    },
    unfollowButton: {
        borderRadius: 4,
        padding: 5,
        backgroundColor: '#ddd',
        marginLeft: 10
    },
    textOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: INITIAL_TEXT_HEIGHT,
        justifyContent: 'flex-end'
    },
    expandIconView: {
        position: 'absolute',
        bottom: 20,
        left: 0,
        right: 0,
        alignItems: 'center'
    }
};
