//imports
import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';

import { MyText, Properties } from '../../common';
import Icon from '../../common/Icon';


export class RadioItem extends Component {

    propTypes: {
        text: React.PropTypes.string.isRequired,
        subText: React.PropTypes.string,
        isChecked: React.PropTypes.string.isRequired,  //string of '1' or '0'
        setting: React.PropTypes.string.isRequired,
        onTap: React.PropTypes.func,
        serverRoute: React.PropTypes.boolean, //whether the setting should be saved to the server
    }


    state = {
        isChecked: this.props.isChecked
    }


    componentWillReceiveProps(nextProps) {
        /**
         * setting state here ensures that when the prop is updated,
         * the state is updated as well
         */
        this.setState({ isChecked: nextProps.isChecked });
    }


    render() {
        const { item, text, subText, checkbox } = styles;

        return (
            <TouchableOpacity style={[item, this.props.highlight ? { backgroundColor: Properties.greyBackColor } : null]} onPress={this.props.onTap}>
                <View style={{ flex: 1 }}>
                    <MyText styles={text}>{this.props.text}</MyText>
                    {
                        this.props.subText ?
                            <MyText styles={subText}>{this.props.subText}</MyText> :
                            null
                    }
                </View>
                <View style={checkbox}>
                    {
                        this.state.isChecked === true ?
                            <Icon name={'radio-button-checked'} color={this.props.checkedColor} size={27} /> :
                            <Icon name={'radio-button-unchecked'} color={'#aaa'} size={27} />
                    }
                </View>
            </TouchableOpacity >
        );
    }
}


RadioItem.defaultProps = {
    checkState: false,
    text: 'Provide Text',
    subText: null,
};



const styles = {
    item: {
        paddingTop: 18,
        paddingBottom: 18,
        paddingLeft: 10,
        borderBottomWidth: 0.4,
        borderBottomColor: '#ddd',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    text: {
        color: '#000',
        fontSize: 17
    },
    subText: {
        color: '#aaa',
        fontSize: 15
    },
    checkbox: {
        paddingRight: 15,
        paddingLeft: 20
    }
};
