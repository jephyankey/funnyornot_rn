import React, { Component } from 'react';
import {
    View, Image, Dimensions, Platform,
    TouchableWithoutFeedback, TouchableOpacity, WebView
} from 'react-native';

import Icon2 from 'react-native-vector-icons/Ionicons';
import Video from 'react-native-video';
import { Spinner, Properties } from '../../common';
import Icon from '../../common/Icon';

import {
    EventEmitter,
    Utilities,
    Config,
    FileService,
    ShareService
} from '../../SERVICES';

import { REMOVE_ALL_VIDEOS, SCROLLING_THROUGH_FEED } from '../../types';

export class FeedCardVideo extends Component {

    width: number = 0;
    height: number = 0;

    constructor(props) {
        super(props);
        this.config = new Config();
        this.utilities = new Utilities();
        this.fileService = new FileService(this.config, this.utilities);
        this.shareService = new ShareService();
    }

    state = {
        id: Math.random(),
        has_played_before: false,
        load_video: false,
        show_video: false,
        loading_video: 'false', //values are 'false', 'true', 'loaded'
        video_paused: false,
        video_url: null,
        video_playing: false,
        removed_video: true, /** this variable is used to remove the video from memory using setState
        it was mainly introduced for for the profile page,
        cos other pages use a list view which takes care of removing the video automatically
        this is set to true first, cos if the video has not even started playing,
        then it is assumed to be removed */
        /** now it might not be necessary cos the profile page also uses a ListView now */
        error: false
    }

    componentWillMount() {
        if (this.props.feedItem.media_type === 'v') {
            const meta = JSON.parse(this.props.feedItem.metadata);
            const { width, height } = this.utilities.resizeImage(meta.width, meta.height);
            this.width = width;
            this.height = height;
        }

        this.remove_videos_subscription = EventEmitter.subscribe(REMOVE_ALL_VIDEOS, (playing_id) => {
            if (this.state.video_playing) {
                if (playing_id !== this.state.id) {
                    //the video that has started playing is different from this video
                    //pause this video if it is playing, and just remove it altogether
                    this.setState({ video_paused: true, video_playing: false, removed_video: true });
                }
            }
        });

        //subscribe to the video_scrolling EventEmitter
        //this way we can check if this video is out of view and pause it,
        //from memory anyway when they are offscreen
        this.scrolling_feed_subscription = EventEmitter.subscribe(SCROLLING_THROUGH_FEED, () => {
            if (this.state.video_playing || !this.state.removed_video) {
                this.videoView.measure((x, y, width, height, px, py) => {
                    //pause the video as soon as a bit of it (20%) is offscreen, either above or below
                    if (py < -1 * 0.2 * height || py > Dimensions.get('window').height - (0.8 * height)
                        || px < -1 * 0.2 * width || px > Dimensions.get('window').width - (0.8 * width)) {
                        this.setState({ video_paused: true, video_playing: false });
                    }
                    //hide the video (so the pause button is shown and memory is freed) when it is totally offscreen
                    if (py <= -1 * height || py >= Dimensions.get('window').height
                        || px <= -1 * width || px >= Dimensions.get('window').width){
                        this.setState({ removed_video: true });
                    }
                });
            }
        });
    }

    componentDidMount() {
        if (this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST)
            this.playFirstTime();
        //check if the file already exists on disk
        else {
            if (Platform.OS === 'android') {
                this.fileService.fileExistsOnDisk(this.props.feedItem.filename)
                    .then(url_of_file => {
                        if (url_of_file !== 'FALSE' && Platform.OS === 'android' && Platform.Version > 18) {
                            this.props.setExternalVideoPlayerUrl(url_of_file);
                            //now show the icon to launch the external player
                            this.props.showPlayerIcon();
                        }
                    });
            }
        }
    }



    videoTapped() {
        if (this.state.has_played_before)
            this.playOrPause();
        else
            this.playFirstTime();
    }

    playOrPause() {
        //if we are about to play the video, then warn others (except the one about to play) to stop
        if (this.state.video_paused) {
            EventEmitter.dispatch(REMOVE_ALL_VIDEOS, this.state.id);
        }

        // also note: if we are pausing the video, then playing shd be false, and vice versa
        //therefore, video_playing is always the inverse of video paused
        this.setState({
            video_paused: !this.state.video_paused,
            video_playing: this.state.video_paused,
            removed_video: false, /**
                will help the case of a previously removed video, a user is now 
                attempting to play
                 */
        });
    }

    playFirstTime() {
        if (this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST) {
            //if there is no filename for some reason, (e.g. preview of a new post), then load video directly
            this.setState({ video_url: this.props.feedItem.media_url, removed_video: false });
        }
        else {
            this.setState({ loading_video: 'true' });
            if (Platform.OS === 'android') {
                this.fileService.getMediaFile(this.props.feedItem.media_url,
                    this.props.feedItem.filename)
                    .then(url => {
                        //for android versions 4.4 and up, or for audio files
                        if (Platform.Version > 18) {
                            //set the url that will be used to launch the external video player
                            this.props.setExternalVideoPlayerUrl(url);
                            //now show the icon to launch the external player
                            this.props.showPlayerIcon();

                            //play the video if it is still in view, otherwise the video will continue to play even if
                            //since starting the download the user has scrolled away
                            this._playVideoIfInView(() => {
                                this.setState({ video_url: url, removed_video: false });
                            });
                        }
                        else { //for android versions 4.3 and below, launch external player
                            this._playVideoIfInView(() => {
                                if (this.props.feedItem.media_type === 'v')
                                    this.shareService.launchExternalPlayer(url);
                                else
                                    this.shareService.launchExternalPlayerAudio(url);
                                setTimeout(() => {
                                        // timeout means we wait for external player to show, before changing the loader to the player icon
                                        //makes for a better ux
                                        this.setState({ loading_video: 'false', removed_video: true });
                                    }, 500);
                                });
                            }
                    })
                    .catch(() => {
                        if (Platform.Version > 18) {
                            /**
                             * if we are unable to obtain file, set the video url directly to the 
                             * server url and allow the video player to handle it
                             */
                            //remember that in this case we do not need to set the external player url first
                            //cos the FeedCard component already sets the http url as the default
                            this._playVideoIfInView(() => {
                                this.setState({ video_url: this.props.feedItem.media_url, removed_video: false });
                                this.props.showPlayerIcon();
                            });
                        }
                        else {
                            //for android versions 4.3 and below, launch external player
                            this._playVideoIfInView(() => {
                                if (this.props.feedItem.media_type === 'v')
                                    this.shareService.launchExternalPlayer(this.props.feedItem.media_url);
                                else
                                    this.shareService.launchExternalPlayerAudio(this.props.feedItem.media_url);
                                setTimeout(() => { 
                                    // timeout means we wait for external player to show, before changing the loader to the player icon
                                    //makes for a better ux
                                    this.setState({ loading_video: 'false', removed_video: true });                                    
                                }, 500);
                            });
                        }
                    });
            }
            else {
                this.setState({ video_url: this.props.feedItem.media_url, removed_video: false });
            }
        }
    }

    _playVideoIfInView(callback: Function) {
        /**
         * we use the videoContainerView, cos the videoView might not even be loaded by the playFirstTime
         * we call this.
         * in fact, i think we should consider using videoContainerView and not videoView in all cases
         * whem we want the videos position... just saying... :)
         */
        this.videoContainerView.measure((x, y, width, height, px, py) => {
            // please note: if the video is completely out of view,
            // by the time this function is called, py, py, height, etc.. will all be undefined
            // cos the ListView will have removed it from the UI

            //if more than 70% of the video is offscreen, do not play
            if (py === undefined || py < -1 * 0.7 * height || py > Dimensions.get('window').height - (0.3 * height)) {
                this.setState({ loading_video: 'false' }); //remove the loader but do nothing else
            }
            else {
                callback();
            }
        });
    }


    videoLoaded() {
        if (!this.state.has_played_before) {
            this.setState({ loading_video: 'loaded', video_playing: true, has_played_before: true });
            EventEmitter.dispatch(REMOVE_ALL_VIDEOS, this.state.id);
        }
    }

    videoProgress() {
        // console.log('vide projecss');
    }

    videoEnded() {
        // console.log('video ended');
    }

    videoError(error) {
        // console.log('error is ', error);
        this.setState({ loading_video: false });
    }

    videoStarted() {
        if (Platform.OS === 'ios') {
            this.videoLoaded();
        }
    }

    renderVideo() {
        if (this.state.video_url && !this.state.removed_video) {
            return (
                <View ref={(view) => { this.videoView = view; }} collapsable={false}>
                    <Video
                        style={{ width: this.width, height: this.height }}
                        ref={videoPlayer => this.videoPlayer = videoPlayer}
                        source={{ uri: this.state.video_url }}
                        volume={1.0}
                        repeat
                        paused={this.state.video_paused}
                        resizeMode="contain"
                        onError={this.videoError.bind(this)}
                        onLoad={this.videoLoaded.bind(this)}
                        onEnd={this.videoEnded.bind(this)}
                        playInBackground={false}
                    />
                </View>
            );
        }
        else if (this.state.audio_url && !this.state.removed_video) {
            return (
                <View ref={(view) => { this.videoView = view; }} collapsable={false}>
                    <Video
                        style={{ width: 0, height: 0 }}
                        ref={videoPlayer => this.videoPlayer = videoPlayer}
                        source={{ uri: this.state.audio_url }}
                        volume={1.0}
                        repeat
                        paused={this.state.video_paused}
                        onProgress={this.videoProgress.bind(this)}
                        onError={this.videoError.bind(this)}
                        onLoad={this.videoLoaded.bind(this)}
                        onEnd={this.videoEnded.bind(this)}
                        playInBackground={false}
                    />
                </View>
            );
        }
    }

    renderVideoIcon() {
        if (/*not loading and not playing*/
            (this.state.loading_video !== 'true' && this.state.video_playing === false)
            || /*loaded but paused*/
            (this.state.loading_video === 'loaded' && this.state.video_paused === true))
            return (
                <View style={{ ...styles.videoIcon, left: (this.width / 2) - 27, top: (this.height / 2) - 27 }}>
                    <Icon name={'play-circle-outline'} size={60} color={'#fff'} style={{ margin: -4, padding: 0 }} />
                </View>
            );
    }

    renderLoader() {
        if (this.state.loading_video === 'true') {
            return (
                <View
                    style={{
                        position: 'absolute',
                        left: (Dimensions.get('window').width / 2) - (this.props.feedItem.media_type === 'v' ? this.getIconPosition() : 15),
                        top: this.props.feedItem.media_type === 'v' ? (this.height / 2) - this.getIconPosition() : 15
                    }}
                >
                    {
                        Platform.OS === 'android' ?
                            <Spinner size={this.props.feedItem.media_type === 'v' ? 55 : 30} color={'#bbb'} />
                            :
                            <Spinner size={this.props.feedItem.media_type === 'v' ? 'large' : 'small'} color={'#bbb'} />
                    }
                </View>
            );
        }
        else if (this.state.error) {
            return (
                <View
                    style={{
                        position: 'absolute',
                        left: (Dimensions.get('window').width / 2) - (this.props.feedItem.media_type === 'v' ? 27 : 15),
                        top: this.props.feedItem.media_type === 'v' ? (this.height / 2) - 27 : 15,
                        padding: -4
                    }}
                >
                    <Icon name={'error'} size={this.props.feedItem.media_type === 'v' ? 60 : 30} color={'#999'} />
                </View>
            );
        }
    }
    getIconPosition() {
        if (Platform.OS === 'ios')
            return 20;
        return 30;
    }

    renderWebPlayer() {
        //android version 4.3 and below
        //use a webview to play the video/audio
        const a = this.props.feedItem.media_url;
        if (this.props.feedItem.media_type === 'v') {
            const b = this.props.feedItem.poster_image;
            return (
                <WebView
                    source={{ uri: `${this.config.getBaseUrlDynamicHtml()}/videoplayer/${encodeURIComponent(a)}===${encodeURIComponent(b)}` }}
                    style={{ width: this.width, height: this.height, backgroundColor: Properties.greyBackColor }}
                    javaScriptEnabled
                    domStorageEnabled
                />
            );
        }
        else if (this.props.feedItem.media_type === 'i')
            return (
                <WebView
                    source={`${this.config.getBaseUrlDynamicHtml()}/audioplayer/${encodeURIComponent(a)}`}
                    style={{ backgroundColor: Properties.greyBackColor }} />
            );
    }

    renderMedia() {
        // if (Platform.OS === 'android' && Platform.Version <= 18) {
        // return this.renderWebPlayer();
        // }
        if (this.props.feedItem.media_type === 'v') {
            if (this.props.feedItem.FOR_POST_PREVIEW || this.props.feedItem.NEW_POST) {
                /**
                 * this is teh case where a video has been loaded and HAS NOT YET BEEN POSTED.
                 * we show the preview by loading the local filename
                 * remember this is different from the post preview in which the user is shown 
                 * a preview version of his post
                 */
                return (
                    <TouchableWithoutFeedback onPress={this.videoTapped.bind(this)}>
                        <View>{this.renderVideo()}</View>
                    </TouchableWithoutFeedback>
                );
            }
            return (
                <TouchableWithoutFeedback onPress={this.videoTapped.bind(this)}>
                    <View ref={(view) => { this.videoContainerView = view; }} style={{ backgroundColor: Properties.greyBackColor }}>
                        <Image
                            style={{ width: this.width, height: this.height }}
                            source={{ uri: this.props.feedItem.poster_image }}
                        >
                            {this.renderVideoIcon()}
                            {this.renderVideo()}
                        </Image>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        //audio
        return (
            <View
                ref={(view) => { this.videoContainerView = view; }}
                style={{
                    ...styles.audioView,
                    borderTopColor: this.state.video_playing ? Properties.themeColor : '#ccc'
                }}>
                <TouchableOpacity onPress={this.videoTapped.bind(this)}>
                    <Icon name={this.state.video_playing ? 'pause-circle-outline' : 'play-circle-outline'} size={30} color={'#ccc'} />
                </TouchableOpacity>
                {this.renderVideo()}
                <Icon2 name={'ios-musical-notes'} size={30} color={'#ccc'} />
            </View>
        );
    }

    render() {
        return (
            <View
                style={{
                    width: Dimensions.get('window').width,
                    alignItems: 'center'
                }}>
                {this.renderMedia()}
                {this.renderLoader()}
            </View>
        );
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(SCROLLING_THROUGH_FEED, this.scrolling_feed_subscription);
        EventEmitter.unsubscribe(REMOVE_ALL_VIDEOS, this.remove_videos_subscription);
    }

}


const styles = {
    videoIcon: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderRadius: 30,
    },
    audioView: {
        borderTopWidth: 2,
        borderTopColor: '#ccc',
        paddingTop: 5,
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: Dimensions.get('window').width - 80,
    }
};
