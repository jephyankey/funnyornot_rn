import React, { Component } from 'react';
import {
    ListView, View, Text, Dimensions,
    Platform, StatusBar,
    TextInput, InteractionManager
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { ErrorCard, Avatar } from '../base';
import { Header, CenterPage, Spinner, SearchBar } from '../../common';
import {
    ContactService, Config, Utilities,
    DatabaseService, AvatarService, UserService,
    EventEmitter
} from '../../SERVICES';
import {
    BLOCK_ACTION, BLOCKED, FOLLOW_ACTION, UNFOLLOWED, PROFILE,
    //error types
    NO_INTERNET2, NO_FOLLOWING_ON_APP, NO_CONTACTS_ON_APP, NO_ONE_DURING_SEARCH,
    NO_FOLLOWERS_ON_APP, HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT
} from '../../types';


class People extends Component {

    //pageMode is a required prop

    err_state: string;
    CALLING_FUNCTION: Object;
    err_msg: string;
    title: string;

    constructor(props) {
        super(props);
        this.utilities = new Utilities();
        const config = new Config();
        const dbService = new DatabaseService(this.utilities);
        this.userService = new UserService(config, this.utilities, dbService);
        this.contactService = new ContactService(config, this.utilities, dbService);
        this.avatarService = new AvatarService(config, this.utilities, dbService);

        this.contact_list = [];
        this.filtered_list = [];

        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
    }

    state = {
        loading_contacts: true,
        avatar_urls_ready: false,
        contacts: [],
        search_term: '',
        err_state: null
    };


    componentWillMount() {
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content');
            //otherwise, when we get here from the profile page, the status bar will not be visisble
        }
        if (this.props.pageMode === 'contacts') {
            this.err_msg = 'Could not load contacts';
            this.CALLING_FUNCTION = this.contactService.getAppFriends();
            this.err_state = NO_CONTACTS_ON_APP;
        }
        else {
            this.err_msg = 'Could not load your list';
            this.CALLING_FUNCTION = this.userService.getFollowingList(this.props.pageMode);
            this.err_state = this.props.pageMode === 'following' ? NO_FOLLOWING_ON_APP : NO_FOLLOWERS_ON_APP;
        }

        InteractionManager.runAfterInteractions(() => {
            this.CALLING_FUNCTION
                .then(data => this.processData(data))
                .catch(err => {
                    this.setState({ err_state: err, loading_contacts: false });
                    this.utilities.handleErrors(err);
                    if (err !== NO_INTERNET2)
                        this.utilities.showToast(this.err_msg);
                });

            //subscribe to block observable, so that wen someone is blocked, 
            //he is removed from list
            this.block_subscription = EventEmitter.subscribe(BLOCK_ACTION, (event_data) => {
                if (event_data.state === BLOCKED)
                    this.removeBlockedUser(event_data.user_id);
            });

            if (this.props.pageMode === 'following') {
                this.follow_subscription = EventEmitter.subscribe(FOLLOW_ACTION, (event_data) => {
                    if (event_data.state === UNFOLLOWED)
                        this.removeBlockedUser(event_data.user_id);
                });
            }
        });
    }

    /**
     * this is necessary because, from the contacts page, you can go to a person's profile_id
     * page and block him from there, when you return to the contacts page,
     * we want that person to be gone'
     */
    removeBlockedUser(user_id) {
        const contact_list = this.state.contacts;
        for (let i = 0; i < contact_list.length; i++) {
            if (user_id === contact_list[i].user_id) {
                contact_list.splice(i, 1);
                this.setState({ contacts: contact_list });
                break;
            }
        }
    }


    processData(data) {
        if (this.props.pageMode === 'contacts')
            this.processContacts(data);
        else
            this.processFollows(data);
    }

    processFollows(data) {
        //>>>>todo?>>>>for those who are in my contact list, replace their names

        //render data
        this.contact_list = data;
        this.setState({ contacts: data, loading_contacts: false, avatar_urls_ready: true });
        /**
         * Note: over here, i.e. for follows, the data is fetched from the server, so it comes with 
         * the avatar urls, unlike contacts which are fetched locally
         */
    }

    /**
     * loop tru contacts to generate a comma serparted list to send to server to fetch
     * avatar urls, this is why getAppFriends() returns the raw sql object
     * renders UI with contacts after fetching, and also makes the request for the urls
     */
    processContacts(temp_contacts) {
        // a comma separated list of strings which will be used to fetch avatar urls from server
        let user_list: string = null;

        for (let j = 0; j < temp_contacts.rows.length; j++) {
            const element = temp_contacts.rows.item(j);
            const contact = [];
            contact.phone = element.phone;

            contact.name = element.name;
            contact.user_id = element.user_id;
            // contact.on_app = 1;

            this.contact_list.push(contact);

            if (j === 0)//now starting
                user_list = `${contact.user_id}`;
            else
                user_list = `${user_list},${contact.user_id}`; // user_list + ',' + contact['user_id'];
        }
        //all contacts are available, show them in UI
        this.setState({ contacts: this.contact_list, loading_contacts: false });

        //get avatar urls for all contacts on the app
        /**
         * this is because contacts are fetched locally, so we have to make a separate 
         * call for the avatar urls
         */
        if (this.contact_list.length) {
            this.fetchAvatarUrls(user_list, this.contact_list);
        }
    }


    fetchAvatarUrls(user_list: string) {
        this.avatarService.getAvatarUrls(user_list)
            .then(data => {
                for (let i = 0; i < this.contact_list.length; i++) {
                    this.contact_list[i].avatar_url = data[i].avatar_url || null;
                }
            })
            .catch(err => {
                if (err === NO_INTERNET2)
                    this.utilities.showToast('No Internet Connection');
                //if there is an error fetching the list of avatar urls, use default avatars
            })
            .then(() => {
                this.setState({ contacts: this.contact_list, avatar_urls_ready: true });
            });
    }

    goToProfile(contact: Array) {
        Actions.profilePage({
            profile_id: contact.user_id,
            name: contact.name || `@${contact.username}`,
            avatar_url: contact.avatar_url
        });
    }

    search(search_term) {
        //filter list
        this.setState({ search_term });
        const keytyped = search_term.toLowerCase().trim();
        // if the value is an empty string don't filter the items
        if (keytyped && keytyped !== '') {
            this.filtered_list = this.contact_list.filter((item) => {
                //search through the name (contacts) or username (followers and following)
                const n = item.name || item.username;
                return ((n.toLowerCase()).indexOf(keytyped) > -1);
            });
            this.setState({ contacts: this.filtered_list });
        }
        else {
            this.setState({ contacts: this.contact_list });
        }
    }


    renderName(data_object) {
        if (data_object.name) {
            return this.utilities.truncateString(data_object.name);
        }
        return this.utilities.truncateString(`@${data_object.username}`);
    }

    renderRow(data) {
        return (
            <View key={data.user_id} style={styles.singleContact}>
                {
                    <Avatar
                        forPeople
                        url={this.state.avatar_urls_ready ? data.avatar_url : null}
                        size={0.22 * window}
                        onTap={this.goToProfile.bind(this, data)} />
                }
                <Text style={styles.avatarText}>{this.renderName(data)}</Text>
            </View>
        );
    }

    renderContacts() {
        this.dataSource = this.ds.cloneWithRows(this.state.contacts);
        return (
            <ListView
                style={{ flex: 1 }}
                dataSource={this.dataSource}
                renderRow={this.renderRow.bind(this)}
                enableEmptySections
                initialListSize={18}
                pageSize={6}
                contentContainerStyle={styles.contactsView}
            />
        );
    }

    renderContent() {
        let no_one_during_search = false;
        if (this.state.loading_contacts) {
            return <CenterPage><Spinner /></CenterPage>;
        }
        else if (this.state.err_state) {
            return <CenterPage><ErrorCard errorState={this.state.err_state} /></CenterPage>;
        }
        else if (this.state.contacts.length === 0) {
            if (!this.state.search_term) { //we are not searching...
                return <CenterPage><ErrorCard errorState={this.err_state} /></CenterPage>;
            }
            no_one_during_search = true;
        }
        if (this.state.contacts.length || no_one_during_search) {
            return (
                <View style={styles.page}>
                    <SearchBar styles={styles.searchBar}>
                        <TextInput
                            style={{ height: 40, width: (Dimensions.get('window').width) - 80 }}
                            multiline={false}
                            onChangeText={this.search.bind(this)}
                            value={this.state.search_term}
                            placeholder={'Search'}
                            underlineColorAndroid={'transparent'}
                        />
                    </SearchBar>
                    {
                        no_one_during_search ?
                            <CenterPage>
                                <ErrorCard errorState={NO_ONE_DURING_SEARCH} />
                            </CenterPage>
                            :
                            this.renderContacts()
                    }
                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.page}>
                {this.renderContent()}
                <Header title={this.props.title} showBackButton transparent />
            </View>
        );
    }

    componentWillUnmount() {
        EventEmitter.unsubscribe(BLOCK_ACTION, this.block_subscription);
        EventEmitter.unsubscribe(FOLLOW_ACTION, this.follow_subscription);
        if (Platform.OS === 'ios') {
            if (this.props.fromPage && this.props.fromPage === PROFILE) {
                StatusBar.setBarStyle('light-content');
            }
        }
    }

}


const window = Dimensions.get('window').width;


const styles = {
    page: {
        flex: 1,
        backgroundColor: '#fff',
    },
    contactsView: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        paddingBottom: 40
    },
    singleContact: {
        width: 0.29 * window,
        height: 0.29 * window,
        marginTop: 20,
        marginLeft: 0.033 * window,
        alignItems: 'center',
        marginBottom: 10
    },
    avatarText: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#000',
        fontSize: 12,
        marginTop: 9
    },
    searchBar: {
        marginTop: Platform.OS === 'android' ? HEADER_HEIGHT : HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT
    }
};


export default People;
