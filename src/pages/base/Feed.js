import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    ListView, RefreshControl, View,
    InteractionManager, Platform
} from 'react-native';

import { CombinedStore } from '../../reducers/interfaces';
import {
    MainContent, CenterPage,
    SetupContent, Spinner,
    FeedFooter
} from '../../common';

import {
    //errors
    EMPTY_RESULT,
    //page modes
    HOME, ALLTIME, FAVOURITES, EXPLORE,
    //actions
    TRENDING_REFRESH, UP_DIRECTION, DOWN_DIRECTON,
    //dispatchable events
    INADEQUATE_FEED,
    RECOMMENDED_FOLLOWS, SCROLLING_THROUGH_FEED, PARALLAX_SCROLL,
    HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT
} from '../../types';

import { FeedCard, ErrorCard, RecommendFollows } from '../base';
import { updateFeedData, updateLoaderState, replaceFeedData } from '../../actions';

import {
    FeedService,
    ContactService,
    Utilities,
    Config,
    DatabaseService,
    UserService,
    FileService,
    PostService,
    EventEmitter,
} from '../../SERVICES';



class Feed extends Component {

    dataSource: any;
    page: number;
    page_store: any;
    retrying_load: boolean;
    should_fetch_more: boolean;
    PAGEMODE: string;
    still_refreshing: boolean;
    show_recommended_people: number;

    listViewRef: null; //a reference to the listview, can be used to scroll to a specific point for e.g.
    //when a new post is made, this is used to scroll to the top so the user can see his post

    propTypes: {
        pageMode: React.PropTypes.string //default is home, others are favourites, trending, alltime
    }


    constructor(props) {
        super(props);
        this.page = 1;
        this.page_store = null; /* used to backup page when performing a page refresh, 
    then page is set to one since for now a refresh means reload page 1, this way, if 
    the refresh fails, we can then restore the page number to the correct value, and keep 
    the current page as it is */

        this.retrying_load = false; /**
      set as a flag, when retrying to load after a failed attempt
      the retry action is performed in componentDidUpdate, however, because that 
      function could be called other times, we use this flag to make sure that 
      when it is called, we only attempt to load data, if we are in the process of a retry attempt
     */
        this.should_fetch_more = true; //used to determine if we shd make a call for more dat
        this.still_refreshing = false;

        //stores the scroll postion to help us determine 
        //whether we are scrolling up or down (used in onScroll function)
        //or whether we've changed directions
        this.old_scroll_position = null;
        this.old_scroll_direction = null;
        this.last_known_parallax_trigger_position = 0;
    }


    state = {
        error: false, // error: tells us whether an error has occured or not
        show_more_loader: true,
        err_state: {}, //err_state: holds the actual error
        refreshing: false,
        // content_height: null,
        recommended_people: null,
        remove_clipped_subviews: false /** only used on ios.
        for ios, not setting this, means that page seems hidden until user interacts with the page */
    };


    componentDidMount() {
        this.config = new Config();
        this.utilities = new Utilities();
        this.dbService = new DatabaseService(this.utilities);
        this.contactService = new ContactService(this.config, this.utilities, this.dbService);
        this.userService = new UserService(this.config, this.utilities, this.dbService);
        this.fileService = new FileService(this.config, this.utilities);
        this.feedService = new FeedService(this.config, this.utilities, this.userService,
            this.contactService, this.dbService);
        this.postService = new PostService(this.config, this.utilities, this.userService, this.fileService);

        //variable that is used to determine if we should show a list of recommended users to following
        this.show_recommended_people = this.utilities.getRandomInt(1, 3);

        if (this.props.pageMode === HOME && this.show_recommended_people === 1) {
            this.feedService.getRecommendations(RECOMMENDED_FOLLOWS)
                .then(data => {
                    this.setState({ recommended_people: data });
                }).catch();
        }

        //set page mode, specifies which actual page this component is used in
        //cos this page is used for both Home, and Favourites
        this.fetchData(); //this will get the data and then re render this component
    }


    /**
     * used when retrying after an error
     */
    componentDidUpdate() {
        if (this.retrying_load)
            this.fetchData();
    }


    onRefresh() {
        this.page_store = this.page;
        this.page = 1;
        this.setState({ refreshing: true });
        this.still_refreshing = true;
        this.fetchData();
    }

    /**
     * for infinite scrolling
     */
    loadMore() {
        if (this.should_fetch_more && !this.state.refreshing) {
            this.fetchData();
        }
    }

    /**
     * for retrying to load the page after an initial failed attempt
     * say internet error, or server error
     */
    retryLoad() {
        this.page = 1;
        this.retrying_load = true;
        this.setState({ error: false, err_state: {}, show_more_loader: true });
    }


    setDataSource() {
        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        let source;
        if (this.props.pageMode === HOME)
            source = this.props.data;
        if (this.props.pageMode === EXPLORE)
            source = this.props.explore_data;
        else if (this.props.pageMode === FAVOURITES)
            source = this.props.fav_data;
        else if (this.props.pageMode === ALLTIME)
            source = this.props.alltime_data;

        this.dataSource = this.ds.cloneWithRows(source);
    }


    /**
     * this will fetch the data, afterwards it will try to rerender
     * the page
     */
    fetchData() {
        this.should_fetch_more = false;
        this.retrying_load = false;

        let REFRESHING = null;
        /**
         * checking state here does not work, so we don't do if(this.state.refreshing)
         * it appears this might be because this.fetchData() is called immediately after the setState (bt i'm not suer yet')
         */
        if (this.still_refreshing)
            REFRESHING = TRENDING_REFRESH;

        let CALLING_FUNCTION = null;
        if (this.props.pageMode === ALLTIME)
            CALLING_FUNCTION = this.feedService.getRankings(this.page, ALLTIME, REFRESHING);
        else if (this.props.pageMode === EXPLORE)
            CALLING_FUNCTION = this.feedService.fetchExplore(this.page);
        else
            CALLING_FUNCTION = this.feedService.fetchFeed(this.page, this.props.pageMode);

        CALLING_FUNCTION
            .then(data => {
                if (data.length === 0)
                    throw EMPTY_RESULT;
                else {
                    //no error
                    this.page++;
                    //for explore, we do not need to update names with local contact names
                    if (this.props.pageMode === EXPLORE)
                        return data;
                    //other cases other than explore, home, favs, alltime, etc
                    return this.userService.updateFeedWithContactNames(data);
                }
            })

            .then((updatedData) => { //data is now updated with contact names where available
                InteractionManager.runAfterInteractions(() => {
                    if (this.state.refreshing) { //data arrived through a page refresh
                        this.props.replaceFeedData(updatedData, this.props.pageMode);
                        this.should_fetch_more = true;
                    }
                    else { // then -> normal data update (initial loading or loading more...)
                        //call action creator to update global state
                        if ((this.page - 1) === 1) { //rremember that the page number wd hav been increased in previous then block
                            //THIS IS PAGE ONE
                            this.props.replaceFeedData(updatedData, this.props.pageMode);
                        }
                        else
                            this.props.updateFeedData(updatedData, this.props.pageMode);
                        // this.props.updateFeedData(updatedData, this.props.pageMode);

                        if (updatedData.length < this.feedService.getPageSize() ||
                            (this.props.pageMode === ALLTIME && this.page - 1 === 9)) {
                            /***
                             * SPAGHETTI LOGIC: for the ALLTIME page, if we are on the 9th page i.e. max number is 48,
                             * then prevent the loader from showing 
                             */
                            this.setState({ show_more_loader: false }); //we are done loading more from server
                        }
                        else { // then -> data.length === this.feedService.getPageSize()
                            /** the length of the data is less than the pageSize 
                             * update the data store and also tell the localstate we are done loading more
                             */
                            this.should_fetch_more = true;
                        }
                    }
                });
            })

            .catch(err => {
                //LET us first determine if this error is just an EMPTY_RESULT
                //in that case lets emit an event that will trigger the welcome page to be shown
                if (this.props.pageMode === HOME && this.page === 1 && err === EMPTY_RESULT) {
                    EventEmitter.dispatch(INADEQUATE_FEED, null);
                }
                else {
                    this.utilities.handleErrors(err);

                    //rerender the page and move to the correct error view (of course only if we are on page 1)
                    //also, we will not update the data state, but we do not say we are done loading more
                    //so if a retry should work, we continue as normal
                    if (this.page === 1 && !this.state.refreshing) {
                        this.setState({ error: true, err_state: err });
                    }
                    else {
                        this.setState({ show_more_loader: false });
                        /**in this case, page has previously loaded some data,
                          so the only thing we do is not show the loader, bt we stay on the same page
                        */
                    }
                    //if we were attempting to do a page refresh, but then failed
                    //we restore the original page number
                    if (this.state.refreshing) {
                        this.page = this.page_store;
                    }
                }
            })

            .then(() => {
                if (this.state.refreshing) {
                    this.still_refreshing = false;
                    this.setState({ refreshing: false });
                } //turn off refresh loader
            });
    }


    onScroll(event) {
        EventEmitter.dispatch(SCROLLING_THROUGH_FEED, null);

        //for parallax header
        if (this.props.pageMode === HOME || this.props.pageMode === EXPLORE) {
            const new_scroll_position = event.nativeEvent.contentOffset.y;
            const direction = new_scroll_position > this.old_scroll_position ? UP_DIRECTION : DOWN_DIRECTON;
            this.old_scroll_position = new_scroll_position;
            /** dispatch event, but only if the direction has changed since the last trigger*/
            if (this.old_scroll_direction !== direction) {
                if (
                    ((direction === UP_DIRECTION && new_scroll_position > (HEADER_HEIGHT + 20)) || /**
                    when scrolling up, do not trigger until, the allowed padding for the header 
                        (which is = to the header's height) + SOME ALLOWANCE has been scrolled away, meaning the top of the 
                        first post_id is touching the top of the screen */
                        (direction === DOWN_DIRECTON && new_scroll_position < (HEADER_HEIGHT * 2))) /**
                    when scrolling down, wait until the user is reasonably close to the top of the list
                    before triggering (i.e. showing the header)
                    -------------------------------------------------
                    we now have an effective trigger range somewhat like this HEADER_HEIGHT < .... > HEADER_HEIGHT
                     */

                ) {
                    EventEmitter.dispatch(PARALLAX_SCROLL, {
                        page_mode: this.props.pageMode,
                        direction
                    });
                    this.last_known_parallax_trigger_position = new_scroll_position;
                    this.old_scroll_direction = direction;
                }
            }
        }

        //for ios, set removeClippedSubviews to true,
        //as a reminder, we set it to false, when it is true the page is initially not rendered properly (.i.e it is hidden)
        //but after rendering if the user attempts to scroll, then set it to true (AS IT SHOULD HAVE BEEN) for performance
        if (Platform.OS === 'ios') {
            if (this.state.remove_clipped_subviews === false) {
                this.setState({ remove_clipped_subviews: true });
            }
        }
    }

    renderRow(data, sectionId, rowId) {
        if (parseInt(rowId, 10) === 2 && this.state.recommended_people !== null
            && this.state.recommended_people.length > 3) {
            return (
                <View key={1}>
                    <RecommendFollows list={this.state.recommended_people} />
                    <FeedCard feedItem={data} />
                </View>
            );
        }
        if (parseInt(rowId, 10) >= 50 && this.props.pageMode === ALLTIME) {
            //for the jokes of fame, do not show more than 50,
            //rowId starts from 0, so 50 is really the 51st item. do not show it and all after
            return null;
        }
        return (
            <FeedCard feedItem={data} key={data.post_id} />
        );
    }

    renderFooter() {
        return <FeedFooter showLoader={this.state.show_more_loader && !this.state.refreshing} />;
        //the double boolean exp ensures that when refreshing, 
        //if the user quickly scrolls to the bottom,
        //the load more loader will not show
    }

    renderContent() {
        let loader;
        if (this.props.pageMode === HOME)
            loader = this.props.initial_loading;
        else if (this.props.pageMode === EXPLORE)
            loader = this.props.initial_loading_explore;
        else if (this.props.pageMode === FAVOURITES) //default of HOME
            loader = this.props.initial_loading_favs;
        else if (this.props.pageMode === ALLTIME)
            loader = this.props.initial_loading_alltime;

        if (loader && !this.state.error) {
            return (
                <SetupContent>
                    <Spinner />
                </SetupContent>
            );
        }
        else if (this.state.error) {
            return (
                <CenterPage>
                    <ErrorCard errorState={this.state.err_state} onRetry={this.retryLoad.bind(this)} />
                </CenterPage>
            );
        }

        this.setDataSource();
        return (
            <ListView
                ref={(listView) => { this.listViewRef = listView; }}
                removeClippedSubviews={Platform.OS === 'android' ? true : this.state.remove_clipped_subviews}
                onScroll={this.onScroll.bind(this)}
                style={{ flex: 1 }}
                dataSource={this.dataSource}
                renderRow={this.renderRow.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                onEndReached={this.loadMore.bind(this)}
                contentContainerStyle={styles.list}
                enableEmptySections
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh.bind(this)}
                        colors={['green', '#ff8c00', '#bdb76b']}
                        progressViewOffset={HEADER_HEIGHT}
                    />
                }
            />
        );
    }


    render() {
        return (
            <MainContent>
                {this.renderContent()}
            </MainContent>
        );
    }
}


Feed.defaultProps = {
    pageMode: HOME
};


function mapStateToProps(state: CombinedStore) {
    return {
        //for main feed or home page
        initial_loading: state.feed.loading_home_data,
        data: state.feed.home_feed_data,
        //for explore page
        initial_loading_explore: state.feed.loading_explore_data,
        explore_data: state.feed.explore_feed_data,
        //for favourites page
        fav_data: state.feed.favourites_data,
        initial_loading_favs: state.feed.loading_favourites_data,
        //for alltime (jokes of fame)
        alltime_data: state.feed.alltime_feed_data,
        initial_loading_alltime: state.feed.loading_alltime_data
    };
}

export default connect(mapStateToProps, {
    updateFeedData, updateLoaderState, replaceFeedData
})(Feed);


const styles = {
    list: {
        overflow: 'hidden',
        paddingTop: Platform.OS === 'android' ? HEADER_HEIGHT : HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT
    }
};
