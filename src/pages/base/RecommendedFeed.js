//imports
import React, { Component } from 'react';
import { ScrollView, View, Image, Platform } from 'react-native';

import { Config, Utilities, DatabaseService, ContactService, UserService, FeedService } from '../../SERVICES';

import { RECOMMENDED_JOKES, RECOMMENDED_FOLLOWS, HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT } from '../../types';
import { CenterPage, Spinner, MyText, HeaderParallax } from '../../common';

import { RecommendFollows, FeedCard } from '../base';

export class RecommendedFeed extends Component {

    state = {
        loaded_jokes: false,
        loaded_people: false,
        recommended_people: null,
        recommended_jokes: null,
        username: null,
    }

    constructor(props) {
        super(props);
        this.config = new Config();
        this.utilities = new Utilities();
        this.dbService = new DatabaseService(this.utilities);
        this.contactService = new ContactService(this.config, this.utilities, this.dbService);
        this.userService = new UserService(this.config, this.utilities, this.dbService);
        this.feedService = new FeedService(this.config, this.utilities, this.userService, this.contactService, this.dbService);
    }

    componentDidMount() {
        this.userService.getCurrentUsersName()
            .then(name => {
                this.setState({ username: name });
            });
        this.feedService.getRecommendations(RECOMMENDED_FOLLOWS)
            .then(data => {
                this.setState({ recommended_people: data, loaded_people: true });
            })
            .catch(() => {
                this.setState({ loaded_people: true }); //we will go ahead even if we are unable to 
                //to fetch the list of people to follow, the idea here is that the list of recommended 
                //jokes should still be enough for a good UX
            });
        this.feedService.getRecommendations(RECOMMENDED_JOKES)
            .then(data => {
                this.setState({ recommended_jokes: data, loaded_jokes: true });
            })
            .catch(err => {
                this.setState({ error: err });
            });
    }


    renderRecommendedJokes() {
        const renderingObj = [];
        for (let i = 0; i < this.state.recommended_jokes.length; i++) {
            const f = this.state.recommended_jokes[i];
            renderingObj.push(
                <FeedCard feedItem={f} key={f.post_id} />
            );
        }
        return renderingObj;
    }

    renderEmptyMsg() {
        const { width, height } = this.utilities.resizeImage(479, 271);

        return (
            <View style={{ backgroundColor: '#fff', marginBottom: 20 }}>
                <Image
                    source={require('../../imgs/laughing_baby.jpg')}
                    style={{ width, height }}
                    resizeMode="contain"
                    />
                <MyText styles={{ fontSize: 16, padding: 10, marginBottom: 20 }}>
                    Your home page does not have much content.{'\n'}
                    Simply follow as many funny people as you can to see more content here.
                </MyText>
            </View>
        );
    }

    renderJokesMsg() {
        return (
            <MyText styles={{ backgroundColor: '#fff', paddingVertical: 20, paddingHorizontal: 15, fontSize: 16, marginBottom: 10 }}>
                In the meantime, here are some jokes everyone seems to like.
            </MyText>
        );
    }



    render() {
        /**
         * TODO: ERROR VIEW SHOULD BE PUT HERE
         */
        if (this.state.error) {
            return null;
        }
        else if (this.state.loaded_jokes && this.state.loaded_people && this.state.username) {
            return (
                <View style={{ flex: 1, backgroundColor: '#eee' }}>
                    <ScrollView style={styles.list}>
                        <View key={10}>{this.renderEmptyMsg()}</View>
                        <View key={20}><RecommendFollows list={this.state.recommended_people} /></View>
                        <View key={30}>{this.renderJokesMsg()}</View>
                        {this.renderRecommendedJokes()}
                        <View style={{ marginBottom: 100 }} />
                    </ScrollView>
                </View>
            );
        }
        return (
            <View style={{ flex: 1, backgroundColor: '#eee' }}>
                <CenterPage>
                    <Spinner />
                </CenterPage>
            </View>
        );
    }

}

const styles = {
    list: {
        paddingTop: Platform.OS === 'android' ? HEADER_HEIGHT : HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT
    }
};