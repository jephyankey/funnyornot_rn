import React, { Component } from 'react';
import { Text } from 'react-native';
import { connect } from 'react-redux';

import axios from 'axios';
import { Actions } from 'react-native-router-flux';

import { Spinner, Header, CenterPage, SetupContent } from '../../common';
import { ErrorCard } from '../base';
import { countryDetected } from '../../actions';
import countryList from '../../SERVICES/country_codes';

import { Config } from '../../SERVICES';
import { NO_INTERNET } from '../../types';


class SettingUp extends Component {

  constructor(props) {
    super(props);
    this.config = new Config();
  }

  state = {
    error: null
  }

  componentWillMount() {
    this.letsGetStarted();
  }

  letsGetStarted() {
    //make an api call to detect country
    axios.get(`${this.config.getBaseUrl()}/myCountry/`)
      .then(response => {
        const list = getCountryFromCode(response.data.country);
        const country_data = {
          country: list.country,
          country_code: list.country_code,
          phone_code: list.phone_code
        };
        this.props.countryDetected(country_data);
        Actions.enterPhone();
      })
      .catch(error => {
        if (error.response) { // there was some error in detecting the country
          const country_data = {
            country: 'United States',
            country_code: 'US',
            phone_code: '+1'
          };
          this.props.countryDetected(country_data);
          Actions.enterPhone();
        }
        else { //there is no internet connection
          this.setState({ error: NO_INTERNET });
        }
      });
  }


  retryLoad() {
    this.setState({ error: null });
    this.letsGetStarted();
  }


  render() {
    if (this.state.error === NO_INTERNET) {
      return (
        <CenterPage>
          <ErrorCard errorState={this.state.error} onRetry={this.retryLoad.bind(this)} />
        </CenterPage>
      );
    }
    return (
      <CenterPage>
        <Header title={'Setting Up'} />
        <SetupContent>
          <Spinner size={'large'} />
          <Text style={styles.help_msg}>Setting up for first use ...</Text>
        </SetupContent>
      </CenterPage>
    );
  }
}


const styles = {
  help_msg: {
    color: '#333',
    marginBottom: 60,
    fontSize: 15
  }
};

//get country from the country's code
const getCountryFromCode = (country_code) => {
  if (country_code !== 'ZZ') {
    for (let i = 0; i < countryList.length; i++) {
      const element = countryList[i];
      if (element.country_code == country_code) {
        return {
          country: element.country,
          phone_code: element.phone_code,
          country_code: element.country_code
        };
      }
    }
  }
  return {    //return defaults when no match is found
    country: 'United States',
    phone_code: '+1',
    country_code: 'US'
  };
};

export default connect(null, { countryDetected })(SettingUp);
