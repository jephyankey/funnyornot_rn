import React, { Component } from 'react';
import { View, ScrollView, Dimensions, Text, TextInput, TouchableOpacity, ListView, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { updateUserInfo, changeCountry } from '../../actions';
import { CenterPage, SetupContent, Button, Header, ListModal, Properties, MyText } from '../../common';
import { Utilities } from '../../SERVICES';
import countryList from '../../SERVICES/country_codes';



class EnterPhone extends Component {

  constructor(props) {
    super(props);
    this.utilities = new Utilities();
  }

  state = {
    modalVisible: false,
    why_number_modal_visiblie: false
  }

  componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(countryList);
  }

  keypressed(e){
    if (e.key === 'Enter'){
      this.continue();
    }
  }

  continue() {
    if (this.props.phone_number.length > 4) {
      Actions.enterVCode();
    }
    else {
      //show toast asking him to enter a valid phone number
      this.utilities.showToast('Enter a valid phone number', 3000, 'ios-sad-outline');
    }
  }

  showCountries() {
    this.setState({ modalVisible: true });
  }

  updateCountry(data) {
    this.props.changeCountry(data);
    this.setState({ modalVisible: false });
  }

  closeCountryList() {
    this.setState({ modalVisible: false });
  }

  showWhyNumberModal() {
    this.setState({ why_number_modal_visiblie: true });
  }


  renderRow(data) {
    return (
      <TouchableOpacity
        style={styles.countryListView}
        onPress={this.updateCountry.bind(this, data)}
      >
        <Text style={styles.country}>{data.country}</Text>
        <Text style={styles.phone_code}>{data.phone_code}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    let disable_next = true;
    if (this.props.phone_number.length > 4) {
      disable_next = false;
    }

    return (
      <CenterPage>
        <SetupContent>
          <View style={styles.countryView}>
            <TouchableOpacity style={styles.touchable} onPress={this.showCountries.bind(this)}>
              <Text style={styles.country}>{this.props.country}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.numberView}>
            <TextInput
              style={styles.phoneCode}
              underlineColorAndroid={'transparent'}
              value={this.props.phone_code}
              keyboardType={'phone-pad'}
              onChangeText={value => this.props.updateUserInfo({ prop: 'phone_code', value })}
              maxLength={5}
            />
            <TextInput
              autoFocus
              style={styles.phoneNumber}
              underlineColorAndroid={'transparent'}
              placeholder={'phone number'}
              keyboardType={'phone-pad'}
              returnKeyType={'next'}
              value={this.props.phone_number}
              maxLength={15}
              onChangeText={value => this.props.updateUserInfo({ prop: 'phone_number', value })}
              onKeyPress={this.keypressed.bind(this)}
            />
          </View>
          <Text style={styles.help_msg}>
            Kindly provide your phone number. We will send you a code
              for verification.
            </Text>
          <Button
            tapped={this.showWhyNumberModal.bind(this)}
            text={'WHY MY PHONE NUMBER?'}
            textColor={Properties.themeColor}
            styles={styles.resendButton}
          />
          <Button
            text={'NEXT'}
            disabled={disable_next}
            tapped={this.continue.bind(this)}
            styles={{ borderWidth: Platform.OS === 'ios' ? 1 : 0 }}
            disabledTextColor={Platform.OS === 'ios' ? Properties.themeColor : '#fff'}
          />
        </SetupContent>


        <ListModal
          modalVisible={this.state.modalVisible}
          title={'Select your country'}
          pressClose={this.closeCountryList.bind(this)}
          pressOkay={this.closeCountryList.bind(this)}
        >
          <ListView
            dataSource={this.dataSource}
            renderRow={this.renderRow.bind(this)}
            pageSize={100}
            scrollRenderAheadDistance={1800}
          />
        </ListModal>

        <ListModal
          modalVisible={this.state.why_number_modal_visiblie}
          pressClose={() => { this.setState({ why_number_modal_visiblie: false }); }}
          pressOkay={() => { this.setState({ why_number_modal_visiblie: false }); }}
          modalHeight={ 0.6 * Dimensions.get('window').height }
        >
          <ScrollView 
            style={{ flex: 1 }}
            contentContainerStyle={{ justifyContent: 'center' }}>
            <MyText styles={styles.text}>
              We use your phone number to connect you with people you already know
              who are posting funny content. That is the whole purpose of FunnyorNot.
            </MyText>
            <MyText styles={styles.text}>
              We will never ever use your number for anything else.
              Your number will never be shown to anyone using the app.
              Not even people in your contact list.
            </MyText>
            <MyText styles={styles.text}>
              So don’t worry, let’s do this!
            </MyText>
          </ScrollView>
        </ListModal>

        
        <Header
            title={'Phone'}
            showRightButton
            btnIcon={'ios-checkmark'}
            btnIconSize={38}
            btnText={'NEXT'}
            disableBtn={disable_next}
            onBtnPress={this.continue.bind(this)}
            transparent
          />

      </CenterPage>
    );
  }
}


const styles = {
  touchable: {
    flex: 1,
  },
  countryView: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    padding: 10,
    marginBottom: 7
  },
  country: {
    flexGrow: 1,
    fontSize: 18,
    color: '#333'
  },
  numberView: {
    flexDirection: 'row',
    marginBottom: 40,
    //platform
    paddingTop: Platform.OS === 'android' ? 10 : 5,
  },
  phoneCode: {
    flex: 1,
    fontSize: 18,
    alignSelf: 'flex-start',
    borderBottomWidth: 1,
    padding: 5,
    paddingLeft: 10,
    borderColor: '#ccc',
    marginRight: 20,
    color: '#333',
    height: Platform.OS === 'ios' ? 30 : null,
  },
  phoneNumber: {
    flex: 4,
    fontSize: 18,
    alignSelf: 'flex-start',
    borderBottomWidth: 1,
    borderColor: '#ccc',
    //platform specific styles
    padding: Platform.OS === 'android' ? 5 : null,
    height: Platform.OS === 'ios' ? 30 : null,
  },
  help_msg: {
    color: '#333',
    marginBottom: 10,
    fontSize: 15
  },

  countryListView: {
    flexDirection: 'row',
    borderBottomWidth: 0.7,
    borderBottomColor: '#ccc',
    padding: 10,
    paddingTop: 13,
    paddingBottom: 13
  },
  resendButton: {
    marginBottom: 60,
    borderWidth: 0,
    backgroundColor: 'transparent',
    alignSelf: 'flex-start'
  },
  text: {
    fontSize: 16,
    padding: 10
  }
};


const mapStateToProps = state => {
  return {
    country: state.user.country,
    phone_code: state.user.phone_code,
    phone_number: state.user.phone_number
  };
};

export default connect(mapStateToProps, { updateUserInfo, changeCountry })(EnterPhone);
