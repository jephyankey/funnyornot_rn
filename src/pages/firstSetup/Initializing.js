import React, { Component } from 'react';
import { Text, AsyncStorage, Platform, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import OneSignal from 'react-native-onesignal';

import {
  //settings
  APP_VERSION, THEME, AFFILIATE_COUNTRIES, USER_COUNTRY,
  USER_ID, USER_NAME, USER_PHONE, DEVICE_ID,
  CONTACTS_PERMISSION_DENIED, FAIL_TO_READ_CONTACTS
} from '../../types';
import { Spinner, Header, CenterPage, SetupContent, DEFAULT_THEME } from '../../common';
import { updateUserInfo } from '../../actions';
import {
  InitService, DatabaseService,
  Utilities, ContactService, Config,
}
  from '../../SERVICES';

class Initializing extends Component {

  constructor(props) {
    super(props);
    this.utilities = new Utilities();
    this.config = new Config();
    this.dbService = new DatabaseService(this.utilities);
    this.initService = new InitService(this.config, this.utilities, this.dbService);
    this.contactService = new ContactService(this.config, this.utilities, this.dbService);
  }

  componentDidMount() {
    //
    //dismiss Keyboard
    Keyboard.dismiss();

    //fetch all country codes 
    const ALL_COUNTRY_CODES = this.utilities.getAllCountryCodes();

    //create all database tables
    this.initService.createAllDbases()
      .then(() => {
        return AsyncStorage.multiSet([
          [USER_ID, this.props.userid.toString()],
          [USER_NAME, this.props.username],
          [USER_PHONE, this.props.phone],
          [USER_COUNTRY, this.props.country_code],
          [AFFILIATE_COUNTRIES, ALL_COUNTRY_CODES],
          [APP_VERSION, this.config.getAppVersion()],
          [THEME, DEFAULT_THEME],
        ]);
      })
      //start working on contacts stuff
      .then(() => {
        this.props.updateUserInfo({
          prop: 'initializing_msg',
          value: 'Searching for people you may know...'
        });
        return this.contactService.fetchContacts(); //fetch contacts from phone
      })
      .catch(err => {
        //this below ensures that even if we cant read contacts, we can move on...
        if (err === CONTACTS_PERMISSION_DENIED || err === FAIL_TO_READ_CONTACTS){
          return [];
        }
        else {
          throw err;
        }
      })
      .then(contacts => {
        //get list of contacts as a string to forward to API
        this.contactService.keepInRAM(contacts);
        //call API and save friend list in database
        return this.contactService.findAndSaveFriends(this.props.country_code);
      })
      .then(() => {
        if (this.props.existing_user) {
          this.props.updateUserInfo({
            prop: 'initializing_msg',
            value: 'Loading your previous settings...'
          });
          return this.initService.loadExistingSettings(this.props.userid);
        }
      })
      .then(() => {
        this.props.updateUserInfo({
          prop: 'initializing_msg',
          value: 'Almost good to go. Just a sec...'
        });
        return this.initService.setHasRunBefore()
      })
      .then(() => Actions.tabbar({ type: 'replace' })) //NAVIGATE TO MAIN)
      .catch(err => {
        this.utilities.handleErrors(err, 'ERROR INITIALIZING APP');
        this.utilities.initError(err);
      });
  }


  render() {
    return (
      <CenterPage >
        <Header title={'Initializing...'} />
        <SetupContent>
          <Spinner size={'large'} />
          <Text style={styles.help_msg}>{this.props.initializing_msg}</Text>
        </SetupContent>
      </CenterPage >
    );
  }
}


const styles = {
  help_msg: {
    color: '#777',
    marginBottom: 60,
    fontSize: 17
  }
};

const mapStateToProps = state => {
  return {
    initializing_msg: state.user.initializing_msg,
    userid: state.user.user_id,
    username: state.user.username,
    phone: state.user.phone,
    country_code: state.user.country_code,
    existing_user: state.user.existing_user
  };
};

export default connect(mapStateToProps, { updateUserInfo })(Initializing);
