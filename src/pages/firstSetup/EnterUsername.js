import React, { Component } from 'react';
import { Text, Keyboard, TextInput, View, Platform } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { updateUserInfo, usernameVerified } from '../../actions';
import { Header, CenterPage, SetupContent, Button, Spinner } from '../../common';
import { Config, Utilities, DatabaseService, UserService } from '../../SERVICES';


class EnterVCode extends Component {

    constructor(props) {
        super(props);
        this.disable_finish = true;

        const config = new Config();
        this.dbService = new DatabaseService(this.utilities);
        this.utilities = new Utilities();
        this.userService = new UserService(config, this.utilities, this.dbService);
    }

    takeUsername(value) {
        if (value.trim().length >= 3) {
            this.disable_finish = false;
        }
        else {
            this.disable_finish = true;
        }
        this.props.updateUserInfo({ prop: 'username', value });
    }

    keypressed(e) {
        if (e.key === 'Enter') {
            this.finish();
        }
    }

    finish() {
        if (!this.disable_finish) {
            Keyboard.dismiss();
            this.props.updateUserInfo({ prop: 'verifying_username', value: true });
            //check if name is valid/ the endpoint will automatically create the new
            //user if the username is valid
            this.userService.createUser(this.props.username, this.props.phone, this.props.country_code)
                .then(resp => {
                    if (resp.unavailable) {
                        this.props.updateUserInfo({ prop: 'verifying_username', value: false });
                        this.utilities.showToast('This name is already taken', 3000);
                    }
                    else {
                        this.props.usernameVerified(resp.user_id, resp.existing_user);
                        //move to initialization page
                        Actions.initializing();
                    }
                })
                .catch(() => {
                    this.props.updateUserInfo({ prop: 'verifying_username', value: false });
                });
        }
        else {
            this.utilities.showToast('Please use 3 characters at least', 3000);
        }
    }


    showFinishOrSpinner() {
        if (this.props.verifying_username) {
            return (
                <View>
                    <Spinner size={'small'} />
                    <Text>Verifying username. Please wait...</Text>
                </View>
            );
        }
        return (
            <Button
                text={'FINISH'}
                disabled={this.disable_finish}
                tapped={this.finish.bind(this)}
                styles={{ borderWidth: Platform.OS === 'ios' ? 1 : null }}
            />
        );
    }


    render() {
        return (
            <CenterPage>
                <SetupContent>
                    <Text style={styles.msg}>Enter Username:</Text>
                    <View style={styles.usernameView}>
                        <Text style={styles.at}>@</Text>
                        <TextInput
                            autoFocus
                            style={styles.username}
                            underlineColorAndroid={'transparent'}
                            placeholder={'username'}
                            keyboardType={'email-address'}
                            returnKeyType={'next'}
                            value={this.props.username}
                            maxLength={10}
                            onChangeText={this.takeUsername.bind(this)}
                            onKeyPress={this.keypressed.bind(this)}
                        />
                    </View>

                    <Text style={styles.help_msg}>
                        Finally, choose a username. You can change this later in your settings.
                    </Text>
                    {this.showFinishOrSpinner()}
                </SetupContent>

                <Header
                    title={'Username'}
                    showRightButton
                    btnIcon={'ios-checkmark'}
                    btnIconSize={38}
                    btnText={'FINISH'}
                    disableBtn={this.disable_finish}
                    onBtnPress={this.finish.bind(this)}
                    transparent
                />
            </CenterPage>
        );
    }
}

const styles = {
    msg: {
        color: '#555',
        marginBottom: 15
    },
    usernameView: {
        flexDirection: 'row',
        marginBottom: 30
    },
    at: {
        alignSelf: 'center',
        padding: 5,
        fontSize: 20,
        fontWeight: 'bold'
    },
    username: {
        paddingVertical: 4,
        paddingLeft: 10,
        flex: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#efefef'
    },
    help_msg: {
        color: '#333',
        marginBottom: 60,
        fontSize: 15
    }
};

const mapStateToProps = state => {
    return {
        username: state.user.username,
        verifying_username: state.user.verifying_username,
        phone: state.user.phone,
        phone_number: state.user.phone_number,
        country_code: state.user.country_code
    };
};

export default connect(mapStateToProps, { updateUserInfo, usernameVerified })(EnterVCode);
