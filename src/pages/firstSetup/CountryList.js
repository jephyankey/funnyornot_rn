import React, { Component } from 'react';
import { View, ListView, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { changeCountry } from '../../actions';
import { Header } from '../../common';
import countryList from '../../SERVICES/country_codes';


class CountryList extends Component {

    componentWillMount() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(countryList);
    }

    updateCountry(data) {
        this.props.changeCountry(data);
        Actions.pop();
    }

    renderRow(data) {
        return (
            <TouchableOpacity
                style={styles.countryView}
                onPress={this.updateCountry.bind(this, data)}
                >
                <Text style={styles.country}>{data.country}</Text>
                <Text style={styles.phone_code}>{data.phone_code}</Text>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title={'Select Your Country'} showBackButton={'true'} />
                <View style={styles.fulllist}>
                    <ListView
                        dataSource={this.dataSource}
                        renderRow={this.renderRow.bind(this)}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                        />
                </View>
            </View>
        );
    }

}


const styles = {
    container: {
        flex: 1
    },
    fulllist: {
        flex: 1,
        padding: 20
    },
    countryView: {
        flexDirection: 'row',
        // borderBottomWidth: 1,
        // borderBottomColor: '#eee',
        padding: 10,
        marginBottom: 3,
    },
    separator: {
        flex: 1,
        height: 1,
        backgroundColor: '#EEE',
    },
    country: {
        flex: 1,
        fontSize: 16,
        color: '#333'
    },
    phone_code: {
        color: '#aaa',
        fontWeight: '500'
    }
};


export default connect(null, { changeCountry })(CountryList);
