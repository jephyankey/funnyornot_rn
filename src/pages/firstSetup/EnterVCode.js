import React, { Component } from 'react';
import { Text, TextInput, View, Platform } from 'react-native';
import { connect } from 'react-redux';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
// import SmsListener from 'react-native-android-sms-listener'

import { updateUserInfo } from '../../actions';
import { Header, CenterPage, SetupContent, Button, Properties } from '../../common';
import { Config, Utilities } from '../../SERVICES';


class EnterVCode extends Component {

    constructor(props) {
        super(props);
        this.disable_next = true;
        this.typed_v_code = '';
        this.utils = new Utilities();
        this.phone = null;
        this.last_request_time = 0;
        this.set_interval = null;
    }


    componentWillMount() {
        this.config = new Config();

        //remove the trailing 0 from the number if any, and join to the phone code
        let num = this.props.phone_number;
        num = num.substring(0, 1) === '0' ? num.substring(1) : num;
        this.phone = `${this.props.phone_code}${num}`;
        this.props.updateUserInfo({ prop: 'phone', value: this.phone });

        this.requestVCode();
    }


    requestVCode() {
        axios.get(`${this.config.getBaseUrl()}/phoneVerifyCode/${encodeURIComponent(this.phone)}`)
            .then(resp => {
                //attempt auto verification
                // const subscription = SmsListener.addListener(msg => {
                //     console.info(msg);
                //     if (msg.originatingAddress === '+1 626-741-1492' && msg.body == resp.data.code)
                //         this.takeVCode(resp.data.code);
                //     subscription.remove();
                // });
                this.props.updateUserInfo({ prop: 'actual_v_code', value: resp.data.code });
            })
            .catch(err => {
                this.utils.handleErrors(err, 'error requesting the verification code');
            });
        this.set_interval = setInterval(() => { this.last_request_time++; }, 1000);
    }


    takeVCode(value) {
        this.props.updateUserInfo({ prop: 'typed_v_code', value });
        if (value.length === 4) {
            if (value === this.props.actual_v_code) {
                this.disable_next = false;
                Actions.enterUsername();
            }
            else {
                this.disable_next = true;
                this.utils.showToast('Invalid Code');
            }
        }
    }

    resendCode() {
        const time = 60 - this.last_request_time;
        let statement = '';
        if (time === 1)
            statement = `${time} more second`;
        else
            statement = `${time} more seconds`;
        if (this.last_request_time <= 60) //the last request should be at least 6 seconds ago
            this.utils.showToast(`Hang on! Let's give it ${statement}`);
        else {
            this.last_request_time = 0;
            this.requestVCode();
            this.utils.showToast('Confirmation SMS will be resent!');
        }
    }

    keypressed(e) {
        if (e.key === 'Enter') {
            this.next();
        }
    }

    next() {
        if (!this.props.typed_v_code || this.props.typed_v_code.length < 4) {
            this.utils.showToast('Code should be 4 characters', 3000, 'ios-sad-outline');
        }
        else {
            this.takeVCode(this.props.typed_v_code);
        }
    }


    render() {
        return (
            <CenterPage>
                <SetupContent>
                    <Text style={styles.msg}>Enter Code</Text>
                    <TextInput
                        autoFocus
                        style={styles.vcode}
                        underlineColorAndroid={'transparent'}
                        placeholder={'Eg. 1234'}
                        keyboardType={'numeric'}
                        returnKeyType={'next'}
                        value={this.props.typed_v_code}
                        maxLength={4}
                        onChangeText={this.takeVCode.bind(this)}
                        onKeyPress={this.keypressed.bind(this)}
                    />
                    <Text style={styles.help_msg}>
                        We are going to send you an SMS with a verification code
                        <Text style={{ fontWeight: 'bold' }}>{` (${this.props.phone})`}</Text>
                    </Text>
                    <Button
                        tapped={this.resendCode.bind(this)}
                        text={'RESEND SMS'}
                        textColor={Properties.themeColor}
                        styles={styles.resendButton}
                    />
                    <Button
                        text={'NEXT'}
                        disabled={this.disable_next}
                        tapped={this.next.bind(this)}
                        styles={{ borderWidth: Platform.OS === 'ios' ? 1 : null }}
                    />
                </SetupContent>
                <Header
                    title={'Verification'}
                    showRightButton
                    showBackButton
                    btnIcon={'ios-checkmark'}
                    btnIconSize={38}
                    btnText={'NEXT'}
                    disableBtn={this.disable_next}
                    onBtnPress={this.next.bind(this)}
                    transparent
                />
            </CenterPage>
        );
    }

    componentWillUnMount() {
        this.set_interval = null;
    }
}


const styles = {
    msg: {
        color: '#555',
        marginBottom: 15
    },
    vcode: {
        minWidth: 130,
        borderBottomWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#efefef',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        color: '#555',
        paddingVertical: 4,
        marginBottom: 30,
        height: 40
    },
    help_msg: {
        color: '#333',
        fontSize: 15,
        textAlign: 'center',
        marginBottom: 3
    },
    resendButton: {
        marginBottom: 50,
        borderWidth: 0,
        backgroundColor: 'transparent',
        alignSelf: 'flex-start'
    }
};

const mapStateToProps = state => {
    return {
        typed_v_code: state.user.typed_v_code,
        actual_v_code: state.user.actual_v_code,
        phone_code: state.user.phone_code,
        phone_number: state.user.phone_number,
        phone: state.user.phone
    };
};

export default connect(mapStateToProps, { updateUserInfo })(EnterVCode);
