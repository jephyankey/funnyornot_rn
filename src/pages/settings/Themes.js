import React, { Component } from 'react';
import {
    View, AsyncStorage,
} from 'react-native';

import { RadioItem } from '../base';
import { THEME, NEW_THEME_CHOSEN } from '../../types.js';

import { Header, SettingsView, Properties } from '../../common';

import {
    DEFAULT_THEME, BLUE_DINAH, BLACK_MAMBA, GREEN_ELDY, RED_LUCY, PURPLE_BARBIE, PINK_LINA,
} from '../../common/Properties';
import { EventEmitter } from '../../SERVICES';


class ThemesPage extends Component {

    state = {
        theme_name: null
    };

    componentWillMount() {
        AsyncStorage.getItem(THEME)
            .then(theme => {
                this.setState({
                    theme_name: theme || DEFAULT_THEME
                });
            });
    }


    setTheme(theme) {
        this.setState({ theme_name: theme });
        AsyncStorage.setItem(THEME, theme);
        Properties.mainColor = theme;
        EventEmitter.dispatch(NEW_THEME_CHOSEN, theme);
    }



    render() {
        return (
            <View style={{ flex: 1 }}>
                <SettingsView>
                    <RadioItem
                        text={'Dark Wine'}
                        subText={'Default Theme'}
                        isChecked={this.state.theme_name === DEFAULT_THEME}
                        checkedColor={DEFAULT_THEME}
                        onTap={this.setTheme.bind(this, DEFAULT_THEME)}
                        />
                    <RadioItem
                        text={'Blue Dinah'}
                        isChecked={this.state.theme_name === BLUE_DINAH}
                        checkedColor={BLUE_DINAH}
                        onTap={this.setTheme.bind(this, BLUE_DINAH)}
                        />
                    <RadioItem
                        text={'Black Mamba'}
                        isChecked={this.state.theme_name === BLACK_MAMBA}
                        checkedColor={BLACK_MAMBA}
                        onTap={this.setTheme.bind(this, BLACK_MAMBA)}
                        />
                    <RadioItem
                        text={'Green Eldy'}
                        isChecked={this.state.theme_name === GREEN_ELDY}
                        checkedColor={GREEN_ELDY}
                        onTap={this.setTheme.bind(this, GREEN_ELDY)}
                        />
                    <RadioItem
                        text={'Pink Lina'}
                        isChecked={this.state.theme_name === PINK_LINA}
                        checkedColor={PINK_LINA}
                        onTap={this.setTheme.bind(this, PINK_LINA)}
                        />
                    <RadioItem
                        text={'Purple Barbie'}
                        isChecked={this.state.theme_name === PURPLE_BARBIE}
                        checkedColor={PURPLE_BARBIE}
                        onTap={this.setTheme.bind(this, PURPLE_BARBIE)}
                        />
                    <RadioItem
                        text={'Red Lucy'}
                        isChecked={this.state.theme_name === RED_LUCY}
                        checkedColor={RED_LUCY}
                        onTap={this.setTheme.bind(this, RED_LUCY)}
                        />
                </SettingsView>
                <Header title={'Themes'} showBackButton transparent />
            </View>
        );
    }
}

export default ThemesPage;
