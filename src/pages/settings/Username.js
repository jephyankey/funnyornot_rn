import React, { Component } from 'react';
import { View, Keyboard, Platform, Text, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { Header, CenterPage, SetupContent, Button, Spinner, Properties } from '../../common';

import {
    UserService,
    Utilities,
    DatabaseService,
    Config
} from '../../SERVICES';


class UsernameSettingsPage extends Component {

    constructor(props) {
        super(props);
        this.utilities = new Utilities();
        this.dbService = new DatabaseService(this.utilities);
        this.userService = new UserService(new Config(), this.utilities, this.dbService);
    }


    state = {
        previous_username: null,
        typed_username: '',
        verifying_username: false,
        disable_finish: true
    };

    componentWillMount() {
        this.userService.getCurrentUsersName()
        .then(name => {
            this.setState({ previous_username: name });
        });
    }

    updateUserName() {
        if (!this.state.disable_finish) {
            Keyboard.dismiss();
            this.setState({ verifying_username: true });
            this.userService.updateUserName(this.state.typed_username)
                .then(message => {
                    this.setState({ verifying_username: false });
                    if (message === 'unavailable') {
                        this.utilities.showToast('This username is already taken');
                    }
                    else {
                        this.utilities.showToast('Username changed successfully');
                        this.userService.setUserName(this.state.typed_username);
                        Actions.pop();
                    }
                })
                .catch(() => {
                    this.setState({ verifying_username: false });
                    this.utilities.showToast('Could not update username');
                });
        }
        else {
            if (this.state.typed_username.length < 3)
                this.utilities.showToast('Please use 3 characters at least');
            else if (this.state.typed_username === this.state.previous_username)
                this.utilities.showToast('Username is still the same');
        }
    }

    takeUsername(value) {
        if (value.trim().length < 3 || this.state.previous_username === value) {
            this.disable_finish = true;
        }
        else {
            this.disable_finish = false;
        }
        this.setState({ typed_username: value, disable_finish: this.disable_finish });
    }

    showFinishOrSpinner() {
        if (this.state.verifying_username) {
            return (
                <View>
                    <Spinner size={'small'} />
                    <Text>Checking name. Please wait...</Text>
                </View>
            );
        }
        return (
            <Button
                text={'CHANGE'}
                disabled={this.state.disable_finish}
                tapped={this.updateUserName.bind(this)}
                styles={styles.btn}
                textColor={Platform.OS === 'ios' ? Properties.themeColor : '#fff'}
                disabledTextColor={Platform.OS === 'ios' ? Properties.themeColor : '#fff'}
                />
        );
    }


    render() {
        return (
            <CenterPage>
                <SetupContent>
                    <View style={styles.usernameView}>
                        <Text style={styles.at}>@</Text>
                        <TextInput
                            style={styles.username}
                            underlineColorAndroid={'transparent'}
                            placeholder={this.state.previous_username}
                            keyboardType={'email-address'}
                            returnKeyType={'done'}
                            value={this.state.typed_username}
                            maxLength={15}
                            onChangeText={this.takeUsername.bind(this)}
                            />
                    </View>
                    {this.showFinishOrSpinner()}
                </SetupContent>
                <Header title={'Change Username'} showBackButton transparent />
            </CenterPage>
        );
    }
}


const styles = {
    usernameView: {
        flexDirection: 'row',
        marginBottom: 30
    },
    at: {
        alignSelf: 'center',
        padding: 5,
        fontSize: 20,
        fontWeight: 'bold'
    },
    username: {
        paddingVertical: 5,
        paddingLeft: 10,
        flex: 5,
        borderBottomWidth: 1,
        borderColor: '#eee',
        backgroundColor: '#efefef'
    },
    btn: {
        borderWidth: Platform.OS === 'ios' ? 1 : null
    }
};


export default UsernameSettingsPage;
