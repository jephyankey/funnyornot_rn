import React, { Component } from 'react';
import { Platform, View, TextInput, Dimensions } from 'react-native';
import axios from 'axios';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { Properties, Header, MyText } from '../../common';
import { Utilities, Config, UserService, DatabaseService } from '../../SERVICES';
import { IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT } from '../../types';

class FeedbackPage extends Component {


    state = {
        feedback_text: '',
    }

    constructor(props) {
        super(props);
        this.utilities = new Utilities();
        this.config = new Config();
        this.userService = new UserService(this.config, this.utilities, new DatabaseService(this.utilities));
    }


    submitFeedback() {
        if (this.state.feedback_text.trim().length < 10) {
            this.utilities.showToast('Oh, how about 10 characters at least!');
        }
        else {
            this.setState({ feedback_text: '' });
            this.utilities.showToast('Feedback submitted. You are a legend!');
            this.userService.getCurrentUsersId()
                .then(user_id => {
                    axios.post(`${this.config.getBaseUrl()}/feedback/${user_id}`);
                });
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <KeyboardAwareScrollView extraScrollHeight={-50} style={styles.page}>
                    <MyText styles={styles.intro}>
                        We'll be grateful if you could give us some advice, report
                        bugs or simply tell us what you think. Be nice.
                    </MyText>
                    <View style={{ flex: 1, backgroundColor: 'red' }}>
                        <TextInput
                            multiline
                            maxLength={500}
                            placeholder={'Write something here'}
                            placeholderTextColor={'#bbb'}
                            numberOfLines={11}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(text) => {
                                this.setState({ feedback_text: text });
                            }}
                            style={styles.textInput}
                            value={this.state.feedback_text}
                        />
                    </View>
                </KeyboardAwareScrollView>

                <Header
                    title={'Feedback'}
                    showBackButton
                    showRightButton
                    btnIcon={Platform.OS === 'android'? 'md-send' : 'ios-send'}
                    btnText={'SEND'}
                    onBtnPress={this.submitFeedback.bind(this)}
                    transparent
                />

            </View>
        );
    }

}



const styles = {
    page: {
        flex: 1,
        backgroundColor: '#fff'
    },
    intro: {
        backgroundColor: Properties.greyBackColor,
        paddingHorizontal: 26,
        paddingBottom: 30,
        fontSize: 16,
        paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT + HEADER_HEIGHT + 10 : HEADER_HEIGHT + 10
    },
    textInput: {
        backgroundColor: '#fff',
        fontSize: 16,
        paddingLeft: 17,
        paddingRight: 17,
        paddingBottom: 20,
        paddingTop: 20,
        textAlignVertical: 'top',
        height: (0.5 * Dimensions.get('window').height) - IOS_STATUS_BAR_HEIGHT - HEADER_HEIGHT
    },
    textInputView: {
        flex: 1
    }
};


export default FeedbackPage;
