import React, { Component } from 'react';
import { Platform, WebView, View, Dimensions, InteractionManager } from 'react-native';
import { Header, Spinner, Properties } from '../../common';
import { ErrorCard } from '../base';
import { NO_INTERNET2, IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT } from '../../types';
import { Config } from '../../SERVICES';


class WebPage extends Component {

    constructor(props) {
        super(props);
        this.config = new Config();
        this.inject_js = null;
    }

    state = {
        loaded: false,
        error: false,
        show_webview: false
    };

    componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({ show_webview: true });
        })
    }

    renderContent() {
        if (!this.state.loaded) {
            return (
                <View
                    style={{
                        position: 'absolute',
                        top: (Dimensions.get('window').height / 2) - 50 + HEADER_HEIGHT,
                        left: (Dimensions.get('window').width / 2) - 20 
                    }}
                >
                    <Spinner />
                </View>
            );
        }
        else if (this.state.error) {
            return <ErrorCard errorState={NO_INTERNET2} />;
        }
    }

    render() {
        let websource = '';

        if (this.props.webSource === 'PRIVACY')
            websource = 'privacypolicy';
        else if (this.props.webSource === 'TERMS')
            websource = 'terms';
        else if (this.props.webSource === 'HELP') {
            websource = 'help';
            this.inject_js = `
                elements = document.getElementsByClassName("accordion");
                for (var i = 0; i < elements.length; i++) {
                    elements[i].style.color="${Properties.themeColor}";
                }`;
        }

        return (
            <View style={styles.webview}>
                {this.state.error === false && this.state.show_webview ?
                    <WebView
                        source={{ uri: `${this.config.getBaseUrl()}/d/${websource}.html` }}
                        onLoad={() => this.setState({ loaded: true })}
                        onError={() => this.setState({ error: true })}
                        injectedJavaScript={this.inject_js}
                    /> : null}
                {this.renderContent()}
                <Header title={this.props.title} showBackButton transparent />
            </View>
        );
    }

}

const styles = {
    webview: {
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT + HEADER_HEIGHT + 10 : HEADER_HEIGHT,
    }
};

export default WebPage;
