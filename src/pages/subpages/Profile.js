import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon8 from '../../common/Icon';
import {
  View, Text, Dimensions, Animated,
  Modal, TouchableWithoutFeedback, TouchableOpacity, Image,
  LayoutAnimation, Platform, UIManager, InteractionManager,
  ListView
} from 'react-native';

import { Header, Properties, Spinner, Button, MyText, StatusBarIos } from '../../common';
import { detectCountry } from '../../actions';
import { FeedCard, Avatar } from '../base';

import {
  FeedService, iFeedService,
  ContactService,
  Utilities,
  Config,
  DatabaseService,
  UserService,
  FileService,
  AvatarService,
  EventEmitter
} from '../../SERVICES';

import {
  //errors
  NO_INTERNET, LOAD_FAIL, EMPTY_RESULT,
  NEW_THEME_CHOSEN,
  SCROLLING_THROUGH_FEED, TAB_OR_VIEW_CHANGED,
  HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT,
  PROFILE
} from '../../types';

const FUN_FACTOR_UNIT_SIZE = 250;



class ProfilePage extends Component {

  /**
   * for performance reaswons, this page uses a list view to render everything except the header obviously
   * the data_source (this.state.dataSource) has some initial dummy data, which acts as the first row_id
   * this allows us to use the first row to render the items that are not really part of the list;
   * mainly the avatar, username and basic info on the user
   */

  propTypes: {
    profile_id: React.PropTypes.number.isRequired,
    name: React.PropTypes.string.isRequired,
    avatar_url: React.PropTypes.string,
    tabbedVersion: React.PropTypes.bool
  }

  feedService: iFeedService;
  users_media_page: number = 1;
  chk_for_updateFollowers: number = 0; //used to prevent updateFollowers() from falling into an infinite loop
  profile_id: number = null;


  constructor(props) {
    super(props);
    this.config = new Config();
    this.utilities = new Utilities();
    this.dbService = new DatabaseService(this.utilities);
    const contactService = new ContactService(this.config, this.utilities, this.dbService);
    this.userService = new UserService(this.config, this.utilities, this.dbService);
    this.feedService = new FeedService(this.config, this.utilities, this.userService,
      contactService, this.dbService);
    this.fileService = new FileService(this.config, this.utilities);
    this.avatarService = new AvatarService(this.config, this.utilities, this.dbService);

    this.should_fetch_more = true;

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    this.ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
  }

  state = {
    data_source: [{ show_profile_data: 1 }],
    is_following: null, // is the viewer following this user or not
    no_of_followers: '...',
    no_following: '...',
    fun_factor: '...',
    blocked: null,
    new_avatar_url: null,
    show_spinner: true,
    error: null,

    show_avatar_full_view: false,
    show_more_loader: true,
    remove_clipped_subviews: false
  };

  componentDidMount() {
    //check if this is a blocked user
    this.userService.isBlocked(this.props.profile_id)
      .then((val) => {
        if (val === true)
          this.setState({ blocked: true });
        else
          this.setState({ blocked: false });
      });


    InteractionManager.runAfterInteractions(() => {
      if (this.props.name === 'You') {
        this.userService.getCurrentUsersId()
          .then(user_id => {
            this.fetchReleventProfileData(user_id);
            this.fetchUsersJokes(user_id);
          });
      }
      else if (this.props.profile_id) {
        this.userService.getCurrentUsersId()
          .then(user_id => {
            this.fetchReleventProfileData(this.props.profile_id, user_id);
            this.fetchUsersJokes(this.props.profile_id);
          });
      }

      //STOP all playing videos if anywhere
      //this is useful when we move from playing a video to this page, 
      //this event will cause that video to stop playing
      EventEmitter.dispatch(TAB_OR_VIEW_CHANGED);

      //subscritbe to new theme UPDATES'
      this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
        this.forceUpdate();
      });
    });
  }

  /**
   * fetches fun factor, no of folowers, no following
   */
  fetchReleventProfileData(profile_id, viewer_id = null) {
    this.userService.fetchReleventProfileData(profile_id, viewer_id)
      .then(data => {
        const is_foll = viewer_id === null ? null : !!parseInt(data.is_following, 10); //converted to boolean by !!
        this.setState({
          fun_factor: this._calculateFunFactor(data.fun_factor),
          no_of_followers: parseInt(data.followers, 10),
          no_following: parseInt(data.following, 10),
          is_following: is_foll
        });
      })
      .catch(err => {
        this.utilities.handleErrors(err);
      });
  }
  _calculateFunFactor(raw_factor) {
    let dividend;
    let i = 1;
    let fun_factor = 0.0;
    let the_raw_factor = raw_factor;
    const UNIT_SIZE = FUN_FACTOR_UNIT_SIZE;

    while (the_raw_factor > 0) {
      dividend = the_raw_factor > UNIT_SIZE ? UNIT_SIZE : the_raw_factor;
      fun_factor += (dividend / i);
      the_raw_factor -= UNIT_SIZE;
      i++;
    }
    return fun_factor.toFixed(1);
  }

  //set name value to be the name of owner of this profile page//
  //this operation is the equivalent of updateFeedParams() in the regular feed (Feed.js)
  updateFeedParams(data) {
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      element.name = this.props.name;
      //stars or funnies
      element.funnyORnot = element.stars;
      if (element.stars === null) {
        element.stars = 0;
      }
    }
    return data;
  }


  fetchUsersJokes(profile_id) {
    this.should_fetch_more = false;

    if (this.profile_id === null) {
      this.profile_id = profile_id;
      /**
       * this is necessary cos this whole function is used for infinite scrolling
       * the correct profile_id is passed when the page is first loaded,
       * however for subsequent pages, this is not the case, hence we store the profile_id 
       * when it is first passed into this.profile_id so it can be used later for the pagination
       */
    }
    this.feedService.getSomeonesJokes(this.profile_id, this.users_media_page)
      .then(data => {
        if (data.length === 0 && this.users_media_page === 1) {
          throw EMPTY_RESULT;
        }
        else {
          const updated_data = this.updateFeedParams(data);
          if (this.users_media_page === 1)
            this.setState({ show_spinner: false, data_source: this.state.data_source.concat(updated_data) });
          else
            this.setState({ data_source: this.state.data_source.concat(updated_data) });

          //increase page number
          this.users_media_page++;

          //should we still load more???????
          if (data.length < this.feedService.getPageSize())
            this.setState({ show_more_loader: false }); /** we are done loading more from server
              this.should_fetch_more is left false as it is, */
          else
            this.should_fetch_more = true;
        }
      })
      .catch(err => {
        this.utilities.handleErrors(err);
        //if we get an error while loading the first page, then show some error message directly
        //on the page

        if (this.users_media_page === 1) {
          this.setState({ show_spinner: false, error: err, show_more_loader: false });
        }
        else {
          this.setState({ show_more_loader: false });
        }
        //note that if we are on page > 1, then the spinner (not the footer spinner) will not be shown
        //in the first place...
      });
  }


  followOrUnfollow() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    if (this.props.name === 'You') {
      this.utilities.showToast('Why are you trying to follow yourself? Are you okay?');
    }
    else if (this.state.is_following === true) {
      //OPTIMISTIC UI UPDATES
      this.setState({ is_following: false });
      this.updateFollowers('sub');
      this.utilities.showToast(`You have stopped following ${this.props.name}`);
      //actual request to stop following the user is made here
      this.userService.unfollow(this.props.profile_id, this.props.name, this.props.avatar_url)
        .catch(() => {
          this.setState({ is_following: true });
          this.updateFollowers('add');
          this.utilities.showToast('Nope! Something went wrong. Still following.');
        });
    }
    else if (this.state.is_following === false) {
      //OPTIMISTIC UI UPDATES
      this.setState({ is_following: true });
      this.updateFollowers('add');
      this.utilities.showToast(`You are now following ${this.props.name}`);
      //actual request to follow the user is now made
      this.userService.follow(this.props.profile_id, this.props.name, this.props.avatar_url)
        .catch(() => { //if something go es wrong, revert changes
          this.setState({ is_following: false });
          this.updateFollowers('sub');
          this.utilities.showToast('Nope! Something went wrong. You cant follow.');
        });
    }
  }

  goToFollPage(page) {
    if (this.props.current_users_id !== this.props.profile_id) {
      this.utilities.showToast("You can only view this for your own page");
      return;
    }
    switch (page) {
      case 'following':
        Actions.followingPage({ fromPage: PROFILE });
        break;
      case 'followers':
        Actions.followersPage({ fromPage: PROFILE });
        break;
      default:
    }
  }


  /**
   * add or subtract 1 from the number of followers shown in the view
   */
  updateFollowers(action) {
    //because this.no_of_followers is obtained from the server,
    //we may not have it yet, in which case if we do +/- end result will be NAN
    //so poll the value, till it is ready, but dont do it infinitely
    if (this.state.no_of_followers == null) {
      this.chk_for_updateFollowers++;
      setTimeout(() => {
        if (this.chk_for_updateFollowers < 10) //prevent accidental infinite loop
          this.updateFollowers(action);
      }, 1000);
    }
    else {
      if (action === 'add')
        this.setState({ no_of_followers: this.state.no_of_followers + 1 });
      else if (action === 'sub')
        this.setState({ no_of_followers: this.state.no_of_followers - 1 });
      this.chk_for_updateFollowers = 0;
    }
  }

  block() {
    //check internet connection
    this.utilities.showToast('Blocking...');
    this.userService.block(this.props.profile_id, this.props.name)
      .then(() => {
        this.utilities.showToast(`${this.props.name} has been blocked`);
        Actions.pop();
      })
      .catch(err => {
        if (err === NO_INTERNET)
          this.utilities.showToast('No Internet Connection');
        this.utilities.showToast(`Something went wrong! ${this.props.name} refuses to be blocked!`);
      });
  }


  uploadAvatar() {
    let new_url;
    let metadata;
    this.fileService.openGallery(1)
      .then(uri_data => {
        new_url = uri_data.uri;
        metadata = `{ "width": ${uri_data.width}, "height": ${uri_data.height} }`;
        this.setState({ new_avatar_url: new_url });
      })
      .then(() => {
        return this.fileService.upload(new_url, 1, `${Math.floor(Date.now() / 1000)}_${this.props.profile_id}`,
          [['is_avatar', 'true'], ['user_id', this.props.profile_id], ['metadata', metadata]]);
      })
      .then(response => {
        this.avatarService.setCurrentUsersAvatar(response.url);
        this.utilities.showToast('Profile Picture updated!');
      })
      .catch(err => {
        if (err !== 'CANCELLED') {
          this.utilities.handleErrors(err, 'ERROR: UPDATING PROFILE PICTURE');
          this.utilities.showToast('We are sorry. Something went wrong.');
        }
        this.setState({ new_avatar_url: null }); //undo OPTIMISTIC update
      });
  }


  closeAvatarFullview() {
    this.setState({ show_avatar_full_view: false });
  }

  showAvatarFullview() {
    if (this.state.new_avatar_url || this.props.avatar_url)
      this.setState({ show_avatar_full_view: true });
  }

  onScroll() {
    //dispatch an event, which will be received by all videos on the profile page
    //saying that the user is scrolling
    EventEmitter.dispatch(SCROLLING_THROUGH_FEED, null);

    if (Platform.OS === 'ios') {
      if (this.state.remove_clipped_subviews === false) {
        this.setState({ remove_clipped_subviews: true });
      }
    }
  }

  loadMore() {
    /**
     * when the page is first loaded, ListView's onEndReached is activated and this function is
     * called, however we do not want to run this function until we know we have at least loaded
     * the first set of data if any, hence the second condition
     */
    if (this.should_fetch_more && this.state.data_source.length > 1) {
      this.fetchUsersJokes();
    }
  }



  /**
   * show upload button, follow button, or alreday following button
   */
  renderButton() {
    if (this.props.name === 'You') {
      return (
        <Button
          iconOnly
          icon={'md-camera'} textColor={'#fff'}
          iconSize={27} iconPack={'ionicons'}
          styles={styles.button}
          tapped={this.uploadAvatar.bind(this)}
        />
      );
    }
    else if (this.state.is_following === false) {
      return (
        <Button
          iconOnly
          icon={'user-plus'}
          iconPack={'funnyornot'}
          textColor={'#fff'}
          iconSize={22}
          styles={styles.button}
          tapped={this.followOrUnfollow.bind(this)}
        />
      );
    }
    else if (this.state.is_following === true) {
      return (
        <Button
          iconOnly
          icon={'user-minus'}
          iconPack={'funnyornot'}
          textColor={'#fff'}
          iconSize={22}
          styles={styles.button}
          tapped={this.followOrUnfollow.bind(this)}
        />
      );
    }
  }

  renderRow(data, section_id, row_id) {
    if (parseInt(row_id, 10) === 0) {
      //this is the first render, show the avatar and basic info
      if (data.show_profile_data === 1) {
        //
        let err_msg = '';
        const avatar_url = this.state.new_avatar_url || this.props.avatar_url;
        const f = this.state.no_of_followers;

        if (this.state.error) {
          const prefer_or_prefers = this.props.name === 'You' ? 'prefer' : 'prefers';
          const e = this.state.error;
          if (e === NO_INTERNET)
            err_msg = 'We cant seem to find the internet anywhere!';
          else if (e === EMPTY_RESULT)
            err_msg = `Apparently, ${this.props.name} ${prefer_or_prefers} not to tell jokes.`;
          else if (e === LOAD_FAIL)
            err_msg = "Please don't laugh. Something has gone wrong. We are looking into it.";
        }

        return (
          <View key={0}>
            {
              this.props.tabbedVersion ?
                <View style={{ ...styles.pseudoHeader, backgroundColor: Properties.themeColor }}></View>
                : null
            }
            <View style={styles.topContainer}>
              <View
                style={{
                  ...styles.redBox,
                  backgroundColor: Properties.themeColor
                }}>
              </View>
              <View style={styles.avatarContainer}>
                <Avatar forProfile size={155} url={avatar_url} onTap={this.showAvatarFullview.bind(this)} />
                <View style={styles.iconView}>{this.renderButton()}</View>
              </View>
            </View>
            <View style={styles.infoView}>
              <View style={styles.nameView}>
                <Text style={styles.userName}>{this.utilities.truncateString(this.props.name)}</Text>
              </View>
              <View style={styles.otherInfoView}>
                <View>
                  <Text style={styles.infoBold}>{this.state.fun_factor}</Text>
                  <Text style={styles.infoLight}>fun factor</Text>
                </View>
                <TouchableOpacity onPress={this.goToFollPage.bind(this, 'followers')}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={styles.oneInfo}>
                      <Text style={styles.infoBold}>{f}</Text>
                      <Text style={styles.infoLight}>follower{f === 1 ? null : 's'}</Text>
                    </View>
                    {
                      this.state.is_following === true ?
                        <Animated.View style={{ marginRight: 10, justifyContent: 'center' }}>
                          <Icon name={'md-checkmark'} size={20} color={'#999'} />
                        </Animated.View> : null
                    }
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.goToFollPage.bind(this, 'following')}>
                  <View style={styles.oneInfo}>
                    <Text style={styles.infoBold}>{this.state.no_following}</Text>
                    <Text style={styles.infoLight}>following</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            {this.state.show_spinner ?
              <View style={styles.gallerySpinner}><Spinner size={Platform.OS === 'ios' ? 'small' : 30} /></View>
              :
              this.state.error ?
                <View style={styles.generalError}><MyText styles={styles.errorText}>{err_msg}</MyText></View>
                : null
            }
          </View>
        );
      }
    }
    else return <FeedCard feedItem={data} key={data.post_id} />;
  }

  renderContent() {
    this.dataSource = this.ds.cloneWithRows(this.state.data_source);
    return (
      <ListView
        onScroll={this.onScroll.bind(this)}
        style={{ flex: 1 }}
        dataSource={this.dataSource}
        renderRow={this.renderRow.bind(this)}
        renderFooter={this.renderFooter.bind(this)}
        onEndReached={this.loadMore.bind(this)}
        enableEmptySections
        removeClippedSubviews={(Platform.OS === 'android') ? true : this.state.remove_clipped_subviews}
      />
    );
  }

  renderFooter() {
    /**
     * the reason for the double condition here is the same as in the function
     * loadMore()
     */
    if (this.should_fetch_more && this.state.data_source.length > 1) {
      return (
        <View style={styles.loader}><Spinner size={Platform.OS === 'ios' ? 'small' : 30} /></View>
      );
    }
    return <View style={{ marginBottom: 40 }} />;
  }


  renderHeader() {
    if (this.props.tabbedVersion) {
      return;
    }
    return (
      <Header
        showBackButton
        noBorder
        showRightButton={!(this.props.name === 'You' || this.state.blocked === true)}
        btnIcon={'block'}
        btnIconPack={'funnyornot'}
        rightButtonColor={'#fff'}
        rightButtonStyles={{ marginBottom: Platform.OS === 'ios' ? -1.7 : 0 }}
        btnIconSize={23}
        backButtonColor={'#fff'}
        onBtnPress={this.block.bind(this)}
        backgroundColor={Properties.themeColor}
        useThemeForStatusBar
      />
    );
  }

  renderStatusBarIos() {
    if (this.props.tabbedVersion) {
      if (Platform.OS === 'ios') {
        return <StatusBarIos useThemeForStatusBar />;
      }
    }
  }

  render() {
    return (
      <View
        style={{
          ...styles.pageMain,
          paddingTop: Platform.OS === 'ios' ?
            (this.props.tabbedVersion ? IOS_STATUS_BAR_HEIGHT : (HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT)) : null
        }}>
        {this.renderStatusBarIos()}
        {this.renderHeader()}
        <View style={styles.page}>
          {this.renderContent()}
          {
            //show big blocked button over the entire view if this user is blocked
            this.state.blocked === true ?
              <View style={styles.blockedView}>
                <Icon8 name={'block'} size={180} color={Properties.themeColor} />
              </View> : null
          }
        </View >

        <Modal
          animationType={'slide'}
          transparent
          visible={this.state.show_avatar_full_view}
          onRequestClose={() => this.setState({ show_avatar_full_view: false })}
        >
          <TouchableWithoutFeedback onPress={this.closeAvatarFullview.bind(this)}>
            <View style={styles.modalBack}>
              <Image
                style={styles.avatarFullview}
                source={{ uri: this.state.new_avatar_url || this.props.avatar_url }}
                resizeMode="contain"
              />
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View >
    );
  }

  componentWillUnmount() {
    EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
  }

}


const styles = {
  pageMain: {
    flex: 1,
    backgroundColor: 'transparent',
    //make room for status bar in ios
  },
  page: {
    flex: 1,
    backgroundColor: '#eee',
  },
  topContainer: {
    marginTop: -7,
    height: 170, //height of redBox + half of imageBox - some small trial and error value
  },
  redBox: {
    height: 100,
    alignItems: 'center',
    overflow: 'visible'
  },
  avatarContainer: {
    position: 'absolute',
    bottom: 0,
    left: (Dimensions.get('window').width / 2) - 80.5,
  },
  iconView: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    backgroundColor: '#F1B2AB',
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,
    width: 42,
    height: 42,
    borderWidth: 2,
    borderColor: '#eee'
  },
  infoView: {
    paddingLeft: 15,
    marginTop: 15,
    paddingRight: 40
  },
  userName: {
    color: '#222',
    fontWeight: 'bold',
    fontSize: 31,
    marginBottom: 20,
  },
  otherInfoView: {
    flexDirection: 'row',
    marginBottom: 40,
    justifyContent: 'space-between',
    // paddingRight: 20
  },
  oneInfo: {
  },
  infoBold: {
    color: '#666',
    fontWeight: 'bold',
    fontSize: 20
  },
  infoLight: {
    fontSize: 16
  },
  gallerySpinner: {
    marginTop: 30,
  },
  button: {
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    backgroundColor: 'transparent',
    borderWidth: 0
  },
  blockedView: {
    backgroundColor: '#333',
    opacity: 0.8,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  pseudoHeader: {
    height: 20,
  },
  generalError: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 35,
    paddingTop: 40,
  },
  errorText: {
    fontWeight: 'bold',
    color: '#bbb',
  },

  modalBack: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatarFullview: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  loader: {
    paddingBottom: 60,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }

};

function mapStateToProps(state) {
  return {
    current_users_id: state.settings.user_id
  };
}

export default connect(mapStateToProps, { detectCountry })(ProfilePage);
