import React, { Component } from 'react';
import {
    Platform, Keyboard, StatusBar, Dimensions, View, TextInput,
    ScrollView, InteractionManager, KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';

import { ErrorCard, CommentItem } from '../base';
import {
    //errors empty resutls
    NO_COMMENTS,
    //events 
    COMMENT_SUBMITTED, SUCCESS, FAIL,
    TAB_OR_VIEW_CHANGED,
    HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT,
    PROFILE
} from '../../types';

import { Header, Properties, Spinner, CenterPage, Button } from '../../common';

import {
    FeedService,
    Utilities,
    Config,
    DatabaseService,
    AvatarService,
    PostService,
    UserService,
    EventEmitter
} from '../../SERVICES';


class CommentsPage extends Component {

    propTypes: {
        totalComments: React.PropTypes.number.isRequired,
        parentId: React.PropTypes.number.isRequired
    }

    state = {
        total_comments: this.props.totalComments,
        comments: [],
        error: null,
        text: null,
        submit_disabled: true,
        focused: false
    }

    constructor(props) {
        super(props);
        const config = new Config();
        this.utilities = new Utilities();
        const dbService = new DatabaseService(this.utilities);
        this.avatarService = new AvatarService(config, this.utilities, dbService);
        this.feedService = new FeedService(config, this.utilities, null, null, dbService);
        this.postService = new PostService(config, this.utilities, null, null);
        this.userService = new UserService(config, this.utilities, dbService);

        this.scroll_ref = null;
    }

    componentWillMount() {
        if (Platform.OS === 'ios') {
            StatusBar.setBarStyle('dark-content');
        }
        InteractionManager.runAfterInteractions(() => {
            if (this.state.total_comments > 0) {
                //fetch comments from server
                this.feedService.fetchComments(this.props.parentId)
                    .then(data => {
                        this.userService.updateFeedWithContactNames(data).then(updated_data => {
                            this.setState({ comments: updated_data });
                        });
                    })
                    .catch(err => { this.setState({ error: err }); });
            }
            //STOP all playing videos if anywhere
            //this is necessary cos you can move from the feed page to the comment page
            //in that case if a video was playing, we want it to stop
            EventEmitter.dispatch(TAB_OR_VIEW_CHANGED);
        });
    }
    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            //if we came here from the profile page, set status bar to white
            if (this.props.fromPage && this.props.fromPage === PROFILE) {
                StatusBar.setBarStyle('light-content');
            }
        }
    }

    submitComment() {
        if (this.state.submit_disabled)
            return;

        Keyboard.dismiss();
        const new_comment_txt = this.state.text.trim(); //temp storage for comment text
        const new_comment_id = Math.random();
        const new_comment_obj = {
            comment_id: new_comment_id,
            username: 'You',
            how_long_ago: 'just now',
            poster_id: this.props.user_id,
            avatar_url: this.props.avatar_url,
            comment: new_comment_txt,
            new_comment: true
        };
        this.setState({
            comments: this.state.comments.concat([new_comment_obj]),
            total_comments: this.state.total_comments + 1,
            text: '',
            submit_disabled: true
        });

        // submit comment to server
        this.postService.submitComment(this.props.user_id, this.props.parentId, new_comment_txt)
            .then(() => EventEmitter.dispatch(COMMENT_SUBMITTED, {
                parent_id: this.props.parentId,
                new_comment_id,
                submit_state: SUCCESS
            }))
            .catch(() => EventEmitter.dispatch(COMMENT_SUBMITTED, {
                parent_id: this.props.parentId,
                new_comment_id,
                submit_state: FAIL
            }));
    }


    onType(text) {
        if (text.trim().length > 2) {
            this.setState({ text, submit_disabled: false });
        }
        else {
            this.setState({ text, submit_disabled: true });
        }
    }


    renderComments() {
        if (this.state.total_comments === 0)
            return <ErrorCard errorState={NO_COMMENTS} />;
        else if (this.state.error)
            return <ErrorCard errorState={this.state.error} />;
        else if (this.state.total_comments > 0 && this.state.comments.length === 0)
            return <CenterPage><Spinner /></CenterPage>;

        const renderingObj = [];
        for (let i = 0; i < this.state.comments.length; i++) {
            const data = this.state.comments[i];
            renderingObj.push(<CommentItem key={data.comment_id} data={data} />);
        }
        return (
            <ScrollView
                ref={(scrollView) => { this.scroll_ref = scrollView; }}
                contentContainerStyle={styles.scrollContainer}
                onContentSizeChange={(contentWidth, contentHeight) => {
                    if (this.state.focused && contentHeight > Dimensions.get('window').height) {
                        if (Platform.OS === 'ios')
                            this.scroll_ref.scrollTo({ y: contentHeight - Dimensions.get('window').height + 100 });
                        else 
                            this.scroll_ref.scrollTo({ y: contentHeight });
                    }
                }}
            >
                {renderingObj}
            </ScrollView>
        );
    }

    _renderInput() {
        if (Platform.OS === 'android') {
            return (
                <KeyboardAvoidingView behavior={'padding'} style={styles.commentBox}>
                    <TextInput
                        multiline
                        maxLength={140}
                        numberOfLines={2}
                        placeholder={'Say something interesting...'}
                        placeholderTextColor={'#BBB'}
                        underlineColorAndroid={'transparent'}
                        onChangeText={this.onType.bind(this)}
                        onFocus={() => this.setState({ focused: true })}
                        onBlur={() => this.setState({ focused: false })}
                        value={this.state.text}
                        style={[styles.textInput]}
                    />
                    <Button
                        styles={styles.submitButton}
                        disabled={this.state.submit_disabled}
                        tapped={this.submitComment.bind(this)}
                        iconOnly
                        textColor={Properties.themeColor}
                        disabledTextColor={'#999'}
                        icon={'md-send'}
                        iconSize={35}
                    />
                </KeyboardAvoidingView >
            );
        }
        return (
            <KeyboardAvoidingView behavior={'padding'} style={styles.commentBox}>
                <TextInput
                    multiline
                    maxLength={140}
                    numberOfLines={2}
                    placeholder={'Say something interesting...'}
                    placeholderTextColor={'#BBB'}
                    underlineColorAndroid={'transparent'}
                    onChangeText={this.onType.bind(this)}
                    onFocus={() => this.setState({ focused: true })}
                    onBlur={() => this.setState({ focused: false })}
                    value={this.state.text}
                    style={[styles.textInput]}
                />
                <Button
                    styles={styles.submitButton}
                    disabled={this.state.submit_disabled}
                    tapped={this.submitComment.bind(this)}
                    iconOnly
                    textColor={Properties.themeColor}
                    disabledTextColor={'#999'}
                    icon={'ios-send'}
                    iconSize={35}
                />
            </KeyboardAvoidingView >
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>{this.renderComments()}</View>
                {this._renderInput()}
                <Header title={'Comments'} showBackButton transparent />
            </View>
        );
    }
}


const styles = {
    textInput: {
        flex: 1,
        marginRight: 10,
        fontSize: 15,
        paddingLeft: 6,
        paddingRight: 6,
        paddingBottom: 6,
        paddingTop: 6,
        textAlignVertical: 'top',
    },
    submitButton: {
        paddingLeft: 0,
        paddingRight: 5,
        backgroundColor: 'transparent'
    },
    commentBox: {
        backgroundColor: '#eee',
        flexDirection: 'row',
        padding: 0,
        margin: 0
    },
    scrollContainer: {
        paddingTop: Platform.OS === 'android' ? HEADER_HEIGHT + 10 : HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT + 10
    }
};



function mapStateToProps(state) {
    return {
        avatar_url: state.settings.user_avatar,
        user_id: state.settings.user_id
    };
}

export default connect(mapStateToProps, {})(CommentsPage);
