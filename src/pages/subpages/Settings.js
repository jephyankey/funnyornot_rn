import React, { Component } from 'react';
import {
    ScrollView, View, AsyncStorage, InteractionManager,
    Platform, ListView, Alert
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import {
    Properties, Header, SettingsSection, SettingItem, SettingCheckItem,
    ListModal, CenterPage, Spinner, MyText,
} from '../../common';

import { detectCountry, setAffiliateCountries, changeUsersCountry } from '../../actions';

import {
    DatabaseService, Utilities, ContactService, Config, UserService, PostService, EventEmitter
} from '../../SERVICES';
import AffiliateCountry from '../base/AffiliateCountry';
import countryList from '../../SERVICES/country_codes';
import { RadioItem, ContactItem } from '../base';

import {
    IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT, NEW_THEME_CHOSEN,
    PRIVATE_DP_SETTING, INVISIBILITY_SETTING, NOTIFICATION_SETTING,
    AFFILIATE_COUNTRIES
} from '../../types';



class SettingsPage extends Component {

    state = {
        private_dp_setting: false,
        inivisibility_setting: false,
        notification_setting: false,
        blocked_contacts: [],
        blocked_non_contacts: [],
        loading_c: true,
        loading_n_c: true,
        which_list: null,
        error_c: false,
        error_n_c: false,
        blk_list_modal_visible: false,
        affiliate_countries_modal_visible: false,
        change_country_modal_visible: false,

        render_everything_on_page: Platform.OS !== 'android'
    };

    constructor(props) {
        super(props);
        this.config = new Config();
        this.utilities = new Utilities();
        this.dbService = new DatabaseService(this.utilities);
        this.contactService = new ContactService(this.config, this.utilities, this.dbService);
        this.userService = new UserService(this.config, this.utilities, this.dbService);
        this.postService = new PostService(this.config, this.utilities, this.userService, null);
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            //load existing settings        
            AsyncStorage.multiGet([PRIVATE_DP_SETTING, INVISIBILITY_SETTING, NOTIFICATION_SETTING])
                .then(response => {
                    this.setState({
                        private_dp_setting: response[0][1],
                        inivisibility_setting: response[1][1],
                        notification_setting: response[2][1]
                    });
                });


            //get blocked contacts
            this.contactService.getBlockedContacts()
                .then(list => {
                    this.setState({ blocked_contacts: list, loading_c: false });
                })
                .catch(() => this.setState({ loading_c: false, error: true }));
            //get non-contacts blocked
            this.contactService.getBlockedNonContacts()
                .then(list => {
                    this.setState({ blocked_non_contacts: list, loading_n_c: false });
                })
                .catch(() => this.setState({ loading_n_c: false, error: true }));

            //-------
            this.ds = new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            });
            this.dataSource = this.ds.cloneWithRows(countryList);

            //-----
            this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
                this.forceUpdate();
            });

            //---
            this.setState({ render_everything_on_page: true });
        });
    }


    componentWillUnmount() {
        //unsubscribe from theme update
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
    }


    //---------------------------------------------------------------------------------------------------------------------------------
    goToPage(page) {
        switch (page) {
            case 'username':
                Actions.usernameSettingsPage();
                break;
            case 'help':
                Actions.webPage({ webSource: 'HELP', title: 'Help' });
                break;
            case 'feedback':
                Actions.feedbackPage();
                break;
            case 'privacy_policy':
                Actions.webPage({ webSource: 'PRIVACY', title: 'Privacy Policy' });
                break;
            case 'terms_of_use':
                Actions.webPage({ webSource: 'TERMS', title: 'Terms of Use' });
                break;
            case 'themes':
                Actions.themesPage();
                break;
            default:
                return;
        }
    }


    //-----------------------------------------------PRIVACY AND NOTIFICATION SETTINGS-------------------------------------------------
    showBlockedContacts() {
        this.setState({ blk_list_modal_visible: true, which_list: 'contacts', modal_title: 'Blocked Contacts' });
    }

    showBlockedNonContacts() {
        this.setState({ blk_list_modal_visible: true, which_list: 'non-contacts', modal_title: 'Blocked People' });
    }

    unblock(contact, mode = 'contacts') {
        const name = contact.name || contact.user_name;
        const list1 = this.state.blocked_contacts;
        const list2 = this.state.blocked_non_contacts;


        this.utilities.showToast(`unblocking ${name} ...`, 5000);
        this.userService.unblock(contact.user_id)
            .then(() => {
                this.utilities.showToast(`${name} has been unblocked`);
                //search for ublocked user and remove him from array
                if (mode === 'contacts')
                    for (let i = 0; i < list1.length; i++) {
                        if (list1[i].user_id === contact.user_id) {
                            //....................TODO......................
                            //this might (?) change state without going through setState(), if so modify
                            list1.splice(i, 1);
                            // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                            this.setState({ blocked_contacts: list1 });
                            break;
                        }
                    }
                else if (mode === 'others')
                    for (let i = 0; i < list2.length; i++) {
                        if (list2[i].user_id === contact.user_id) {
                            list2.splice(i, 1);
                            // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
                            this.setState({ blocked_non_contacts: list2 });
                            break;
                        }
                    }
            })
            .catch(() => this.utilities.showToast(`Could not unblock ${name}`));
    }

    renderBlockedList() {
        const renderingObj = [];
        const list1 = this.state.blocked_contacts;
        const list2 = this.state.blocked_non_contacts;

        if (this.state.error_c || this.state.error_n_c) {
            this.utilities.showToast('An error has occurred!');
            return;
        }

        if (this.state.which_list === 'contacts') {
            if (this.state.loading_c) {
                return <CenterPage><Spinner color={'#ccc'} size={35} /></CenterPage>;
            }
            else if (list1.length === 0) {
                return (
                    <CenterPage>
                        <MyText styles={styles.noOne}>You have not blocked any contact. Nice going!</MyText>
                    </CenterPage>
                );
            }
            //last case render elements
            for (let i = 0; i < list1.length; i++) {
                const d = list1[i];
                renderingObj.push(
                    <ContactItem
                        key={i}
                        mainText={d.name}
                        subText={d.phone}
                        onTap={this.unblock.bind(this, d, 'contacts')}
                        btnText={'UNBLOCK'}
                    />
                );
            }
            return <ScrollView style={{ flex: 1 }}>{renderingObj}</ScrollView>;
        }

        else if (this.state.which_list === 'non-contacts') {
            if (this.state.loading_c) {
                return <CenterPage><Spinner color={'#ccc'} size={'large'} /></CenterPage>;
            }
            else if (list2.length === 0) {
                return (
                    <CenterPage>
                        <MyText styles={styles.noOne}>You have not blocked anyone. Good for you!</MyText>
                    </CenterPage>
                );
            }
            //last case render elements
            for (let i = 0; i < list2.length; i++) {
                const d = list2[i];
                renderingObj.push(
                    <ContactItem
                        key={i}
                        mainText={d.user_name}
                        onTap={this.unblock.bind(this, d, 'others')}
                        btnText={'UNBLOCK'}
                    />
                );
            }
            return <ScrollView style={{ flex: 1 }}>{renderingObj}</ScrollView>;
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------------



    //--------------------------------------------------------CONTETNT SETTINGS--------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------
    showAffiliatesPopup() {
        //go through the list of affiliate countries and tick those part of my countries
        const country_list = JSON.parse(JSON.stringify(countryList));

        for (let i = 0; i < this.props.affiliates.length; i++) {
            for (let j = 0; j < country_list.length; j++) {
                if (country_list[j].country_code === this.props.affiliates[i]) {
                    country_list[j].selected = '1';
                    break;
                }
            }
        }
        this.dataSource = this.ds.cloneWithRows(country_list);
        this.setState({ affiliate_countries_modal_visible: true });
    }

    updateAffiliatesSetting() {
        const list = this.props.affiliates.join();
        AsyncStorage.setItem(AFFILIATE_COUNTRIES, list);
        this.setState({ affiliate_countries_modal_visible: false });
    }

    resetAllBlocked() {
        //ask for confirmation first,
        Alert.alert(
            'Careful!',
            'You are about to unblock every post you have ever blocked since you were born.',
            [
                { text: Platform.OS === 'android' ? 'CANCEL' : 'Cancel', onPress: () => { }, style: 'cancel' },
                {
                    text: Platform.OS === 'android' ? 'GO AHEAD' : 'Go ahead',
                    onPress: () => {
                        this.utilities.showToast('Unblocking...');
                        this.postService.resetAllBlockedPosts()
                            .then(() => this.utilities.showToast('Blocked posts resetted'))
                            .catch(err => {
                                this.utilities.showToast('Reset Failed!');
                                this.utilities.handleErrors(err);
                            });
                    }
                }
            ]
        );
    }

    renderRow(data, sectionId, rowId) {
        return (
            <AffiliateCountry data={data} key={rowId} />
        );
    }
    //---------------------------------------------------------------------------------------------------------------------------------



    //--------------------------------------------------------SUPPORT SETTINGS--------------------------------------------------------
    goToSite() {
        this.utilities.showToast("Hehehe! Just kidding. We don't have one.", 2000);
    }
    //---------------------------------------------------------------------------------------------------------------------------------



    //--------------------------------------------------------ACCOUNT SETTINGS--------------------------------------------------------\
    showCountryListToSelect() {
        this.setState({ change_country_modal_visible: true });
    }

    renderRowChangeCountry(data) {
        return (
            <RadioItem
                isChecked={this.props.user_country.toString() === data.country_code}
                highlight={this.props.user_country.toString() === data.country_code}
                checkedColor={Properties.themeColor}
                onTap={this.updateUsersCountry.bind(this, data.country_code)}
                text={data.country}
                key={data.country_code}
            />
        );
    }
    /**
     * when a user selects a particular country in the list, update the users country 
     * in the redux settings store
     * then take that and set in the database and also set on the server side
     */
    updateUsersCountry(country_code) {
        const previous_country = this.props.user_country;
        this.props.changeUsersCountry(country_code); /* update redux store, will also cause new country to be shown in UI */
        this.setState({ change_country_modal_visible: false });
        //update database locally and server side
        this.userService.updateUsersCountry(country_code, this.props.user_id)
            .catch(() => {
                //if update fails revert UI and redux change
                this.props.changeUsersCountry(previous_country);
                this.utilities.showToast('Could not update country', 3000, 'ios-sad-outline');
            });
    }

    closeUserCountryList() {
        this.setState({ change_country_modal_visible: false });
    }

    //---------------------------------------------------------------------------------------------------------------------------------


    renderModals() {
        if (this.state.render_everything_on_page) {
            return (
                <View>
                    <ListModal
                        modalVisible={this.state.blk_list_modal_visible}
                        title={this.state.modal_title}
                        pressClose={() => this.setState({ blk_list_modal_visible: false })}
                        pressOkay={() => this.setState({ blk_list_modal_visible: false })}
                    >
                        {this.renderBlockedList()}
                    </ListModal>

                    <ListModal
                        modalVisible={this.state.affiliate_countries_modal_visible}
                        title={'Explore content from...'}
                        pressClose={this.updateAffiliatesSetting.bind(this)}
                        pressOkay={this.updateAffiliatesSetting.bind(this)}
                    >
                        <ListView
                            dataSource={this.dataSource}
                            renderRow={this.renderRow.bind(this)}
                            pageSize={100}
                            scrollRenderAheadDistance={1800}
                            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                        />
                    </ListModal>

                    <ListModal
                        modalVisible={this.state.change_country_modal_visible}
                        title={'Select your country'}
                        pressClose={this.closeUserCountryList.bind(this)}
                        pressOkay={this.closeUserCountryList.bind(this)}
                    >
                        <ListView
                            dataSource={this.dataSource}
                            renderRow={this.renderRowChangeCountry.bind(this)}
                            pageSize={100}
                            scrollRenderAheadDistance={1800}
                        />
                    </ListModal>
                </View>
            );
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ScrollView style={styles.fullview}>

                    <SettingsSection title={'Account'} icon={'ios-contact-outline'}>
                        <SettingItem
                            text={'Change my username'}
                            pressed={this.goToPage.bind(this, 'username')}
                            hasSubPage
                        />
                        <SettingItem
                            text={'Change my country'}
                            pressed={this.showCountryListToSelect.bind(this)}
                            pseudoSubText={`(${this.props.user_country})`}
                            noBorder
                        />
                    </SettingsSection>

                    <SettingsSection title={'Notifications'} icon={'ios-notifications-outline'}>
                        <SettingCheckItem
                            text={'Turn Off Notifications'}
                            isChecked={this.state.notification_setting}
                            setting={NOTIFICATION_SETTING}
                            serverRoute={NOTIFICATION_SETTING}
                            noBorder
                        />
                    </SettingsSection>

                    <SettingsSection title={'Privacy'} icon={'ios-finger-print-outline'}>
                        <SettingItem
                            text={'Blocked contacts'}
                            pressed={this.showBlockedContacts.bind(this)}
                        />
                        <SettingItem
                            text={'Other blocked people'}
                            subText={'People you have blocked who are not in your contacts'}
                            pressed={this.showBlockedNonContacts.bind(this)}
                        />
                        <SettingCheckItem
                            text={'Private Profile Picture'}
                            subText={'Only your contacts will see your profile picture'}
                            isChecked={this.state.private_dp_setting}
                            setting={PRIVATE_DP_SETTING}
                            serverRoute={PRIVATE_DP_SETTING}
                        />
                        <SettingCheckItem
                            text={'Make me invisible'}
                            subText={'Your friends will not be able to see you'}
                            isChecked={this.state.inivisibility_setting}
                            setting={INVISIBILITY_SETTING}
                            serverRoute={INVISIBILITY_SETTING}
                            noBorder
                        />
                    </SettingsSection>

                    {this.state.render_everything_on_page ?
                        /**delay rendering all content for a better animated transition */
                        <View>
                            <SettingsSection title={'Content'} icon={'ios-cart-outline'}>
                                <SettingItem
                                    text={'Affiliate Countries'}
                                    subText={'Select the countries from which you explore'}
                                    pressed={this.showAffiliatesPopup.bind(this)}
                                />
                                <SettingItem
                                    text={'Reset Blocked Content'}
                                    pressed={this.resetAllBlocked.bind(this)}
                                    noBorder
                                />
                            </SettingsSection>

                            <SettingsSection title={'Themes'} icon={'ios-brush-outline'}>
                                <SettingItem
                                    text={'Change theme'}
                                    pressed={this.goToPage.bind(this, 'themes')}
                                    hasSubPage
                                    noBorder
                                />
                            </SettingsSection>

                            <SettingsSection title={'Support'} icon={'ios-help-circle-outline'}>
                                <SettingItem
                                    text={'Help'}
                                    pressed={this.goToPage.bind(this, 'help')}
                                    hasSubPage
                                />
                                <SettingItem
                                    text={'Feedback'}
                                    pressed={this.goToPage.bind(this, 'feedback')}
                                    hasSubPage
                                />
                                <SettingItem
                                    text={'Go to our website'}
                                    pressed={this.goToSite.bind(this)}
                                    noBorder
                                />
                            </SettingsSection>


                            <SettingsSection title={'Legal'} icon={'ios-document-outline'}>
                                <SettingItem
                                    text={'Privacy policy'}
                                    pressed={this.goToPage.bind(this, 'privacy_policy')}
                                    hasSubPage
                                />
                                <SettingItem
                                    text={'Terms of use'}
                                    pressed={this.goToPage.bind(this, 'terms_of_use')}
                                    hasSubPage
                                    noBorder
                                />
                            </SettingsSection>

                            <View style={styles.copyright}>
                                <MyText styles={styles.copyrightText}>Copyright  &#169; 2017 Asqii, LLC</MyText>
                                <MyText styles={styles.copyrightText}>All  rights reserved.</MyText>
                            </View>
                        </View>
                        : null}

                </ScrollView>
                <Header title={'Settings'} showBackButton transparent />

                {this.renderModals()}

            </View>
        );
    }
}


const styles = {
    fullview: {
        paddingHorizontal: 20,
        paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT + HEADER_HEIGHT + 25 : HEADER_HEIGHT + 25
    },
    noOne: {
        fontWeight: 'bold',
        color: '#aaa'
    },
    copyright: {
        alignItems: 'center',
        height: Platform.OS === 'android' ? 150 : null
    },
    copyrightText: {
        color: '#999',
    },
};


function mapStateToProps(state: CombinedStore) {
    return {
        affiliates: state.settings.affiliate_countries,
        user_country: state.settings.user_country,
        user_id: state.settings.user_id
    };
}

export default connect(mapStateToProps, { setAffiliateCountries, detectCountry, changeUsersCountry })(SettingsPage);
