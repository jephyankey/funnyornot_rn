import React, { Component } from 'react';
import People from '../base/People';


class FollowersPage extends Component {

    render() {
        return (
            <People pageMode={'followers'} title={'Followers'} fromPage={this.props.fromPage}/>
        );
    }

}


export default FollowersPage;
