import React, { Component } from 'react';
import {
    View, Platform, UIManager,
    InteractionManager
} from 'react-native';
import Feed from '../base/Feed';
import { ALLTIME } from '../../types';

import { Header, Properties, Spinner, CenterPage2 } from '../../common';

class AlltimePage extends Component {

    state = {
        show_content: false
    }

    constructor(props) {
        super(props);
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentWillMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({ show_content: true });
        });
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Properties.greyBackColor }}>
                {
                    this.state.show_content ?
                        <Feed pageMode={ALLTIME} />
                        :
                        <CenterPage2><Spinner /></CenterPage2>
                }
                <Header title={'Jokes of fame'} showBackButton transparent />
            </View>
        );
    }
}



export default AlltimePage;
