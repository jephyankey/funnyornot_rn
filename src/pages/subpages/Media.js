import React, { Component } from 'react';
import {
    Dimensions, View, ListView, Platform
} from 'react-native';

import {
    NO_INTERNET, NO_INTERNET2, EMPTY_RESULT, IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT
} from '../../types';

import { ErrorCard, SwipeMedia } from '../base';

import { Header, CenterPage2, Spinner, GalleryItem, Properties } from '../../common';
import {
    UserService, Config, Utilities,
    DatabaseService, ContactService, FeedService
} from '../../SERVICES';


class MediaPage extends Component {

    page: number = 1;

    propTypes: {
        type: React.PropTypes.string.isRequired
    }

    constructor(props) {
        super(props);
        this.utilities = new Utilities();
        this.config = new Config();
        this.utilities = new Utilities();
        this.dbService = new DatabaseService(this.utilities);
        this.contactService = new ContactService(this.config, this.utilities, this.dbService);
        this.userService = new UserService(this.config, this.utilities, this.dbService);
        this.feedService = new FeedService(this.config, this.utilities, this.userService,
            this.contactService, this.dbService);
    }


    state = {
        loading: true,
        err_state: {},
        error: false,
        data: [],
        show_more_loader: true,
        should_fetch_more: true,
        modal_visible: false,
        initial_slide_id: 0
    };


    componentWillMount() {
        if (this.props.which === 'images') {
            this.title = 'Images from your feed';
            this.feed_keyword = 'image-feed';
        }
        else if (this.props.which === 'videos') {
            this.title = 'Videos from your feed';
            this.feed_keyword = 'video-feed';
        }
    }


    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        this.setState({ should_fetch_more: false }); //prevent attempts to fetch more until this fetch is done

        this.feedService.fetchFeed(this.page, this.feed_keyword)
            .then(data => {
                if (data.length === 0)
                    throw EMPTY_RESULT;
                else {
                    this.page++;
                    this.setState({ data: this.state.data.concat(data), loading: false });

                    //is there more stuff to show???
                    if (data.length < this.feedService.getMediaPageSize())
                        this.setState({ should_fetch_more: false, show_more_loader: false });
                    else
                        this.setState({ should_fetch_more: true });
                }
            })
            .catch(err => {
                if (err === NO_INTERNET)
                    err = NO_INTERNET2;
                this.utilities.handleErrors(err);
                //rerender the page and move to the correct error view (of course only if we are on page 1)
                if (this.page === 1) {
                    this.setState({ error: true, err_state: err });
                }
                else {
                    /**
                     * we do not say we are done fetching more (cos we don't know that'), lets say 
                     * the internet just went off momentarily, so we turn off showing the loadmore loader,
                     * however, if the user say scrolls up and back down, we still want to attempt to fetch more
                     * just in case the original error (say no internet) has been resolved
                     * this is why we are not setting should_fetch_more to false here
                     */
                    this.setState({ show_more_loader: false });
                }
            });
    }


    loadMore() {
        if (this.state.should_fetch_more) {
            this.fetchData();
        }
    }

    showSwipable(id) {
        this.setState({ modal_visible: true, initial_slide_id: id });
    }

    closeSwipable() {
        this.setState({ modal_visible: false });
    }


    renderDataSource() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(this.state.data);
    }


    renderFooter() {
        if (this.state.show_more_loader)
            return <View style={styles.footer}><Spinner size={Platform.OS === 'android' ? 35 : 'small'} /></View>;
    }

    renderRow(data) {
        const source = data.poster_image || data.media_url;
        return (
            <GalleryItem
                key={data.post_id}
                imageSource={this.utilities.httpsFromHttp(source)}
                mediaType={data.media_type}
                onTap={this.showSwipable.bind(this, data.post_id)}
                mediaType={data.media_type}
                />
        );
    }

    renderGallery() {
        if (this.state.loading) {
            return <CenterPage2><Spinner /></CenterPage2>;
        }
        this.renderDataSource();
        return (
            <ListView
                style={{ flex: 1, marginBottom: 15 }}
                initialListSize={24} //note value SHOULD MATCH this.feedService.getMediaPageSize()
                pageSize={6}
                dataSource={this.dataSource}
                renderRow={this.renderRow.bind(this)}
                renderFooter={this.renderFooter.bind(this)}
                onEndReached={this.loadMore.bind(this)}
                onEndReachedThreshold={20}
                contentContainerStyle={styles.listGallery}
                />
        );
    }

    renderError() {
        return (
            <CenterPage2>
                <ErrorCard errorState={this.state.err_state} />
            </CenterPage2>
        );
    }


    render() {
        return (
            <View style={styles.page}>
                {this.state.error ? this.renderError() : this.renderGallery()}
                <SwipeMedia
                    modalVisible={this.state.modal_visible}
                    data={this.state.data}
                    initialSlideId={this.state.initial_slide_id}
                    onClose={this.closeSwipable.bind(this)}
                    />
                <Header title={this.title} showBackButton transparent />
            </View>
        );
    }
}

const window = Dimensions.get('window').width;

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#fff'
    },
    listGallery: {
        flexDirection: 'row',
        backgroundColor: Properties.greyBackColor,
        paddingLeft: 0.01 * window,
        //make room for transparent header
        paddingTop: Platform.OS === 'android' ? HEADER_HEIGHT : HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT, 
        flexWrap: 'wrap',
        marginBottom: 30,
        alignItems: 'flex-start'
    },
    footer: {
        width: (0.96 * window) / 3,
        marginTop: 20,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },

};


export default MediaPage;
