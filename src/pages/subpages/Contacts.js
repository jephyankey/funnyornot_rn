import React, { Component } from 'react';
import People from '../base/People';

class ContactsPage extends Component {


    render() {
        return (
            <People pageMode={'contacts'} title={'Contacts'} />
        );
    }

}


export default ContactsPage;
