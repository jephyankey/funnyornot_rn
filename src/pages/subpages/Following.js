import React, { Component } from 'react';
import People from '../base/People';

class FollowingPage extends Component {


    render() {
        return (
            <People pageMode={'following'} title={'Following'} fromPage={this.props.fromPage} />
        );
    }

}


export default FollowingPage;
