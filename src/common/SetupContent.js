//the main content during the initial setup
import React from 'react';
import { View, Platform } from 'react-native';
import { IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT } from '../types';


//a regular page with a beaddr that centers all content
export const SetupContent = (props) => (
    <View style={styles.content}>
        {props.children}
    </View>
);

const styles = {
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
    position: 'relative',
    paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT + HEADER_HEIGHT + 30 : 30
  }
};
