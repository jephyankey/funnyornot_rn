//imports
import React from 'react';
import { View, Text, TouchableOpacity, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Properties } from './Properties';

export const MenuItem = (props) => (
    <TouchableOpacity style={styles.item} onPress={props.onTap}>
        <View style={styles.iconView}>
            <Icon
                name={props.icon}
                color={props.iconColor || Properties.themeColor}
                size={props.iconSize}
                />
        </View>
        <View>
            <Text style={styles.text}>{props.text}</Text>
        </View>
    </TouchableOpacity>
);


MenuItem.defaultProps = {
    iconColor: null,
    iconSize: Platform.OS === 'android' ? 33 : 30,
};


const styles = {
    item: {
        flexDirection: 'row',
        marginBottom: 18,
        alignItems: 'center'
    },
    iconView: {
        marginRight: 25
    },
    text: {
        color: '#888',
        fontSize: 16
    }
};
