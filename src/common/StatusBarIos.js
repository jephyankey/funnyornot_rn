import React from 'react';
import { View, StatusBar } from 'react-native';
import { IOS_STATUS_BAR_HEIGHT } from '../types';
import { Properties } from './Properties';

export const StatusBarIos = ({ useThemeForStatusBar }) => (
    <View style={{ ...styles.iosStatusBar, backgroundColor: useThemeForStatusBar ? Properties.themeColor : 'rgba(255,255,255,0.9)' }}>
        <StatusBar barStyle={useThemeForStatusBar ? 'light-content' : 'dark-content'} />
    </View>
);

const styles = {
    iosStatusBar: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: IOS_STATUS_BAR_HEIGHT,
    }
};


