//imports
import React from 'react';
import { View, Text } from 'react-native';
import { Properties } from './Properties';
import Icon from 'react-native-vector-icons/Ionicons';


export const SettingsSection = (props) => (
    <View style={styles.overall}>
        <View style={{ ...styles.underline, borderBottomColor: Properties.themeColor }}>
            <Icon name={props.icon} size={18} color={Properties.themeColor} style={{ marginRight: 10 }} />
            <Text style={{ ...styles.text, color: Properties.themeColor }}>{props.title}</Text>
        </View>
        <View style={styles.settingItem}>{props.children}</View>
    </View>
);


const styles = {
    overall: {
        marginBottom: 40,
    },
    underline: {
        borderBottomWidth: 0.8,
        paddingLeft: 20,
        paddingBottom: 5,
        flexDirection: 'row'
    },
    text: {
        fontSize: 15
    },
    settingItem: {
        paddingHorizontal: 15,
        paddingTop: 10
    }
};
