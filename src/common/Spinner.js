import React from 'react';
import { View, ActivityIndicator, Platform } from 'react-native';
import { Properties } from './Properties';

const spinner_size = Platform.OS === 'android' ? 50 : 'large';

export const Spinner = ({ color = Properties.themeColor, size = spinner_size }) => (
  <View style={styles.spinner}>
    <ActivityIndicator color={color} size={size} />
  </View>
);


const styles = {
  spinner: {
    justifyContent: 'center',
    alignItems: 'center',
  }
};
