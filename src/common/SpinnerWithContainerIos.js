import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { Properties } from './Properties';

const spinner_size = 'large';

export const SpinnerWithContainerIos = ({ color = Properties.themeColor, size = spinner_size }) => (
    <View style={styles.spinner}>
        <ActivityIndicator color={color} size={size} />
    </View>
);


const styles = {
    spinner: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: 'rgba(255,255,255,0.8)',
        padding: 10,
        paddingTop: 13,
        paddingLeft: 12
    }
};
