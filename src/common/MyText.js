//imports
import React from 'react';
import { Text, Platform } from 'react-native';


//a regular page with a header that centers all content
export const MyText = (props) => (
    <Text style={{ ...styles.text, ...props.styles }}>
        {props.children}
    </Text>
);

const styles = {
    text: {
        fontFamily: Platform.OS === 'android' ? 'Roboto' : null,
        lineHeight: 27
    }
};
