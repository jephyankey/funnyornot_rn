//imports
import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { MyText } from '../common';



export class SettingItem extends Component {
  
    //pass textColor for icons color
    propTypes: {
        text: React.PropTypes.string.isRequired,
        subText: React.PropTypes.string,
        noBorder: React.PropTypes.boolean, // whether a bottom border should be given or not
        pressed: React.PropTypes.func,
        firstItem: React.PropTypes.boolean,
        hasSubPage: React.PropTypes.boolean
    }


    noBorder() {
        if (this.props.noBorder) {
            return { borderBottomColor: '#fff' };
        }
    }

    firstItem() {
        /**
         * if this is the first item in the setting list, add 
         * a little more padding to the top
         */
        if (this.props.firstItem) {
            return { paddingTop: 5 };
        }
    }

    render() {
        const { item, text, subText, pseudoSubText } = styles;

        return (
            <TouchableOpacity
                style={{ ...item, ...this.noBorder(), ...this.firstItem() }}
                onPress={this.props.pressed}
            >
                <View style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <MyText styles={text}>{this.props.text}</MyText>
                        {this.props.pseudoSubText ? <MyText styles={pseudoSubText}>{this.props.pseudoSubText}</MyText> : null } 
                    </View>
                    {
                        this.props.subText ?
                            <MyText styles={subText}>{this.props.subText}</MyText> :
                            null
                    }
                </View>
                {
                    this.props.hasSubPage ?
                    <Icon name={'ios-arrow-forward'} size={20} color={'#bbb'} style={styles.chevronRight} />
                    : null
                }
            </TouchableOpacity>
        );
    }
}


SettingItem.defaultProps = {
    text: 'Provide Text',
    subText: null
};



const styles = {
    item: {
        paddingVertical: 12,
        borderBottomWidth: 0.5,
        borderBottomColor: '#eee',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    text: {
        color: '#555',
        fontSize: 15
    },
    subText: {
        color: '#aaa',
        fontSize: 14
    },
    pseudoSubText: {
        color: '#aaa',
        fontSize: 14,
        marginLeft: 25
    },
    chevronRight: {
        paddingLeft: 20,
     }
};
