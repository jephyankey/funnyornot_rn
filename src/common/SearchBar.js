//imports
import React from 'react';
import { View, Platform } from 'react-native';
import { Properties } from './Properties';
import Icon from 'react-native-vector-icons/Ionicons';

//a regular page with a header that centers all content
export const SearchBar = (props) => (
    <View style={{ ...styles.searchBar, ...props.styles }}>
        <View style={styles.searchView}>
            <View style={styles.searchIcon}>
                <Icon name={Platform.OS === 'android'? 'md-search': 'ios-search'} size={20} color={'#aaa'} />
            </View>
            {props.children}
        </View>
    </View>
);

const styles = {
    searchBar: {
        backgroundColor: Properties.greyBackColor,
        padding: 8
    },
    searchView: {
        backgroundColor: '#fff',
        borderRadius: 4,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingHorizontal: 10,
    },
    searchIcon: {
        marginRight: 10
    }
};
