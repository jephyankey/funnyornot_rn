//imports
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon8 from './Icon';


export const MenuItemCustomFont = (props) => (
    <TouchableOpacity onPress={props.pressed} style={styles.item}>
        <View style={styles.iconView}>
            <Icon8
                name={props.icon}
                color={props.iconColor}
                size={props.iconSize}
                />
        </View>
        <View>
            <Text style={styles.text}>{props.text}</Text>
        </View>
    </TouchableOpacity>
);


MenuItemCustomFont.defaultProps = {
    iconColor: '#666',
    iconSize: 33,
};


const styles = {
    item: {
        flexDirection: 'row',
        marginBottom: 18,
        alignItems: 'center'
    },
    iconView: {
        marginRight: 25
    },
    text: {
        color: '#888',
        fontSize: 16
    }
};
