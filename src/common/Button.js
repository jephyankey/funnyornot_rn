//imports
import React, { Component } from 'react';
import { View, TouchableOpacity, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon8 from './Icon';
import { Properties } from './Properties';
import { MyText } from './MyText';



export class Button extends Component {

    //pass textColor for icons color
    propTypes: {
        disabled: React.PropTypes.bool,
        text: React.PropTypes.string,
        textStyles: React.PropTypes.object,
        icon: React.PropTypes.string,
        tapped: React.PropTypes.func,
        iconOnly: React.PropTypes.bool,
        textColor: React.PropTypes.string,
        disabledTextColor: React.PropTypes.string,
        styles: React.PropTypes.object,
        iconSize: React.PropTypes.number,
        iconPack: React.PropTypes.string,
        userImage: React.PropTypes.object
    }

    disableBtn() {
        if (this.props.disabled === true) {
            return { opacity: 0.5 };
        }
    }

    disabledBtnText() {
        if (this.props.disabled)
             return { color: this.props.disabledTextColor };
        return { color: this.props.textColor };
    }

    renderIcon() {
        let Ikon = null;
        if (this.props.iconPack === 'funnyornot')
            Ikon = Icon8;
        else
            Ikon = Icon;

        if (this.props.icon) {
            return (
                <View style={styles.iconView}>
                    <Ikon
                        name={this.props.icon} 
                        size={this.props.iconSize}
                        color={this.props.disabled === true ? this.props.disabledTextColor : this.props.textColor}
                        />
                </View>
            );
        }
    }

    renderImageIcon() {
        return (
                <View style={styles.imageIconView}>
                    {this.props.useImage}
                </View>
            );
    }

    renderText() {
        if (!this.props.iconOnly) {
            return (
                <MyText
                    styles={{ 
                        ...styles.text,
                        ...this.props.textStyles,
                        ...{ fontWeight: this.props.weight }, 
                        ...this.disabledBtnText(),
                    }}
                    >
                    {this.props.text}
                </MyText>
            );
        }
    }


    render() {
        return (
            <TouchableOpacity onPress={this.props.tapped}>
                <View 
                    style={{ 
                        ...styles.view, 
                        backgroundColor: Platform.OS === 'android' ? Properties.themeColor : 'transparent',
                        borderColor: Platform.OS === 'ios' ? Properties.themeColor : null,
                        ...this.props.styles, 
                        ...this.disableBtn() 
                    }}>
                    {this.props.useImage ? this.props.useImage : this.renderIcon()}
                {this.renderText()}
                </View>
            </TouchableOpacity >
        );
    }
}


Button.defaultProps = {
    textColor: Platform.OS === 'android' ? '#fff' : Properties.themeColor,
    disabledTextColor: Platform.OS === 'android' ? '#fff' : Properties.themeColor,
    iconSize: 20,
    disabled: false,
    weight: '500'
};



const styles = {
    view: {
        flexDirection: 'row',
        borderRadius: 2,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 13,
        position: 'relative',
    },
    text: {
        fontSize: 15,
        lineHeight: 20,
    },
    iconView: {
        alignSelf: 'center',
        marginRight: 6,
        paddingTop: Platform.OS === 'ios' ? 4 : null
    },
    imageIconView: {
        marginRight: 6,
    }
};
