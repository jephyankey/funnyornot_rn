//imports
import React from 'react';
import { View, Dimensions, Image, TouchableWithoutFeedback } from 'react-native';
import Icon from './Icon';


//a regular page with a header that centers all content
export const GalleryItem = (props) => (
    <View style={styles.galleryItem}>
        <TouchableWithoutFeedback onPress={props.onTap}>
            <View>
                <Image
                    style={styles.image}
                    source={{ uri: props.imageSource }}
                    resizeMode="cover"
                    />
                {props.mediaType === 'v' ?
                    <View style={styles.videoIcon}>
                    <Icon name={'play-circle-outline'} size={15} color={'#fff'} />
                </View> : null}
            </View>
        </TouchableWithoutFeedback>
    </View>
);

const window = Dimensions.get('window').width;


const styles = {
    image: {
        width: (0.96 * window) / 3,
        height: (0.96 * window) / 3,
        backgroundColor: 'rgba(250,250,250,0.5)'
    },
    galleryItem: {
        width: (0.96 * window) / 3,
        height: (0.96 * window) / 3,
        marginRight: 0.01 * window,
        marginBottom: 0.01 * window,
    },
    videoIcon: {
        position: 'absolute',
        left: 5,
        top: 5,
        borderBottomRightRadius: 3,
        padding: 0,
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderRadius: 100
    }
};
