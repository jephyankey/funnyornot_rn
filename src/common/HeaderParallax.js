//imports
import React, { Component } from 'react';
import { Text, View, Platform, Animated, Easing, StatusBar } from 'react-native';
import { Properties } from './Properties';
import { StatusBarIos } from './StatusBarIos';

import { EventEmitter } from '../SERVICES';
import {
    NEW_THEME_CHOSEN, PARALLAX_SCROLL, HEADER_HEIGHT,
    DOWN_DIRECTON, UP_DIRECTION, IOS_STATUS_BAR_HEIGHT
} from '../types';


export class HeaderParallax extends Component {

    constructor(props) {
        super(props);
        this.slide_value = new Animated.Value(0);
        this.slide_out = Animated.timing(
            this.slide_value,
            {
                toValue: 1,
                duration: 200,
                easing: Easing.linear
            }
        );
        this.slide_in = Animated.timing(
            this.slide_value,
            {
                toValue: 0,
                duration: 200,
                easing: Easing.linear
            }
        );
        this.slide_vals =
            Platform.OS === 'android' ?
                this.slide_value.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, -1 * HEADER_HEIGHT]
                })
                :
                this.slide_value.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, -1 * (HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT)]
                })
            ;
    }

    componentDidMount() {
        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
            this.forceUpdate();
        });
        this.parallax_scroll_subscription = EventEmitter.subscribe(PARALLAX_SCROLL,
            ({ page_mode, direction }) => {
                //user has changed the scroll direction
                if (this.props.pageMode === page_mode) { //the page on which we are scrolling should be the same page that has the header
                    if (direction === UP_DIRECTION)
                        this.slide_out.start();
                    else if (direction === DOWN_DIRECTON)
                        this.slide_in.start();
                }
            });
    }

    slideIn() {

    }

    render() {
        if (Platform.OS === 'android') {
            return (
                <Animated.View style={[styles.overallTransparent, { transform: [{ translateY: this.slide_vals }] }]}>
                    <View
                        style={styles.header}>
                        <Text style={{ ...styles.headerText, color: Properties.themeColor }}>{this.props.title}</Text>
                    </View >
                </Animated.View >
            );
        }
        return (
            <View style={styles.overallIos}>
                <StatusBar barStyle={'default'} />
                <Animated.View style={[styles.overallTransparentIos, { transform: [{ translateY: this.slide_vals }] }]}>
                    <View
                        style={styles.headerIos}>
                        <Text style={{ ...styles.headerText, color: Properties.themeColor }}>{this.props.title}</Text>
                    </View>
                </Animated.View >
                <StatusBarIos color={Properties.themeColor} />
            </View>
        );
    }

    componentWillUnmount() {
        //unsubscribe from theme update
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
        EventEmitter.unsubscribe(PARALLAX_SCROLL, this.parallax_scroll_subscription);
    }
}


const styles = {
    overallTransparent: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: HEADER_HEIGHT,
        backgroundColor: 'rgba(255,255,255,0.80)',
        paddingLeft: 20
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        borderBottomWidth: 0,
        height: 50,
        justifyContent: 'center',
    },
    headerText: {
        fontSize: 20,
        fontWeight: '500',
    },
    //--------------------------ios------------------------------
    overallIos: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    overallTransparentIos: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        top: IOS_STATUS_BAR_HEIGHT,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(255,255,255,0.80)',
    },
    headerIos: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        borderBottomWidth: 0.5,
        borderColor: '#ccc',
        height: HEADER_HEIGHT,
    },
};
