//imports
import React from 'react';
import { Platform, View } from 'react-native';
import { IOS_STATUS_BAR_HEIGHT, HEADER_HEIGHT } from '../types'

//a regular page with a header that centers all content
export const CenterPage2 = (props) => (
    <View style={styles.page}>
        {props.children}
    </View>
);

const styles = {
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT + HEADER_HEIGHT : null
    }
};
