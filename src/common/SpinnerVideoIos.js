import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { Properties } from './Properties';

const spinner_size = 'large';

export const SpinnerVideoIos = ({ color = Properties.themeColor, size = spinner_size }) => (
    <View style={styles.spinner}>
        <ActivityIndicator color={color} size={size} />
    </View>
);


const styles = {
    spinner: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor: 'rgba(255,255,255,0.7)',
        padding: 5,
        paddingTop: 7,
        paddingLeft: 9
    }
};
