
import React from 'react';
import { View, Dimensions, Modal } from 'react-native';
import { Properties } from './Properties';
import { Button } from './Button';
import { MyText } from './MyText';


export const ListModal = (props) => (
    <Modal
        animationType={'slide'}
        transparent
        visible={props.modalVisible}
        onRequestClose={props.pressClose}
    >
        <View style={styles.modalBack}>
            <View style={{...styles.modal, height: props.modalHeight? props.modalHeight : 0.8 * height }}>
                <View style={styles.modalHeader}>
                    <MyText styles={{ ...styles.modalTitle, color: Properties.themeColor }}>{props.title}</MyText>
                    {
                        <Button
                            icon={'ios-close-outline'}
                            textColor={Properties.themeColor}
                            iconOnly
                            iconSize={30}
                            styles={styles.btn}
                            tapped={props.pressClose}
                            iconPack={'ionicons'}
                        />
                    }

                </View>
                {props.children}
                <View style={styles.modalFooter}>
                    <Button
                        text={'OK'}
                        textColor={Properties.themeColor}
                        styles={styles.btnOk}
                        tapped={props.pressOkay}
                    />
                </View>
            </View>
        </View>
    </Modal>
);


const { width, height } = Dimensions.get('window');


const styles = {
    modalBack: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modal: {
        width: 0.9 * width,
        backgroundColor: 'white',
        opacity: 1,
        borderRadius: 2,
        padding: 5,
        paddingLeft: 10,
        paddingBottom: 50
    },
    modalHeader: {
        flexDirection: 'row',
        marginBottom: 10
    },
    btn: {
        paddingTop: 0,
        paddingBottom: 0,
        paddingRight: -5,
        backgroundColor: 'transparent',

    },
    modalTitle: {
        flex: 1,
        fontSize: 17,
        fontWeight: 'bold',
        paddingTop: 2
    },
    modalFooter: {
        position: 'absolute',
        bottom: 5,
        right: ((0.9 * width) / 2) - 25,
    },
    btnOk: {
        width: 50,
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        paddingBottom: 0,
        paddingTop: 0,
        backgroundColor: 'white'
    },
};
