//imports
import React from 'react';
import { View } from 'react-native';


//a regular page with a header that centers all content
export const FeedContent = (props) => (
    <View style={styles.page}>
        {props.children}
    </View>
);

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#eee',
        paddingBottom: 50
    }
};
