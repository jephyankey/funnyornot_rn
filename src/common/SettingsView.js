//imports
import React from 'react';
import { ScrollView, Platform } from 'react-native';
import { HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT } from '../types';


//a regular page with a header that centers all content
export const SettingsView = (props) => (
    <ScrollView style={styles.page}>
        {props.children}
    </ScrollView>
);

const styles = {
    page: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 20,
        paddingLeft: 25,
        paddingTop: Platform.OS === 'ios' ? HEADER_HEIGHT + IOS_STATUS_BAR_HEIGHT + 20 : HEADER_HEIGHT + 20
    }
};
