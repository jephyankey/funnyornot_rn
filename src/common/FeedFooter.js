//imports
import React from 'react';
import { View, Platform } from 'react-native';
import { Spinner } from './Spinner';

//a regular page with a header that centers all content
export const FeedFooter = (props) => {
    if (props.showLoader) {
        return (
            <View style={styles.main}>
                <Spinner size={Platform.OS === 'ios' ? 'small' : 30} />
            </View>
        );
    }
    return <View style={{ marginBottom: 40 }} />;
};


const styles = {
    main: {
        marginBottom: 60, 
        flex: 1, 
        flexDirection: 'row', 
        justifyContent: 'center', 
        alignItems: 'center'
    }
};

