//imports
import React, { Component, PropTypes } from 'react';
import { Text, View, TouchableOpacity, Platform, StatusBar } from 'react-native';
import Icon2 from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';

import { Properties } from './Properties';
import { Button } from './Button';
import { StatusBarIos } from './StatusBarIos';

import { EventEmitter } from '../SERVICES';
import { NEW_THEME_CHOSEN, HEADER_HEIGHT, IOS_STATUS_BAR_HEIGHT } from '../types';


export class Header extends Component {

    propTypes: {
        title: React.PropTypes.string,
        showBackButton: React.PropTypes.bool,
        showRightButton: React.PropTypes.bool,
        btnText: React.PropTypes.string,
        btnIcon: React.PropTypes.string,
        btnIconSize: React.PropTypes.number,
        disableBtn: React.PropTypes.bool,
        onBtnPress: React.PropTypes.func,
        btnIconPack: React.PropTypes.string,
        backgroundColor: React.PropTypes.string,
        backButtonColor: React.PropTypes.string,
        statusBarColor: React.PropTypes.string,
        rightBtnColor: React.PropTypes.string,
        transparent: React.PropTypes.bool,
    }

    componentDidMount() {
        this.new_theme_subscription = EventEmitter.subscribe(NEW_THEME_CHOSEN, () => {
            this.forceUpdate();
        });
    }

    showBackButton() {
        if (this.props.showBackButton) {
            return (
                <TouchableOpacity onPress={this.popUp}>
                    <View style={styles.backButton} >
                        {
                            Platform.OS === 'android' ?
                                <Icon2 name={'ios-arrow-back'} size={35} color={this.props.backButtonColor || Properties.themeColor} />
                                :
                                <Icon2 name={'ios-arrow-back'} size={35} color={this.props.backButtonColor || Properties.themeColor} />
                        }
                        { (this.props.showRightButton && Platform.OS === 'ios') ? <View style={{ width: 40 }} /> : null }
                    </View>
                </TouchableOpacity>
            );
        }
        else {
            if (Platform.OS === 'ios')
                if (this.props.showRightButton)
                    return <View style={{ width: 70 }} />
        }
    }

    showRightButton() {
        if (this.props.showRightButton) {
            return (
                <Button
                    text={this.props.btnText}
                    icon={this.props.btnIcon}
                    iconSize={this.props.btnIconSize}
                    disabled={this.props.disableBtn}
                    styles={{ ...styles.rightButton, ...this.props.rightButtonStyles }}
                    tapped={this.props.onBtnPress}
                    textColor={this.props.rightButtonColor || Properties.themeColor}
                    disabledTextColor={this.props.rightButtonColor || Properties.themeColor}
                    iconPack={this.props.btnIconPack}
                />
            );
        }
        //else we are not showing the right Button
        if (Platform.OS === 'ios') {
            if (this.props.showBackButton)
                return <View style={{ width: 80 }} />
        }

    }


    popUp() {
        Actions.pop();
    }



    render() {
        if (Platform.OS === 'android') {
            return (
                <View style={this.props.transparent ? styles.overallTransparent : styles.overall}>
                    <View
                        style={{ ...styles.header, backgroundColor: this.props.backgroundColor }}>
                        {this.showBackButton()}
                        <View style={{ flex: 1 }}>
                            <Text style={{ ...styles.headerText, color: Properties.themeColor }}>{this.props.title}</Text>
                        </View>
                        {this.showRightButton()}
                    </View >
                </View >
            );
        }
        return (
            //ios
            <View style={styles.iosOverall}>
                <StatusBarIos useThemeForStatusBar={this.props.useThemeForStatusBar} />
                <View
                    style={{
                        ...styles.headerIos,
                        backgroundColor: this.props.backgroundColor,
                        borderBottomWidth: this.props.noBorder ? 0 : 0.5,
                    }}>
                    {this.showBackButton()}
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text
                            style={{
                                ...styles.headerText,
                                color: Properties.themeColor,
                                alignSelf: 'center',
                            }}>
                            {this.props.title}
                        </Text>
                    </View>
                    {this.showRightButton()}
                </View >
            </View >
        );
    }

    componentWillUnmount() {
        //unsubscribe from theme update
        EventEmitter.unsubscribe(NEW_THEME_CHOSEN, this.new_theme_subscription);
    }
}


Header.defaultProps = {
    backgroundColor: Platform.OS === 'android' ? 'transparent' : 'rgba(255,255,255,0.9)',
};


const styles = {
    //android
    overall: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    overallTransparent: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: HEADER_HEIGHT,
        backgroundColor: 'rgba(255,255,255,0.92)',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 10,
        position: 'relative',
        flex: 1,
        height: HEADER_HEIGHT,
        borderBottomWidth: 0,
        borderBottomColor: '#ccc',
    },
    //--------------------------ios------------------------------
    iosOverall: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
    },
    headerIos: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: IOS_STATUS_BAR_HEIGHT,
        left: 0,
        right: 0,
        height: HEADER_HEIGHT,
        borderBottomColor: '#ccc',
    },
    //both
    headerText: {
        fontSize: 20,
        fontWeight: '500',
        marginLeft: Platform.OS === 'android' ? 15 : 0,
    },
    backButton: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10,
        paddingTop: Platform.OS === 'ios' ? 2 : null,
        width: Platform.OS === 'ios' ? 80 : null,
    },
    rightButton: {
        paddingVertical: 0,
        marginBottom: -3, //ok for android
        backgroundColor: 'transparent',
    }
};
