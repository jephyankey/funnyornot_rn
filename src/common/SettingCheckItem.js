//imports
import React, { Component } from 'react';
import { View, TouchableOpacity, AsyncStorage } from 'react-native';

import { MyText, Properties } from '../common';
import Icon from './Icon';

import {
    UserService,
    Utilities,
    DatabaseService,
    Config
} from '../SERVICES';



export class SettingCheckItem extends Component {

    propTypes: {
        text: React.PropTypes.string.isRequired,
        subText: React.PropTypes.string,
        isChecked: React.PropTypes.string.isRequired,  //string of '1' or '0'
        setting: React.PropTypes.string.isRequired,
        noBorder: React.PropTypes.boolean, // whether a bottom border should be given or not
        serverRoute: React.PropTypes.boolean, //whether the setting should be saved to the server
    }


    constructor(props) {
        super(props);
        this.utilities = new Utilities();
        this.userService = new UserService(new Config(), this.utilities, new DatabaseService(this.utilities));
    }

    state = {
        isChecked: null
    }


    componentWillReceiveProps(nextProps) {
        //solution is to set state here
        this.setState({ isChecked: nextProps.isChecked });
    }

    changeCheckedState() {
        //change setting, if it was checked (1), now set it to 0, vice versa
        const val = this.state.isChecked === '1' ? '0' : '1';
        AsyncStorage.setItem(this.props.setting, val);
        this.setState({ isChecked: val });
        if (this.props.serverRoute) { //if there is a server side update
            this.userService.routeSettingToServer(this.props.serverRoute, val)
                .catch(() => {
                    //if it fails from the server, revert changes
                    const opposite_val = val === '1' ? '0' : '1';
                    this.setState({ isChecked: opposite_val });
                    this.utilities.showToast('Could not update setting on server');
                    AsyncStorage.setItem(this.props.setting, opposite_val);
                });
        }
    }

    noBorder() {
        if (this.props.noBorder) {
            return { borderBottomColor: '#fff' };
        }
    }

    render() {
        const { item, text, subText, checkbox } = styles;

        return (
            <TouchableOpacity style={{ ...item, ...this.noBorder() }} onPress={this.changeCheckedState.bind(this)}>
                <View style={{ flex: 1 }}>
                    <MyText styles={text}>{this.props.text}</MyText>
                    {
                        this.props.subText ?
                            <MyText styles={subText}>{this.props.subText}</MyText> :
                            null
                    }
                </View>
                <View style={checkbox}>
                    {
                        this.state.isChecked === '1' ?
                            <Icon name={'check-box'} color={this.props.checkedColor || Properties.themeColor} size={20} /> :
                            <Icon name={'check-box-outline-blank'} color={'#aaa'} size={20} />
                    }
                </View>
            </TouchableOpacity >
        );
    }
}


SettingCheckItem.defaultProps = {
    checkState: false,
    text: 'Provide Text',
    subText: null,
};



const styles = {
    item: {
        paddingVertical: 12,
        borderBottomWidth: 0.5,
        borderBottomColor: '#eee',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    text: {
        color: '#555',
        fontSize: 15
    },
    subText: {
        color: '#aaa',
        fontSize: 14
    },
    checkbox: {
        // paddingRight: 10,
        paddingLeft: 20
    }
};
