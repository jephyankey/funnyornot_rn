//imports
import React from 'react';
import { View, Text } from 'react-native';
import { Properties } from './Properties';

//a regular page with a header that centers all content
export const MenuSection = (props) => (
    <View style={[styles.overall, props.border ? { borderTopWidth: 1 } : {}]}>
        <View style={styles.subView}>
            <View style={styles.titleView}>
                <Text style={styles.title}>{props.title}</Text>
            </View>
            <View>
                {props.children}
            </View>
        </View>
    </View>
);


MenuSection.defaultProps = {
    border: true
};


const styles = {
    overall: {
        borderTopColor: Properties.greyBackColor,
        paddingTop: 15
    },
    subView: {
        paddingLeft: 25
    },
    titleView: {
        marginBottom: 10
    },
    title: {
        color: '#CCC',
        fontSize: 16,
    }

};
