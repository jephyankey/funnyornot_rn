//color names
export const DEFAULT_THEME = '#9E3C32';
export const BLUE_DINAH = '#1276b9';
export const BLACK_MAMBA = '#000';
export const PURPLE_BARBIE = '#800080';
export const PINK_LINA = '#FF69B4';
export const GREEN_ELDY = '#228B22';
export const RED_LUCY = '#D64738';


let themeColor: string = DEFAULT_THEME;
let statusBarColor: string = DEFAULT_THEME;
let greyBackColor: string = '#EEE';
let darkBlue: string = '#417d9a';
let profileColor: string = '#bbb';

const object_of_colors = {
    [DEFAULT_THEME]: { r: 158, g: 60, b: 50 },
    [BLUE_DINAH]: { r: 18, g: 118, b: 185 },
    [BLACK_MAMBA]: { r: 0, g: 0, b: 0 },
    [PURPLE_BARBIE]: { r: 128, g: 0, b: 128 },
    [PINK_LINA]: { r: 255, g: 108, b: 180 },
    [GREEN_ELDY]: { r: 34, g: 139, b: 34 },
    [RED_LUCY]: { r: 214, g: 71, b: 56 }
};


export class Properties {

    static set mainColor(color: string) {
        themeColor = color || DEFAULT_THEME;
        statusBarColor = color || DEFAULT_THEME;
    }

    static get themeColor() {
        return themeColor;
    }
    static get statusBarColor() {
        return statusBarColor;
    }

    static get greyBackColor() {
        return greyBackColor;
    }

    static get profileColor() {
        return profileColor;
    }

    static get darkBlue() {
        return darkBlue;
    }

/**
 * color stops used by scrollable tabs to animate the change in color 
 * when swiping between tabs
 * this is a modified version of the facebook example on github
 * see: https://github.com/skv-headless/react-native-scrollable-tab-view/blob/master/examples/FacebookTabsExample/FacebookTabBar.js
 * similar to the iconColor function
 */
    static progressColor(progress) {
        const red = object_of_colors[themeColor].r + ((204 - object_of_colors[themeColor].r) * progress);
        const green = object_of_colors[themeColor].g + ((204 - object_of_colors[themeColor].g) * progress);
        const blue = object_of_colors[themeColor].b + ((204 - object_of_colors[themeColor].b) * progress);
        return `rgb(${red}, ${green}, ${blue})`;
    }

}
