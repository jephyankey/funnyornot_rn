
import { PropTypes } from 'react';
import { requireNativeComponent, View } from 'react-native';


var iface = {
  name: 'VideoPlayer',
  propTypes: {
    looping: PropTypes.bool, //default is true
    state: PropTypes.string, //PLAY, PAUSE, STOP
    source: PropTypes.string,
    release: PropTypes.bool,
    ...View.propTypes,
  },
};

module.exports = requireNativeComponent('CustomVideoPlayer', iface);
