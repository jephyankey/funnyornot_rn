import SQLite from 'react-native-sqlite-storage';


export class DatabaseService {

    constructor(utilities) {
        this.sqlInstance = null;
        this.utilities = utilities;
    }

    //returns a promise 
    getSqlInstance() {
        if (!this.sqlInstance) {
            this.sqlInstance = SQLite.openDatabase({ name: '_funnyornot.db', location: 'default' });
        }
        return this.sqlInstance;
    }

    /**
     * remember: data returned by query will look like this:
     * [{column: value, column: value, ...}, {column: value, column: value, ...}, ...]
     */
    query(sql, params = [], mode = 'SELECT') {
        const db = this.getSqlInstance();
        return new Promise((resolve, reject) => {
            db.executeSql(sql, params,
                (result) => {
                    if (mode === 'SELECT') {
                        resolve(this.fetchAll(result));
                    }
                    else {
                        resolve(true);
                    }
                },
                (error) => {
                    this.utilities.handleErrors(error, ' ERROR IN DBASE CLASS in query function');
                    reject(error);
                });
        });
    }


    /**
     * runs a query and returns an object that can be used to fetch the data
     * unlike query above which  resolves with the actual data
     * query above loops through said object to get all the data before resolving it.
     * NOw if the caller, will also like to loop through it for some reason, 
     * then we will be looping through the 
     * database result multiple times, by just returning the object here and not looping through it,
     * the caller can perform other operations while looping tru the dbase object ONCE
     */
    queryRaw(sql, params = [], mode = 'SELECT') {
        const dbs = this.getSqlInstance();
        return new Promise((resolve, reject) => {
            dbs.executeSql(sql, params,
                (result) => {
                    if (mode === 'SELECT') {
                        resolve(result);
                    }
                    else {
                        resolve(true);
                    }
                },
                (error) => {
                    this.utilities.handleErrors(error, ' ERROR IN DBASE CLASS in query function');
                    reject(error);
                });
        });
    }


    fetch(result) {
        if (result.rows.length <= 0) return false;
        return result.rows.item(0);
    }

    //TODO is it possible to simply return the entire item array??
    fetchAll(result) {
        const output = [];
        for (let i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }
        return output;
    }

    //fetch all but return result as returned by dbase
    fetchRaw(result) {
        return result;
    }
}


export interface iDatabaseService {
    query: (sql: string, params?: Array, mode?: string) => any,
    queryRaw: (sql: string, params?: Array, mode?: string) => any,
    fetch: (result: any) => Array
}
