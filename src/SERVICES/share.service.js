import { NativeModules } from 'react-native';

export class ShareService {

    inviteFriends() {
        const body = "You won't stop laughing! Check out FunnyorNot on the play store: https://play.google.com/store/apps/details?id=com.funnyornot";
        NativeModules.MySharer.shareText(body);
    }

    launchExternalPlayer(uri) {
        NativeModules.MySharer.sendVideoIntent(uri);
    }

    launchExternalPlayerAudio(uri) {
        NativeModules.MySharer.sendAudioIntent(uri);
    }

} 
