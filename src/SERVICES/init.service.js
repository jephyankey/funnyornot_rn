
import { AsyncStorage, Platform } from 'react-native';
import axios from 'axios';
import {
    PRIVATE_DP_SETTING, INVISIBILITY_SETTING, USER_AVATAR, NOTIFICATION_SETTING,
    PREVIOUSLY_RUN, USER_ID, DEVICE_ID
} from '../types';


export class InitService {
    constructor(config, utilities, dbService) {
        this.config = config;
        this.dbService = dbService;
        this.utilities = utilities;
    }

    /**
     * returns a promise
     */
    hasRunBefore() {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(PREVIOUSLY_RUN)
                .then(data => {
                    if (data)
                        resolve(true);
                    else
                        resolve(false);
                })
                .catch(() => reject());
        });
    }

    setHasRunBefore() {
        return new Promise((resolve, reject) => {
            AsyncStorage.setItem(PREVIOUSLY_RUN, 'TRUE')
                .then(() => resolve())
                .catch(() => reject());
        });
    }


    createAllDbases() {
        const t1 = `CREATE TABLE IF NOT EXISTS contacts 
                    (user_id INTEGER PRIMARY KEY AUTOINCREMENT, phone TEXT DEFAULT '', 
                    name TEXT, on_app INTEGER DEFAULT 1)`;

        const t2 = `CREATE TABLE IF NOT EXISTS blocked 
                    (user_id INTEGER PRIMARY KEY, user_name TEXT DEFAULT '')`;

        const t3 = `CREATE TABLE IF NOT EXISTS following 
                    (user_id INTEGER PRIMARY KEY, user_name TEXT DEFAULT '')`;


        return new Promise((resolve, reject) => {
            this.dbService.query(t1, [], 'CREATE').then(
                () => { return this.dbService.query(t2, [], 'CREATE'); }
            )
                .then(() => { return this.dbService.query(t3, [], 'CREATE'); })
                .then(() => { resolve(true); })
                .catch(err => {
                    this.utilities.handleErrors(err, 'ERROR CREATING TABLES');
                    reject(false);
                });
        });
    }


    /**
     * when an old user reinstalls the app, this is used to fetch all relevant data
     */
    loadExistingSettings(user_id) {
        return new Promise((resolve, reject) => {
            const url = `${this.config.getBaseUrl()}/settings/${user_id}`;
            axios.get(url)
                .then(response => {
                    const settings = response.data.data[0];
                    AsyncStorage.multiSet([
                        [INVISIBILITY_SETTING, settings.invisible.toString()],
                        [PRIVATE_DP_SETTING, settings.who_sees_dp.toString()],
                        [NOTIFICATION_SETTING, settings.no_notifications.toString()],
                        [USER_AVATAR, settings.avatar_url || '']
                    ])
                        .then(() => {
                            //load all blocked users into local database
                            const b = settings.blocked_list;
                            if (b.length > 0) {
                                let query = `INSERT INTO blocked (user_id, user_name) VALUES (${b[0].blocked_id}, '@${b[0].username}' )`;
                                for (let i = 1; i < b.length; i++) {
                                    query = `${query}, (${b[i].blocked_id}, '@${b[i].username}')`;
                                }
                                this.dbService.query(query, null, 'INSERT')
                                    .then(() => resolve())
                                    .catch(err => {
                                        this.utilities.handleErrors(err, 'ERROR COPYING BLOCKED LIST INTO LOCAL DBASE');
                                        reject();
                                    });
                            }
                            else resolve();
                        });
                })
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject(err);
                });
        });
    }

    configurePushNotifications(device_info) {
        if (device_info.userId && device_info.pushToken) {
            AsyncStorage.multiGet([USER_ID, DEVICE_ID])
                .then(data => {
                    const user_id = data[0][1];
                    const device_id = data[1][1];
                    if (user_id === null) //possibly first time app install, id not stored yet
                        return;
                    if (device_id === device_info.userId) // device id has been previosly set
                        return;
                    this.savePushNotificationIds(user_id, device_info.userId, device_info.pushToken);
                });
        }
    }

    savePushNotificationIds(user_id, device_id, push_token) {
        const url = `${this.config.getBaseUrl()}/deviceId`;
        axios.post(url, {
            user_id,
            device_id,
            push_token,
            os: Platform.OS
        })
            .then(() => AsyncStorage.setItem(DEVICE_ID, device_id))
            .catch(err => {
                this.utilities.handleErrors(err, 'ERROR SAVING DEVICE ID AND PUSH TOKEN');
            });
    }

}
