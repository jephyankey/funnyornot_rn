
export class Config {

    constructor() {
        this.api_base_url = 'https://2-dot-funnyornot-backend-1412.appspot.com';
         //also definded in utilities, so remeber to change it there if it is changed there
        this.api_base_url_dynamic_html = 'https://4-dot-funnyornot-backend-1412.appspot.com';
        this.app_version = '1.1.0';
    }

    getBaseUrl(): string {
        return this.api_base_url;
    }

    getBaseUrlDynamicHtml(): string {
        return this.api_base_url_dynamic_html;
    }

    getAppVersion() :string {
        return this.app_version;
    }

}


export interface iConfig {
    getBaseUrl: () => string
}
