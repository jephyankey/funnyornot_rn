//@flow
import axios from 'axios';
import {
    TRENDING_REFRESH, ALLTIME, HOME, FAVOURITES,
    LOAD_FAIL, NO_INTERNET, NO_INTERNET2,
    RECOMMENDED_JOKES, RECOMMENDED_FOLLOWS
} from '../types';


export class FeedService {

    RANK: number = 1;
    INDEX: number = 0;

    pagesize: number = 6;
    media_pagesize: number = 24

    config: any;
    utilities: any;
    userService: any;
    contactService: any;
    dbService: any;


    constructor(config, utilities, userService, contactService, dbService) {
        this.config = config;
        this.utilities = utilities;
        this.userService = userService;
        this.contactService = contactService;
        this.dbService = dbService;
    }

    //returns the number of items on each page (ie pagesize)
    getPageSize() {
        return this.pagesize;
    }

    getMediaPageSize() {
        return this.media_pagesize;
    }

    //fetch feed from server
    fetchFeed(page: number = 1, type: string = HOME) {
        let feed_url;
        return new Promise((resolve, reject) => {
            this.contactService.getContactIds()
                .then(all_ids => {
                    const ids = all_ids || 'xxx';
                    //above, xxx is arbitrary value cos an empty value will break the route
                    this.userService.getCurrentUsersId().then(
                        the_user_id => {
                            if (type === HOME) {
                                feed_url = `${this.config.getBaseUrl()}/feed/${the_user_id}/${ids}/${page}/${this.pagesize}`;
                            }
                            else if (type === 'image-feed') {
                                feed_url = `${this.config.getBaseUrl()}/imageFeed/${the_user_id}/${ids}/${page}/${this.media_pagesize}`;
                            }
                            else if (type === 'video-feed') {
                                feed_url = `${this.config.getBaseUrl()}/videoFeed/${the_user_id}/${ids}/${page}/${this.media_pagesize}`;
                            }
                            else if (type === FAVOURITES) {
                                feed_url = `${this.config.getBaseUrl()}/favourites/all/${the_user_id}/${page}/${this.pagesize}`;
                            }

                            axios.get(feed_url)
                                .then(response => {
                                    const data = response.data.data;
                                    const final_data = this.formatFeedData(data, 'home');
                                    resolve(final_data);
                                })
                                .catch((error) => {
                                    if (error.response)
                                        reject(LOAD_FAIL);
                                    else
                                        reject(NO_INTERNET);
                                });
                        });
                })
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject(LOAD_FAIL);
                });
        });
    }

    //exploring funny posts in a country(s)
    fetchExplore(page: number = 1) {
        let ids;
        let user_id;
        return new Promise((resolve, reject) => {
            //get ids of my contacts, so their posts don't appear in explore
            this.contactService.getContactIds()
                .then(all_ids => {
                    //if contacts is empty assign arbitrary xxxx value, 
                    //if not nothing is sent it breaks the api route
                    ids = all_ids || 'xxx';
                    return this.userService.getCurrentUsersId();//get user id
                })
                .then(the_user_id => {
                    user_id = the_user_id;
                    return this.userService.getCurUsersAffiliateCountries();//get the country code
                })
                .then(country_codes => {
                    const explore_url = `${this.config.getBaseUrl()}/explore/${country_codes}/${user_id}/${ids}/${page}/${this.pagesize}`;

                    axios.get(explore_url)
                        .then(response => {
                            const data = response.data.data;
                            const final_data = this.formatFeedData(data, 'explore');
                            resolve(final_data);
                        })
                        .catch((error) => {
                            if (error.response)
                                reject(LOAD_FAIL);
                            else
                                reject(NO_INTERNET);
                        });
                })
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject(LOAD_FAIL);
                });
        });
    }

    /**
     * TODO: trending should take into account all affiliate countries, not just users country
     * (will require changing mostly the api,)
     * fetch trending, also known as trending...
     * also known as ranking
     */
    getRankings(page: number = 1, filter: string = ALLTIME, activity: any = null) {
        let co_co: string; //country code
        return new Promise((resolve, reject) => {
            this.userService.getCurrentUsersCountCode()
                .then(country_code => {
                    co_co = country_code;
                    return this.userService.getCurrentUsersId();
                })
                .then(user_id => {
                    const trending_url = `${this.config.getBaseUrl()}/trending/${filter}/${co_co}/${user_id}/${page}/${this.pagesize}`;

                    axios.get(trending_url)
                        .then(response => {
                            const data = response.data.data;
                            const final_data = this.formatFeedData(data, 'trending', activity);
                            resolve(final_data);
                        })
                        .catch((error) => {
                            if (error.response)
                                reject(LOAD_FAIL);
                            else
                                reject(NO_INTERNET);
                        });
                })
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject(LOAD_FAIL);
                });
        });
    }

    //reformat feed data using local data.. add the names of contacts
    //and local versions of avatars if available
    //rank is added, but only for trending/trending
    formatFeedData(arrFeed: Array<Object>, mode: string = 'feed', activity: any = null) {
        if (activity === TRENDING_REFRESH) {
            /**
             * CURRENTLY, this activity variable is only used for trending
             * reason: in trending, if we perform a refresh after we have already loaded
             * some feed, the value of RANK still continues from the previous feed,
             * thus messing up the numbering.
             * this fixes it.
             */
            this.RANK = 1;
        }
        for (let i = 0; i < arrFeed.length; i++) {
            const element = arrFeed[i];
            //-------------------------------------------------------
            element.index = this.INDEX++;
            //sets an index on each element, could be used to
            //identify position of post in a bunch, 
            //as used in the mediafeed component for a slideshow

            if (mode === 'trending')
                element.rank = this.RANK++;
            else if (mode === 'explore')
                element.name = `@${element.username}`;

            //stars or funnies
            element.funnyORnot = element.stars;
            if (element.stars === null) {
                element.stars = 0;
            }

            //replace all http urls with https equivalent (remember that http does not work on ios)
            element.media_url = this.utilities.httpsFromHttp(element.media_url);
            element.poster_image = this.utilities.httpsFromHttp(element.poster_image);
            element.avatar_url = this.utilities.httpsFromHttp(element.avatar_url);
        }
        return arrFeed;
    }


    /**
     * fetch all jokes or media uploaded by a given user, in batches
     *  ----mode can be either 'jokes' or 'media'
     */
    getSomeonesJokes(user_id: string, page: number = 1, mode = 'jokes') {
        return new Promise((resolve, reject) => {
            this.userService.getCurrentUsersId()
                .then(someones_id => {
                    const url = `${this.config.getBaseUrl()}/someone/${mode}/${someones_id}/${user_id}/${page}/${this.pagesize}`;

                    axios.get(url)
                        .then(response => {
                            const final_data = this.formatFeedData(response.data.data, 'profile');
                            resolve(final_data);
                        })
                        .catch((error) => {
                            if (error.response)
                                reject(LOAD_FAIL);
                            else
                                reject(NO_INTERNET);
                        });
                })
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject(LOAD_FAIL);
                });
        });
    }


    fetchComments(parent_id: number) {
        return new Promise((resolve, reject) => {
            const url = `${this.config.getBaseUrl()}/comments/${parent_id}`;
            axios.get(url)
                .then(response => {
                    resolve(response.data.data);
                })
                .catch((error) => {
                    if (error.response)
                        reject(LOAD_FAIL);
                    else
                        reject(NO_INTERNET2);
                });
        });
    }


    /**
     * list of users recommended to follow, 
     */
    getRecommendations(which = RECOMMENDED_FOLLOWS) {
        let route = null;
        if (which === RECOMMENDED_FOLLOWS)
            route = 'recommendUsersToFollow';
        else if (which === RECOMMENDED_JOKES)
            route = 'recommendJokes';

        return new Promise((resolve, reject) => {
            this.contactService.getContactIds()
                .then(ids => {
                    const contact_ids = ids || 0;
                    this.userService.getCurrentUsersId().then(
                        user_id => {
                            const url = `${this.config.getBaseUrl()}/${route}/${user_id}/${contact_ids}`;
                            axios.get(url)
                                .then(response => {
                                    const final_data = this.formatFeedData(response.data.data, 'profile');
                                    resolve(final_data);
                                })
                                .catch(() => { reject(); });
                        });
                });
        });
    }

}


export interface iFeedService {
    getPageSize: () => number,
    getMediaPageSize: () => number,
    fetchFeed: (page: number, type?: string) => any,
    fetchExplore: (page: number) => any,
    getRankings: (page: number, filter?: string) => Promise,
    formatFeedData: (arrFeed: Array, mode?: string, activity?: string) => Array,
    getSomeonesJokes: (user_id: string, page?: number, mode?: string) => Promise
}
