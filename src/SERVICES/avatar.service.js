
import { AsyncStorage } from 'react-native';
import axios from 'axios';

import { NO_INTERNET, USER_AVATAR, NEW_AVATAR_UPLOADED } from '../types';
import { EventEmitter } from '../SERVICES';

export class AvatarService {
    AVATAR_URL: string = null; //path to avatar on device

    constructor(config, utilities, dbService) {
        this.utilities = utilities;
        this.dbaseService = dbService;
        this.config = config;
    }

    /**
     * set the avatar, i.e. insert into database, given an object that
     * contains the server url and the filename stored as "'server_url'==='filename'"
     */
    setCurrentUsersAvatar(avatar_url: string) {
        return new Promise((resolve, reject) => {
            AsyncStorage.setItem(USER_AVATAR, avatar_url)
                .then(() => {
                    this.AVATAR_URL = avatar_url;
                })
                .then(() => {
                    EventEmitter.dispatch(NEW_AVATAR_UPLOADED, this.AVATAR_URL);
                    resolve();
                })
                .catch(() => reject());
        });
    }

    // both avatar url and filename are saved in dbase as a string in the form: "url===filename"
    // originally done so that using the name, we could save the avatar on the local file system
    getCurrentUsersAvatar() {
        return new Promise((resolve) => {
            if (this.AVATAR_URL) {
                resolve(this.AVATAR_URL);
            }
            else {
                AsyncStorage.getItem(USER_AVATAR)
                    .then(avatar_path => {
                        this.AVATAR_URL = avatar_path;
                        resolve(this.AVATAR_URL);
                    })
                    .catch(() => {
                        resolve(null);
                    });
            }
        });
    }

    getRandomAvatar() {
        return `av${Math.round(Math.random() * 38)}`;
    }

    getNonRegisteredAvatar() {
        return '../imgs/default_grey_avatar.png';
    }


    /**
     * given a list of user_ids, get all their avatar urls
     */
    getAvatarUrls(user_list) {
        return new Promise((resolve, reject) => {
            const url = `${this.config.getBaseUrl()}/avatarUrls/${user_list}`;
            axios.get(url)
                .then(response => {
                    resolve(response.data.data);
                })
                .catch(() => reject(NO_INTERNET));
        });
    }
}


export interface iAvatarService {
    getCurrentUsersAvatar: () => Promise,
    getRandomAvatar: () => string,
    getNonRegisteredAvatar: () => string
}

