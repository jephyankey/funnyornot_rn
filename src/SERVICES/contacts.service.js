import { NativeModules, AsyncStorage, Platform, PermissionsAndroid } from 'react-native';
import axios from 'axios';
import Contacts from 'react-native-contacts';
import { CONTACTS_PERMISSION_DENIED, FAIL_TO_READ_CONTACTS } from '../types';
/**
 * get a list of cached contacts from sqlite databse
 * get a list of user's contacts from his phone
 * save a list of users contacts to sqlite database (cache)
 */
export class ContactService {

    constructor(config, utilities, dbaseService) {
        this.CONTACTS_ONLY = [];
        this.CONTACTS_WITH_NAMES = [];
        this.contactIds = null;
        this.dbaseService = dbaseService;
        this.config = config;
        this.utilities = utilities;
    }

    /**
     * fetches contacts from device, returns a PROMISE
     */
    fetchContacts() {
        return new Promise((resolve, reject) => {
            if (Platform.OS === 'android') {
                PermissionsAndroid.requestPermission(
                    PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                    {
                        'title': 'FunnyorNot Contacts Permission',
                        'message': 'FunnyorNot would like to access your contacts to show you funny content from people you already know'
                    })
                    .then(perm => {
                        if (perm) {
                            NativeModules.MyContacts.readContacts()
                                .then(data => resolve(data))
                                .catch(err => reject(err));
                        }
                        else{
                            reject(CONTACTS_PERMISSION_DENIED);
                        }
                    })
                    .catch(err => reject(FAIL_TO_READ_CONTACTS));
            }
            else {
                //this is ios
                Contacts.getAll((err, contacts) => {
                    if (err) {
                        if (err.type === 'permissionDenied')
                            reject(CONTACTS_PERMISSION_DENIED);
                        else
                            reject(FAIL_TO_READ_CONTACTS);
                    } else {
                        resolve(contacts);
                    }
                });
            }
        });
    }


    /**
     * saves contacts into a data structure in memory
     * CONTACTS_ONLY [num1, num2, num3...] 
     * CONTACS_WITH_NAMES [{phone_no, contact_name}, {phone_no, contact_name} .....]
     */
    keepInRAM(contact_list: Array) {
        if (Platform.OS === 'android') {
            for (let i = 0; i < contact_list.length; i++) {
                //only save contacts with names
                if (contact_list[i].name && contact_list[i].phone) {
                    const phone_no = contact_list[i].phone;
                    const contact_name = contact_list[i].name;
                    this.CONTACTS_WITH_NAMES.push({ phone_no, contact_name });
                    this.CONTACTS_ONLY.push(phone_no);
                }
            }
        }
        else { //ios
            for (let i = 0; i < contact_list.length; i++) {
                //only save contacts with names
                const c = contact_list[i];
                if ((c.givenName || c.familyName) && c.phoneNumbers.length > 0) {
                    //create contact info for all phone numbers
                    const p = c.phoneNumbers;
                    for (let x = 0; x < p.length; x++) {
                        const phone_no = p[x].number;
                        let contact_name = '';
                        if (c.givenName && c.familyName)
                            contact_name = `${c.givenName} ${c.familyName}`;
                        else
                            contact_name = c.givenName ? c.givenName : c.familyName;
                        this.CONTACTS_WITH_NAMES.push({ phone_no, contact_name });
                        this.CONTACTS_ONLY.push(phone_no);
                    }
                }
            }
        }
    }


    /**
     * using the contacts from the phone, get the list of 
     * contacts who are actually on the app (i.e. ur 'friends')
     * save these to the database
     * NORMALLY, YOU SHOULD ONLY CALL THIS AFTER keepInRAM() 
     * since it uses the CONTACS_WITH_NAMES and CONTACTS_ONLY arrays
     * 
     * NOTE:::.....LOOK INTO.... 
     * now that we are no longer saving contacts not on the app, maybe we can 
     * simplify the friendfider algorithm in general??? ... not too sure...
     */
    findAndSaveFriends(country_code) {
        //get list of friends from server: [[pos, user_id], [pos, user_id]...]
        return new Promise((resolve, reject) => {
            axios.post(`${this.config.getBaseUrl()}/findFriends`, {
                country_code,
                contact_list: encodeURIComponent(this.CONTACTS_ONLY.toLocaleString())
            })
                .then(response => {
                    const data = response.data;
                    //1. save all my contacts who use the app as a single string
                    //delete existing non-app contacts if any 
                    //(will be the case ONLY AFTER the first setup of the app)
                    this.dbaseService.query('DELETE FROM contacts').then(() => { return; })
                        /**
                         * TODO.... stop deleting from the entire database
                         * this was necesary cos if a person was previously not on the app (ie he'll be in ur dbase still, as not on the app)
                         * and he came onto the app, deleting before storing everything, will ensure that he did not still remain
                         * in ur db as a contact not on the app, while being added as a contact on the app,
                         * now that we do not store the list of those not on the app, this is not really necessary,
                         * however we are leaving it for now, cos for those already using the app, we want to clear
                         * their dbs of the list of non-app contacts (ie contacts not using the app)
                         * (remember, it was formerly used only on the invite friends page anyway)
                         */
                        .then(() => {
                            return AsyncStorage.setItem('all_ids', data.all_ids.toString());
                        })
                        .then(() => {
                            //2. save each individual contact with his phone, id, 
                            //and name as saved by the user
                            let sequence = Promise.resolve();

                            const results = data.data;
                            if (results.length > 0) //USER HAS SOME CONTACTS WHO USE THE APP
                            {
                                const sql = `INSERT OR IGNORE INTO contacts 
                                            (user_id, phone, name, on_app) VALUES (?, ?, ?, ?)`;
                                /** from the api, results should now contain the contacts 
                                 * out of ur entire contact list who use funny or not
                                 * it will have their ids, and their positions 
                                 * within the CONTACTS_WITH_NAMES (and CONTACTS_ONLY_) array
                                 * */
                                for (let i = 0; i < results.length; i++) {
                                    const element = results[i];
                                    const user_id = element[1]; // see api results sample
                                    const phone_no = this.CONTACTS_WITH_NAMES[element[0]].phone_no;
                                    const name = this.CONTACTS_WITH_NAMES[element[0]].contact_name;
                                    //'remove' this element from the CONTACTS ARRAY
                                    //since splice is potentially expensive, 
                                    //we 'remove' by setting the phone_no to -1
                                    this.CONTACTS_WITH_NAMES[element[0]].phone_no = -1;  /** is
                                    not really necessary, if we are never going to run this.saveNonFriends(), but we might
                                    decided to use nonFriends later, so i'm leaving it for now */
                                    const params = [user_id, phone_no, name, 1];

                                    //create a sequnce of promises that 
                                    //each resolves to create the next
                                    sequence = sequence.then(() => {
                                        return this.dbaseService.queryRaw(sql, params)
                                            .then(
                                            () => { },
                                            err => this.utilities.handleErrors(err, 'error inserting CONTACT')
                                            );
                                    });
                                }
                            }
                            return sequence;
                        })
                        // .then(() => { return this.saveNonFriends(); })
                        .then(() => {
                            //"delete" contact variables in RAM
                            this.CONTACS_WITH_NAMES = null;
                            this.CONTACTS_ONLY = null;
                            resolve();
                        })
                        .catch(err => reject(err));
                })
                .catch(error => {
                    this.utilities.handleErrors(error, 'an error in findfriends');
                    reject(error);
                });
        });
    }

    /**
     * save contacts who do not use the app, (should only be called withing findAndSaveFriend. 
     * This is cos this fx determines that a contact 
     * does not use the app, by checking if the phone_no is -1, 
     * a property that is set by the findAndSaveFriends function on the 
     * CONTACS_WITH_NAMES array )
     */
    saveNonFriends() {
        return new Promise((resolve, reject) => {
            const sql = 'INSERT INTO contacts (phone, name, on_app) VALUES (?, ?, ?)';
            const len = this.CONTACTS_WITH_NAMES.length;
            let sequence = Promise.resolve();

            //do actual saving of contacts not on app (socalled NonFriends):
            for (let i = 0; i < len; i++) {
                if (this.CONTACTS_WITH_NAMES[i].phone_no !== -1) //contact does not use the app
                {
                    //let user_id use the AUTO INCREMENT VALUE :-)
                    const phone_no = this.CONTACTS_WITH_NAMES[i].phone_no;
                    const name = this.CONTACTS_WITH_NAMES[i].contact_name;
                    const params = [phone_no, name, 0];
                    //sequence of promises is created:
                    sequence = sequence.then(() => {
                        this.dbaseService.queryRaw(sql, params);
                    });
                }
            }
            sequence = sequence.then(() => resolve()).catch(err => reject(err));
        });
    }

    /**
     * fetches contacts who use the app from application's local database
     * both names and numbers
     */
    getAppFriends() {
        return new Promise((resolve, reject) => {
            const sql = `SELECT user_id, phone, name FROM contacts WHERE on_app = 1 
                        AND user_id NOT IN (SELECT user_id FROM blocked) ORDER BY name ASC`;
            this.dbaseService.queryRaw(sql).then(
                data => resolve(data),
                err => {
                    this.utilities.handleErrors(err, 'could not read app friends from dbase');
                    reject(err);
                }
            );
        });
    }

    getNonAppFriends() {
        return new Promise((resolve, reject) => {
            const sql = `SELECT user_id, phone, name FROM contacts WHERE on_app = 0 
                         ORDER BY name ASC`;
            this.dbaseService.query(sql).then(
                data => resolve(data),
                err => reject(err)
            );
        });
    }

    /**
     * returns the ids of current users contacts who use the app as 
     * a string (this is previously stored int he database)
     */
    getContactIds() {
        return new Promise((resolve, reject) => {
            if (this.contactIds) {
                resolve(this.contactIds);
            }
            else {
                AsyncStorage.getItem('all_ids').then(
                    data => {
                        this.contactIds = data;
                        resolve(this.contactIds);
                    },
                    err => reject(err)
                );
            }
        });
    }

    getBlockedContacts() {
        return new Promise((resolve, reject) => {
            const sql = `SELECT user_id, phone, name FROM blocked JOIN 
                        contacts USING(user_id) WHERE on_app = 1`;
            this.dbaseService.query(sql).then(
                data => resolve(data),
                err => reject(err)
            );
        });
    }

    getBlockedNonContacts() {
        return new Promise((resolve, reject) => {
            //get all blocked ppl who are not in ur list of contacts of those who use the app
            const sql = `SELECT user_id, user_name FROM blocked WHERE user_id NOT IN 
                        (SELECT user_id FROM contacts WHERE on_app = 1)`;
            this.dbaseService.query(sql).then(
                data => resolve(data),
                err => reject(err)
            );
        });
    }

}


export interface iContactService {
    fetchContacts: () => Promise,
    keepInRAM: (contact_list: Array) => void,
    findAndSaveFriends: () => Promise,
    saveNonFriends: () => Promise,
    getAppFriends: () => Promise,
    getNonAppFriends: () => Promise,
    getContactIds: () => Promise,
    getBlockedContacts: () => Promise,
    getBlockedNonContacts: () => Promise
}

