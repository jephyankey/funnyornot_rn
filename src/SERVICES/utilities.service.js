import { Alert, BackAndroid, Dimensions, Platform } from 'react-native';
import Toast from '@remobile/react-native-toast';
import axios from 'axios';
import { LOAD_FAIL, SHOW_TOAST, HIDE_TOAST } from '../types';
import { EventEmitter } from './EventEmitter';


export class Utilities {

    constructor() {
        this.api_base_url = 'https://2-dot-funnyornot-backend-1412.appspot.com';
    }

    resizeImage(width, height) {
        const max_w = Dimensions.get('window').width; //device width
        const max_h = Dimensions.get('window').height * 0.9; //device width
        const ratio1 = max_w / width;
        const ratio2 = max_h / height;
        const ratio = ratio1 < ratio2 ? ratio1 : ratio2;
        if (ratio < 1) {
            //this means the actual width is bigger than the device width
            width *= ratio;
            height *= ratio;
        }
        return { width, height };
    }

    handleErrors(error, extra = null) {
        //--------------special errors that mostly occur when fetching feed data--------
        if (error === LOAD_FAIL)
            this.showToast('Error at our end. Kindly try again later.');
        // else if (error === NO_INTERNET || error === NO_INTERNET2)
        //     this.showToast('No Internet Connection!');
        //-------------------------------------------------------------------------------

        ///----------------SEND ERROR TO SERVER FOR LOGGING PURPOSES---------------------
        const error_data = {};
        if (extra) {
            error_data['EXTRA_INFO'] = extra;
        }
        error_data['ERROR_INFO'] = error.toString();
        axios.post(`${this.api_base_url}/logErrors`, {
            os: Platform.OS,
            version: Platform.Version,
            error: JSON.stringify(error_data)
        }).catch();
    }


    showToast(message, duration = 5000, icon = 'ios-happy-outline') {
        if (Platform.OS === 'ios') {
            EventEmitter.dispatch(SHOW_TOAST, { message, duration, icon });
        }
        else {
            Toast.hide();// cos for some reason 2 toasts do not show at the same time
            Toast.show(message, duration, 'bottom');
        }
    }

    hideToast() {
        if (Platform.OS === 'ios') {
            EventEmitter.dispatch(HIDE_TOAST, null);
        }
        else {
            Toast.hide();
        }
    }

    //something went wrong with initializing the app
    initError(err) {
        msg = 'Initialization failed! Please uninstall and then reinstall the application. Thank you.';

        if (Platform.OS === 'android') {
            Alert.alert(
                'Initialization Error!',
                msg,
                [
                    { text: 'OKAY', onPress: () => BackAndroid.exitApp() },
                ]
            );
        }
        else {
            Alert.alert(
                'Initialization Error!',
                msg,
            );
        }
    }


    truncateString(striing, maxlength = 26) {
        return (striing.length > maxlength) ? `${striing.substr(0, maxlength - 2)}...` : striing;
    }


    getAllCountryCodes() {
        return `AF,AX,AL,DZ,AS,AD,AO,AI,AG,AR,AM,AW,AC,AU,AT,AZ,BS,BH,BD,BB,BY,BE,BZ,BJ,BM,BT,BO,BA,BW,BR,IO,VG,BN,BG,BF,BI,KH,CM,CA,CV,BQ,KY,CF,TD,CL,CN,CX,CC,CO,KM,CD,CG,CK,CR,HR,CU,CW,CY,CZ,CI,DK,DJ,DM,DO,EC,EG,SV,GQ,ER,EE,ET,FK,FO,FJ,FI,FR,GF,PF,GA,GM,GE,DE,GH,GI,GR,GL,GD,GP,GU,GT,GG,GN,GW,GY,HT,HN,HK,HU,IS,IN,ID,IR,IQ,IE,IM,IL,IT,JM,JP,JE,JO,KZ,KE,KI,KW,KG,LA,LV,LB,LS,LR,LY,LI,LT,LU,MO,MK,MG,MW,MY,MV,ML,MT,MH,MQ,MR,MU,YT,MX,FM,MD,MC,MN,ME,MS,MA,MZ,MM,NA,NR,NP,NL,NC,NZ,NI,NE,NG,NU,NF,KP,MP,NO,OM,PK,PW,PS,PA,PG,PY,PE,PH,PL,PT,PR,QA,RO,RU,RW,RE,BL,SH,KN,LC,MF,PM,VC,WS,SM,SA,SN,RS,SC,SL,SG,SX,SK,SI,SB,SO,ZA,KR,SS,ES,LK,SD,SR,SJ,SZ,SE,CH,SY,ST,TW,TJ,TZ,TH,TL,TG,TK,TO,TT,TA,TN,TR,TM,TC,TV,VI,UG,UA,AE,GB,US,UY,UZ,VU,VA,VE,VN,WF,EH,YE,ZM,ZW`;
    }

    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    getRandomInt(min, max) {
        return Math.floor(Math.random() * ((max - min) + 1)) + min;
    }

    httpsFromHttp(url) {
        if (url) {
            if (url.substr(0, 5) === 'http:') {
                return url.replace('http', 'https');
            }
            return url;
        }
        return null;
    }
}


export interface iUtilities {
    checkInternet: () => Promise,
    handleErrors: (error: {}, extra?: string) => void,
    showToast: (message: string, duration?: string) => void,
    hideToast: () => void,
    resizeImage: (image_metadata: {}) => {},
    initError: () => void
}
