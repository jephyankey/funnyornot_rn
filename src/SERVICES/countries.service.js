import countryCodes from './country_codes.js';

export class CountryService {

    getCountryList() {
        return countryCodes;
    }

}
