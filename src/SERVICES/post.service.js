import axios from 'axios';

import { iFileService, iUserService, iConfig, iUtilities } from '../SERVICES';


export class PostService {

    constructor(config: iConfig, utilities: iUtilities, userService: iUserService, fileService: iFileService, contactService = null) {
        this.userService = userService;
        this.config = config;
        this.utilities = utilities;
        this.fileService = fileService;
        this.contactService = contactService;
    }

    setAsFunnyOrNot(post_id: number, stars: number, poster_id: number) {
        this.userService.getCurrentUsersId().then(
            id => {
                axios.post(`${this.config.getBaseUrl()}/setAsFunny`, {
                    user_id: id,
                    post_id,
                    stars,
                    poster_id
                });
            });
    }

    blockPost(post_id: number) {
        this.utilities.showToast('Blocking...');
        this.userService.getCurrentUsersId().then(
            id => {
                axios.post(`${this.config.getBaseUrl()}/blockPost`, {
                    user_id: id,
                    post_id
                })
                    .then(() => this.utilities.showToast('Post Blocked'));
            })
            .catch(() => this.utilities.showToast('Check Internet Connection'));
        //we are making a dangerous assumption above that if we are unable 
        //to block, the only cause is lack of internet....
    }


    addToFavourites(post_id: number) {
        return new Promise((resolve, reject) => {
            this.userService.getCurrentUsersId()
                .then(user_id => {
                    axios.post(`${this.config.getBaseUrl()}/favourite`, {
                        user_id,
                        post_id
                    })
                        .then(() => resolve())
                        .catch(() => reject());
                })
                .catch(() => reject());
        });
    }


    removeFromFavourites(post_id: number) {
        return new Promise((resolve, reject) => {
            this.userService.getCurrentUsersId()
                .then(user_id => {
                    axios.get(`${this.config.getBaseUrl()}/favourite/delete/${user_id}/${post_id}`)
                        .then(() => resolve())
                        .catch(err => reject(err));
                })
                .catch(() => reject());
        });
    }


    reportAbuse(post_id: number) {
        this.utilities.showToast('Reporting...');
        this.userService.getCurrentUsersId().then(
            reporter_id => {
                axios.post(`${this.config.getBaseUrl()}/abuseReport/${post_id}/${reporter_id}`)
                    .then(
                    response => {
                        if (response.data.message === 'already done')
                            this.utilities.showToast("You've already reported this");
                        else
                            this.utilities.showToast('Report submitted');
                    });
            })
            .catch(() => this.utilities.showToast('Check Internet Connection'));
        //assumption here is that, if it fails, then there was no connection.....???
        //a bit dangerous????
    }


    resetAllBlockedPosts() {
        return new Promise((resolve, reject) => {
            this.userService.getCurrentUsersId().then(user_id => {
                axios.get(`${this.config.getBaseUrl()}/blockReset/${user_id}`)
                    .then(
                    () => resolve(),
                    err => reject(err)
                    );
            });
        });
    }


    submitComment(poster_id, parent_id, comment) {
        return new Promise((resolve, reject) => {
            axios.post(`${this.config.getBaseUrl()}/comments`, {
                parent_id,
                poster_id,
                comment: encodeURIComponent(comment)
            })
                .then(response => resolve(response.data.message))
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject();
                });
        });
    }

    //save a new post to backend
    saveNewItem(feed_item: any) {
        let user_id;
        // var username;
        return new Promise((resolve, reject) => {
            if (feed_item.media_url) {
                this.userService.getCurrentUsersId()
                    .then(id => {
                        user_id = id;
                        return this.userService.getCurrentUsersName();
                    })
                    .then(username => {
                        let m_t = null;
                        if (feed_item.media_type === 'i')
                            m_t = 1;
                        else if (feed_item.media_type === 'v')
                            m_t = 2;
                        else if (feed_item.media_type === 'a')
                            m_t = 3;
                        else
                            reject('NO VALID MEDIA TYPE');
                        const filename = `${Math.floor(Date.now() / 1000)}_${user_id}_${username}`;
                        return this.fileService.upload(feed_item.media_url, m_t, filename, []);
                    })
                    .then(resp => {
                        return this.postPromise(feed_item, resp.url, resp.filename, resp.poster_url);
                    })
                    .then(() => resolve())
                    .catch(() => reject());
            }
            else
                this.postPromise(feed_item)
                    .then(() => resolve())
                    .catch(() => reject());
        });
    }
    //promise to send new post to server (helper used in saveNewItem() above)
    postPromise(feed_item, uploaded_media_url = null, filename = null, poster_image_data = null) {
        return new Promise((resolve, reject) => {
            const f = feed_item;
            //send the new post to backend to save
            this.userService.getCurrentUsersId()
                .then(user_id => {
                    const url = `${this.config.getBaseUrl()}/post/${user_id}`;
                    axios.post(url, {
                        post_text: encodeURIComponent(f.post_text),
                        media_type: f.media_type,
                        media_url: uploaded_media_url,
                        filename,
                        metadata: feed_item.metadata,
                        poster_image: poster_image_data
                    })
                        .then(() => resolve())
                        .catch(() => reject());
                });
        });
    }


    sendNotification() {
        this.contactService.getContactIds()
            .then(all_ids => {
                this.userService.getCurrentUsersId()
                    .then(sender_id => {
                        const ids = all_ids || 'xxx';  // 'xxx is just an arbitrary value, to be used if all_ids is null'
                        const url = `${this.config.getBaseUrl()}/notifications/${sender_id}/${ids}`;
                        axios.get(url);
                    });
            });
    }
}



export interface iPostService {
    setAsFunnyOrNot: (post_id: number, stars: number) => void,
    blockPost: (post_id: number) => void,
    addToFavourites: (post_id: number) => Promise,
    removeFromFavourites: (post_id: number) => Promise,
    reportAbuse: (post_id: number) => void,
    resetAllBlockedPosts: () => Promise,
    tagPostsOfBlockedPerson: (feed: Array, blocked_id: number) => void,
    untagPostsOfUnblockedPerson: (feed: Array, blocked_id: number) => void,
    saveNewItem: (feed_item: any) => Promise,
    // postPromise: (feed_item: {}, uploaded_media_url: string, filename: string, poster_image_data: string) => Promise;
}
