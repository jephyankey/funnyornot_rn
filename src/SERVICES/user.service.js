import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { EventEmitter } from './EventEmitter';
import {
    //settings
    AFFILIATE_COUNTRIES, USER_COUNTRY,
    USER_ID, USER_NAME,
    //events or actions
    BLOCK_ACTION, FOLLOW_ACTION, BLOCKED, UNBLOCKED,
    FOLLOWED, UNFOLLOWED,
    //errors
    NO_INTERNET, NO_INTERNET2, LOAD_FAIL
} from '../types';


export class UserService {

    constructor(config, utilities, dbService) {
        this.config = config;
        this.utilities = utilities;
        this.dbService = dbService;
        this.USER_ID = null;
        this.USER_NAME = null;
        this.AFFILIATE_COUNTRIES = null;
        this.COUNTRY_CODE = null;
    }

    createUser(name, phone, country_code) {
        return new Promise((resolve, reject) => {
            axios.post(`${this.config.getBaseUrl()}/user/`, {
                username: name,
                phone_number: phone,
                country_SN: country_code
            })
                .then(response => {
                    resolve(response.data.data);
                })
                .catch(err => {
                    this.utilities.handleErrors(err, 'ERROR CREATING NEW USER');
                    reject(err);
                });
        });
    }

    getCurrentUsersId() {
        return new Promise((resolve, reject) => {
            if (this.USER_ID) {
                resolve(this.USER_ID);
            }
            else {
                AsyncStorage.getItem(USER_ID).then(
                    data => {
                        this.USER_ID = data;
                        resolve(data);
                    }, err => reject(err));
            }
        });
    }

    getCurrentUsersName() {
        return new Promise((resolve, reject) => {
            if (this.USER_NAME) {
                resolve(this.USER_NAME);
            }
            else {
                AsyncStorage.getItem(USER_NAME).then(
                    data => {
                        this.USER_NAME = data;
                        resolve(data);
                    }, err => reject(err));
            }
        });
    }

    /**
     * returns a list of coutnry codes the current user is affiliated to
     */
    getCurUsersAffiliateCountries() {
        return new Promise((resolve, reject) => {
            if (this.AFFILIATE_COUNTRIES)
                resolve(this.AFFILIATE_COUNTRIES);
            else {
                AsyncStorage.getItem(AFFILIATE_COUNTRIES).then(
                    data => {
                        this.AFFILIATE_COUNTRIES = data;
                        resolve(data);
                    }, err => {
                        this.utilities.handleErrors('COULD NOT FETCH AFFILIATE COUNTRIES', err);
                        reject(err);
                    });
            }
        });
    }

    getCurrentUsersCountCode() {
        return new Promise((resolve, reject) => {
            if (this.COUNTRY_CODE)
                resolve(this.COUNTRY_CODE);
            else {
                AsyncStorage.getItem(USER_COUNTRY).then(
                    data => {
                        this.COUNTRY_CODE = data;
                        resolve(data);
                    }, err => reject(err));
            }
        });
    }

    //TODO cleanup this is no longer in use
    setUserId(user_id) {
        return new Promise((resolve, reject) => {
            //stored as xxx.0 in sqlite without toString() i dnt kno y
            AsyncStorage.setItem(USER_ID, user_id.toString())
                .then(() => resolve(),
                err => reject(err));
        });
    }

    setUserName(username) {
        return new Promise((resolve, reject) => {
            AsyncStorage.setItem(USER_NAME, username).then(
                () => resolve(),
                err => reject(err));
        });
    }

    /**
     * check if the currrent user is 
     * following the user whos id is passed in as followed
     */
    isFollowing(followed) {
        return new Promise((resolve, reject) => {
            this.dbService.query('SELECT user_id FROM following WHERE user_id = ?', [followed])
                .then(data => {
                    if (data[0])
                        resolve(true);
                    else
                        resolve(false);
                },
                err => {
                    this.utilities.handleErrors(err);
                    reject(err);
                });
        });
    }

    isBlocked(who) {
        return new Promise((resolve, reject) => {
            this.dbService.query('SELECT user_id FROM blocked WHERE user_id = ?', [who])
                .then(data => {
                    if (data[0])
                        resolve(true);
                    else
                        resolve(false);
                },
                err => {
                    this.utilities.handleErrors(err);
                    reject(err);
                });
        });
    }


    follow(followed: string, user_name: string, avatar_url: string) {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId()
                .then(user_id => {
                    const url = `${this.config.getBaseUrl()}/follow/${user_id}/${followed}`;
                    axios.post(url)
                        .then(response => {
                            if (response.data.message === 'success') {
                                /**
                                 * dispatch a new event saying we are now following this user,
                                 * on the following page for eg, this will be used to add the new user to the list
                                 */
                                EventEmitter.dispatch(FOLLOW_ACTION, {
                                    user_id: followed,
                                    state: FOLLOWED,
                                    user_name,
                                    avatar_url
                                });
                                resolve();
                            }
                        })
                        .catch(err => {
                            this.utilities.handleErrors('ERROR IN FOLLOW USER FUNCTION', err);
                            reject(err);
                        });
                });
        });
    }


    unfollow(followed: string, user_name: string, avatar_url: string) {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId().then(
                user_id => {
                    const url = `${this.config.getBaseUrl()}/unfollow/${user_id}/${followed}`;
                    axios.post(url)
                        .then(response => {
                            if (response.data.message === 'success') {
                                /**
                                 * dispatch a new event saying we are now following this user,
                                 * on the following page for eg, this will be used to add the new user to the list
                                 */
                                EventEmitter.dispatch(FOLLOW_ACTION, {
                                    user_id: followed,
                                    state: UNFOLLOWED,
                                    user_name,
                                    avatar_url
                                });
                                resolve();
                            }
                        })
                        .catch(err => {
                            this.utilities.handleErrors('ERROR IN UNFOLLOW USER FUNCTION', err);
                            reject(err);
                        });
                });
        });
    }

    block(user_id, user_name = '') {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId().then(
                id => { //id of user currently using the app
                    const url = `${this.config.getBaseUrl()}/blockUser/${id}/${user_id}`;
                    axios.post(url)
                        .then(response => {
                            if (response.data.message === 'success') {
                                const sql = 'INSERT INTO blocked (user_id, user_name) VALUES (?, ?)';
                                this.dbService.query(sql, [user_id, user_name])
                                    .then(() => {
                                        //tell observers this user is blocked, used on the contacts/following pages 
                                        //to auto remove the user from the list
                                        EventEmitter.dispatch(BLOCK_ACTION, { user_id, state: BLOCKED });
                                        resolve();
                                    }).catch(err => {
                                        this.utilities.handleErrors(err);
                                        reject(err);
                                    });
                            }
                        })
                        .catch(() => reject(NO_INTERNET));
                });
        });
    }


    unblock(user_id) {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId()
                .then(id => {
                    const url = `${this.config.getBaseUrl()}/unblockUser/${id}/${user_id}`;
                    axios.post(url)
                        .then(response => {
                            if (response.data.message === 'success') {
                                const query = 'DELETE FROM blocked WHERE user_id = ?';
                                this.dbService.query(query, [user_id])
                                    .then(() => {
                                        resolve();
                                        //publish unblocked event to all subscribers.
                                        EventEmitter.dispatch(BLOCK_ACTION, { user_id, state: UNBLOCKED });
                                    }).catch(() => reject());
                            }
                        })
                        .catch(err => {
                            this.utilities.handleErrors(err);
                            reject();
                        });
                })
                .catch(() => reject());
        });
    }



    /**
     * fetches fun factor, no of folowers, no following
     * if the viewer_id is specified, then we know someone is viewing someone else's profie
     * so the info returned includes whether the viewer is following the profile owner or not
     */
    fetchReleventProfileData(user_id, viewer_id = null) {
        let url = `${this.config.getBaseUrl()}/profileData/${user_id}`;
        if (viewer_id)
            url = `${this.config.getBaseUrl()}/profileData/${user_id}/${viewer_id}`;
        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(response => {
                    resolve(response.data.data);
                })
                .catch(error => {
                    if (error.response)
                        reject(LOAD_FAIL);
                    else
                        reject(NO_INTERNET);
                });
        });
    }

    /**
     * gets a list of ppl the current user is following, from the SERVER
     * if you are following someone u have blocked, he will not appear in this list
     */
    getFollowingList(mode = 'following') {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId().then(
                user_id => {
                    let url = `${this.config.getBaseUrl()}/followers/${user_id}`;
                    if (mode === 'following')
                        url = `${this.config.getBaseUrl()}/following/${user_id}`;

                    axios.get(url)
                        .then(response => {
                            resolve(response.data.data);
                        })
                        .catch(() => reject(NO_INTERNET2));
                });
        });
    }


    routeSettingToServer(setting_name, value) {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId().then(
                user_id => {
                    const url = `${this.config.getBaseUrl()}/updateSetting/${setting_name}/${user_id}/${value}`;
                    axios.post(url)
                        .then(response => {
                            if (response.data.message === 'success')
                                resolve();
                            else
                                reject();
                        })
                        .catch(err => {
                            this.utilities.handleErrors(err);
                            reject(err);
                        });
                });
        });
    }


    updateUserName(username) {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId().then(
                user_id => {
                    const url = `${this.config.getBaseUrl()}/userName/${user_id}/${username}`;
                    axios.post(url)
                        .then(response => {
                            if (response.data.message === 'success') {
                                this.setUserName(username).then(this.USER_NAME = username);
                            }
                            resolve(response.data.message);
                        })
                        .catch(err => reject(err));
                });
        });
    }

    /**
     * fetch the name of the poster as saved by the current user in their contact list
     * this is cached in the sqlite dbase 
     * a second parameter is passed, primarily because of the way this fx 
     * is used in promises within the updateFeedParams fx
     * -----------
     * remember that in contacts there are also users with random ids 
     * (auto generated wit no significance)
     * who are not on the app, hence. on_app = 1
     * -------------
     * we are using reposter_id and not poster_id, in the case of an original post, 
     * it will make no difference
     * but for a repost, the reposter_id is the one who will be 
     * in ur contacts (or who u will be following)
     * so to summarize, for an OP, feed_item.name has the name of the OP
     * but for a repost, feed_item.name has the name of the reposter, 
     * and the feed_item.username will ofcourse have
     * the name of the OP. make sense???
     */
    getProperName(feed_item, i) {
        return new Promise((resolve, reject) => {
            this.getCurrentUsersId().then(
                user_id => {
                    //
                    const reposter_id = feed_item.reposter_id || feed_item.poster_id;
                    const reposter_name = feed_item.reposter_name || feed_item.username;
                    /**
                     * we do the above cos this same component is used to render favourites
                     * however, for favourites the data loaded from the server does not include reposter_id
                     * and reposter_name, this is fine, since a repost itself cannot be favourited, only 
                     * the original post
                     */

                    //does the post belong to the current user???
                    //REMEMBER TO KEEP == (double comparison operator) NOT TRIPLE IN THIS case
                    //OTHERWISE U'LL BE SORRY'
                    if (user_id == reposter_id) {
                        resolve(['You', i]);
                    }
                    else {
                        const sql = 'SELECT name FROM contacts WHERE user_id = ? AND on_app = 1';
                        this.dbService.query(sql, [reposter_id]).then(
                            data => {
                                //REMEMBER TO KEEP == (double comparison operator) NOT TRIPLE IN THIS case
                                //OTHERWISE U'LL BE SORRY'
                                if (data.length == 0) //poster not in contacts (someone u r following) 
                                    resolve([`@${reposter_name}`, i]);
                                else //poster in contacts, use contact name
                                    resolve([data[0].name, i]);
                            }, err => reject(err));
                    }
                }, err => reject(err));
        });
    }


    /**
    * updates name of each post
    */
    updateFeedWithContactNames(feed_array): any {
        let sequence = Promise.resolve(0);
        for (let i = 0; i < feed_array.length; i++) {
            //create a sequence of promises to get correct name and avatar of poster of each post
            sequence = sequence.then((num) => {
                return this.getProperName(feed_array[num], num);
            })
                .then((data) => {
                    const num = data[1];
                    feed_array[num].name = data[0]; //1. SET THE NAME
                    if (num == feed_array.length - 1) {
                        return (feed_array);
                    }
                    return num + 1;
                });
        }
        return sequence;
    }

    /**
     * updates the users country on server side and then lcally
     */
    updateUsersCountry(country_code, user_id) {
        return new Promise((resolve, reject) => {
            axios.post(`${this.config.getBaseUrl()}/usersCountry`, { country_code, user_id })
                .then(() => { return AsyncStorage.setItem(USER_COUNTRY, country_code); })
                .then(() => resolve())
                .catch(() => reject());
        });
    }

}


export interface iUserService {
    getCurrentUsersId: () => Promise,
    getCurUsersAffiliateCountries: () => Promise,
    getCurrentUsersCountCode: () => Promise,
    setUserId: (user_id: number) => Promise,
    setUserName: (username: string) => Promise,
    isFollowing: (id: number) => Promise,
    isBlocked: (who: number) => Promise,
    follow: (followed: string, user_name: string, avatar_url: string) => Promise,
    unfollow: (followed: string, user_name: string, avatar_url: string) => Promise,
    block: (user_id: string, user_name?: string) => Promise,
    fetchReleventProfileData: (profile_id: string) => Promise,
}
