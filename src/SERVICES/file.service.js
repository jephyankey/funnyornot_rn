import { NativeModules, Platform } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { iConfig } from './config';
import { iUtilities } from './utilities.service';


export class FileService {
    media_cache_size: number = 20;


    constructor(config: iConfig, utilities: iUtilities) {
        this.config = config;
        this.utilities = utilities;
    }

    //create requisite app folders the first time the app is run
    initializeAppFolders() {
        //create funnyornot folder and subfolders
        return new Promise((resolve, reject) => {
            NativeModules.MyFileManager.isStorageAvailable()
                .then(() => {
                    NativeModules.MyFileManager.getOrCreateFunnyFolder()
                        .then(() => resolve())
                        .catch(err => this.utilities.handleErrors(err));
                })
                .catch(err => {
                    this.utilities.handleErrors(err);
                    reject();
                });
        });
    }

    /**
     * function that ensures that our cache for media files does not get too big,
     * for now we cache a maximum of 20 files
     */
    cleanUpMediaCache() {
        return new Promise((resolve, reject) => {
            NativeModules.MyFileManager.cleanMediaCache()
                .then(() => resolve())
                .catch(err => reject(err));
        });
    }


    /**
     * checks if a media file exists locally, if so returns it (in a promise),
     * otherwise, it downloads it and returns the the file path (in a promise)
     * media file can be a user's avatar, or any of an image, video or audio
    */
    getMediaFile(media_url, filename, filetype = 'VIDEO') {
        return new Promise((resolve, reject) => {
            NativeModules.MyFileManager.asyncDownload(media_url, filename, filetype)
                .then(url => resolve(url))
                .catch(err => {
                    reject(err);
                    this.utilities.handleErrors(err, 'DOWNLOADING FILE FAILED. VIDEO WILL ATTEMPT TO PLAY FROM SERVER');
                });
        });
    }


    fileExistsOnDisk(filename) {
        return new Promise((resolve) => {
            NativeModules.MyFileManager.fileExists(filename)
                .then(response => {
                    if (response === 'DOES-NOT-EXIST')
                        resolve('FALSE');
                    else
                        resolve(response);
                })
                .catch(() => {
                    resolve('FALSE');
                });
        });
    }


    openGallery(mode) {
        //mode: 1 = images, 2 = videos
        return new Promise((resolve, reject) => {
            if (Platform.OS === 'android') {
                NativeModules.MyMediaPicker.openGallery(mode,
                    (uri_data) => { resolve(uri_data); },
                    (error) => {
                        if (error !== 'CANCELLED')
                            this.utilities.handleErrors(error, `ERROR GETTING FILE FROM GALLERY, mode is ${mode}`);
                        reject(error);
                    });
            }
            else {
                //for ios
                let config = { title: 'Select an Image', noData: true, takePhotoButtonTitle: 'Take a Picture', mediaType: 'photo' };
                if (mode === 2)
                    config = { title: 'Select a Video', takePhotoButtonTitle: null, mediaType: 'video' };
                ImagePicker.showImagePicker(
                    config,
                    (response) => {
                        if (response.didCancel) {
                            reject('CANCELLED');
                        }
                        else if (response.error) {
                            this.utilities.handleErrors(response.error, `ERROR GETTING FILE FROM GALLERY, mode is ${mode}`);
                            reject(response.error);
                        }
                        else {
                            resolve(response);
                        }
                    });
            }
        });
    }

    /**
     * extra fields is an array of arrays
     * e.g. [["is_avatar": true], ["user_id": 200]]
     */
    upload(uri: string, media_type: number, filename: string, extra_fields: Array = []) {
        return new Promise((resolve, reject) => {
            if (Platform.OS === 'android') {
                NativeModules.MyUpload.uploadFile(uri, media_type, filename, extra_fields)
                    .then(response => {
                        resolve(JSON.parse(response));
                    })
                    .catch(err => {
                        this.utilities.handleErrors(err, `ERROR WITH FILE UPLOAD, mediaType is ${media_type}`);
                        reject(err);
                    });
            }
            else { //ios
                const photo = {
                    uri,
                    name: 'file',
                };
                const body = new FormData();
                body.append('file', photo);
                body.append('filename', filename);
                for (let i = 0; i < extra_fields.length; i++) {
                    body.append(extra_fields[i][0], extra_fields[i][1]);
                }

                fetch(
                    `${this.config.getBaseUrl()}/upload`,
                    {
                        method: 'POST',
                        headers: { 'Content-Type': 'multipart/form-data' },
                        body
                    })
                    .then((res) => res.json())
                    .then(res => resolve(res))
                    .catch((err) => {
                        this.utilities.handleErrors(err, `ERROR WITH FILE UPLOAD, mediaType is ${media_type}`);
                        reject(err);
                    });
            }
        });
    }
}


export interface iFileService {
    initializeAppFolders: () => Promise;
    cleanUpMediaCache: () => Promise;
    getMediaFile: (media_url: string, filename: string, filetype?: '') => Promise;
    upload: (uri: string, media_type: string, filename: string, extra_fields: Array) => Promise;
}
