
/**
 * a simple class that implements an interface to allow
 * components to communicate through a subscribe/dispatch system
 */
export const EventEmitter = (function () {
    const events = {};

    return {
        dispatch: (event, data) => {
            if (!events[event]) return; // no one is listening to this event
            for (let i = 0; i < events[event].length; i++)
                events[event][i].func(data);
        },

        subscribe: (event, callback) => {
            if (!events[event]) events[event] = []; // new event
            const event_id = Math.random();
            events[event].push({ id: event_id, func: callback });
            return event_id;
        },

        unsubscribe: (event, id) => {
            if (!events[event]) {
                return;
            }
            const e = events[event];
            for (let i = 0; i < e.length; i++) {
                if (e[i].id === id) {
                    e.splice(i, 1);
                    return;
                }
            }
        }
    };
})();
