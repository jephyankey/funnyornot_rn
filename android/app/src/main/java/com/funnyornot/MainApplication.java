package com.funnyornot;

import android.app.Application;
import android.util.Log;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

//from npm....
import com.brentvatne.react.ReactVideoPackage;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.microsoft.codepush.react.CodePush;
import org.pgsqlite.SQLitePluginPackage;
import com.remobile.toast.RCTToastPackage; 

//self defined
import com.funnyornot.mycontacts.*;
import com.funnyornot.myfilemanager.*;
import com.funnyornot.mymediapicker.*;
import com.funnyornot.myupload.*;
import com.funnyornot.videoplayerview.*;
import com.funnyornot.mysharer.*;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
          new ReactVideoPackage(),
          new ReactNativeOneSignalPackage(),
          new CodePush("0CxBJqIsxCyL3-SqmcNG2fOwnorwNJMvr2ZIM", MainApplication.this, BuildConfig.DEBUG),
          new SQLitePluginPackage(),
          new RCTToastPackage(),
          //+++++++++++++++++++++++++
          new MyContactsPackage(),
          new MyFileManagerPackage(),
          new MyMediaPickerPackage(),
          new MyUploadPackage(),
          new MySharerPackage(),
          new VideoPlayerPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
