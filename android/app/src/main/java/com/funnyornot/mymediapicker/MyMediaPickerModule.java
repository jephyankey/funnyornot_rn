package com.funnyornot.mymediapicker;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;

import android.content.Intent;
import android.app.Activity;
import java.io.File;
import java.io.InputStream;
import java.lang.Integer;
import java.lang.Long;
import java.io.FileNotFoundException;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.MediaStore;

import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.Manifest;
import android.content.pm.PackageManager;

import android.os.ParcelFileDescriptor;
import android.util.Log;


public class MyMediaPickerModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private static final int PICK_IMAGE = 1;
    private ReactApplicationContext appContext;
    private int galleryMode;

    private Callback pickerSuccessCallback;
    private Callback pickerErrorCallback;

    private Uri selectedFileUri;
    static final int REQUEST_CODE = 1234;
    static final int READ_SD_CARD_PERMISSION = 9999;

    public MyMediaPickerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(this);
        appContext = reactContext;
    }

    @Override
    public String getName() {
        return "MyMediaPicker";
    }

    @ReactMethod
    public void openGallery(int mode, Callback successCallback, Callback errorCallback) {
        Activity currentActivity = getCurrentActivity();
        galleryMode = mode;
        long fileSize;

        if (currentActivity == null) {
            successCallback.invoke("CANCELLED");
            return;
        }
        pickerSuccessCallback = successCallback;
        pickerErrorCallback = errorCallback;

        try {

            final Intent filePickerIntent = new Intent();
            String intentTitle = "";
            filePickerIntent.setAction(Intent.ACTION_GET_CONTENT);
            //only access files that are locally on the device
            filePickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            //only access files that can be opened (ContentResolver.openFileDescriptor(Uri uri, String mode) can be called on the uri)
            filePickerIntent.addCategory(Intent.CATEGORY_OPENABLE);

            if (mode == 1) {
                filePickerIntent.setType("image/*");
                intentTitle = "Pick an image";
            } else if (mode == 3) {
                filePickerIntent.setType("audio/*");
                intentTitle = "Pick an audio";
            } else if (mode == 2) {
                filePickerIntent.setType("video/*");
                intentTitle = "Pick a video";
            } else {
                pickerErrorCallback.invoke(mode + "NO VALID MODE SUPPLIED");
            }

            currentActivity.startActivityForResult(Intent.createChooser(filePickerIntent, intentTitle), 909);

        } catch (Exception e) {
            pickerErrorCallback.invoke(e.toString());
        }

    }

    @Override
    public void onActivityResult(Activity activity, final int requestCode, final int resultCode, final Intent intent) {
        if (pickerSuccessCallback != null) {
            if (resultCode == activity.RESULT_CANCELED) {
                pickerErrorCallback.invoke("CANCELLED");
            } else if (resultCode == activity.RESULT_OK) {
                Uri uri = intent.getData();
                selectedFileUri = uri;

                if (uri == null) {
                    pickerErrorCallback.invoke("NULL-URL");
                } else {
                    try {
                        if (ContextCompat.checkSelfPermission(appContext,
                                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                            // ActivityCompat.requestPermissions(activity,
                            //         new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                            //         READ_SD_CARD_PERMISSION);
                            pickerErrorCallback.invoke("PERMISSION-DENIED");

                        } else {
                            if (galleryMode == 1) { //image
                                getImageDimension(uri, appContext);
                            } else if (galleryMode == 2) {
                                getVideoDimension(uri, appContext);
                            } else {
                                WritableMap data = Arguments.createMap();
                                data.putString("uri", selectedFileUri.toString());
                                data.putString("size", Long.toString(getFileSize(selectedFileUri)));
                                pickerSuccessCallback.invoke(data);
                            }
                        }
                    } catch (Exception e) {
                        pickerErrorCallback.invoke(e.toString());
                    }
                }
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
    }

    // @Override
    // public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
    //         @NonNull int[] grantResults) {
    //     switch (requestCode) {
    //     case READ_SD_CARD_PERMISSION: {
    //         // If request is cancelled, the result arrays are empty.
    //         if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
    //             //permission was granted
    //             getImageDimension(selectedFileUri, appContext);
    //         } else {
    //             pickerErrorCallback.invoke("PERMISSION-DENIED");
    //         }
    //         return;
    //     }
    //     }
    // }

    public void getImageDimension(Uri imageUri, Context context) {
        int dimensions[] = null;
        try {

            InputStream imageStream = appContext.getContentResolver().openInputStream(imageUri); //throws exception if uri is invalid
            Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
            dimensions = new int[] { bitmap.getWidth(), bitmap.getHeight() };
            bitmap.recycle();
            imageStream.close();

            //write data to a react native writeablemap and return to javascript
            WritableMap data = Arguments.createMap();
            data.putString("width", Integer.toString(dimensions[0]));
            data.putString("height", Integer.toString(dimensions[1]));
            data.putString("size", Long.toString(getFileSize(imageUri)));
            data.putString("uri", imageUri.toString());

            pickerSuccessCallback.invoke(data);

        } catch (Exception e) {
            pickerErrorCallback.invoke(e.toString());
        }
    }

    public void getVideoDimension(Uri videoUri, Context context) {
        int dimensions[] = null;
        try {
            Bitmap bitmap = getVideoFrame(videoUri, appContext);
            if (bitmap == null) {
                pickerErrorCallback.invoke("ERROR GETTING VIDEO FRAME");
            } else {
                dimensions = new int[] { bitmap.getWidth(), bitmap.getHeight() };
                bitmap.recycle();

                //write data to a react native writeablemap and return to javascript
                WritableMap data = Arguments.createMap();
                data.putString("width", Integer.toString(dimensions[0]));
                data.putString("height", Integer.toString(dimensions[1]));
                data.putString("size", Long.toString(getFileSize(videoUri)));
                data.putString("uri", videoUri.toString());

                pickerSuccessCallback.invoke(data);
            }
        } catch (Exception e) {
            pickerErrorCallback.invoke(e.toString());
        }
    }

    //returns a bitmap which is the frame retrieved at 3 seconds into the video play
    public static Bitmap getVideoFrame(Uri videoUri, Context context) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = context.getContentResolver().openFileDescriptor(videoUri, "r"); //'r' means READ ONLY
            mediaMetadataRetriever.setDataSource(parcelFD.getFileDescriptor());
        } catch (FileNotFoundException fex) {
            return null;
        } catch (IllegalArgumentException ille) {
            return null;
        } finally {
            try {
                parcelFD.close();
            } catch (Exception exc) {
            }
        }

        Bitmap frame = mediaMetadataRetriever.getFrameAtTime(3);
        mediaMetadataRetriever.release();

        return frame;
    }

    //return file size in KB
    public long getFileSize(Uri uri) {
        File file = new File(uri.getPath());
        long size = file.length();
        return size;
    }

}