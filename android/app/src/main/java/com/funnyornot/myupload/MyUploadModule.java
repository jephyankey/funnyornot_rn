package com.funnyornot.myupload;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
// import com.facebook.react.bridge.WritableMap;
// import com.facebook.react.bridge.Arguments;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.util.Base64;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ResponseCache;
import java.net.URL;
import java.util.Map;

import android.os.Handler;
import android.os.Message;
// import android.util.Log;
import java.lang.Thread;
import java.lang.Runnable;
import java.lang.Integer;
import java.lang.Math;

import android.os.Message;
import android.os.Bundle;

import com.funnyornot.mymediapicker.MyMediaPickerModule;

public class MyUploadModule extends ReactContextBaseJavaModule {

    private static final String uploadUrl = "https://2-dot-funnyornot-backend-1412.appspot.com/upload";
    private ReactApplicationContext appContext;

    public MyUploadModule(ReactApplicationContext reactContext) {
        super(reactContext);
        appContext = reactContext;
    }

    @Override
    public String getName() {
        return "MyUpload";
    }

    @ReactMethod
    public void uploadFile(final String fileUriAsEncodedString, final int mediaType, final String filename,
            final ReadableArray extraFields, final Promise promise) {

        // Create Inner Thread Class
        Thread background = new Thread(new Runnable() {
            // After call for background.start this run method call
            //see: http://androidexample.com/Thread_With_Handlers_-_Android_Example/index.php?view=article_discription&aid=58
            // and see: http://www.techotopia.com/index.php/A_Basic_Overview_of_Android_Threads_and_Thread_handlers
            // and http://stackoverflow.com/questions/13954611/android-when-should-i-use-a-handler-and-when-should-i-use-a-thread
            public void run() {
                // FileUploadResponse serverResponse = new FileUploadResponse(false);
                String serverResponse = null;
                Uri fileUri = Uri.parse(fileUriAsEncodedString);

                HttpURLConnection connection = null;
                DataOutputStream outputStream = null;
                DataInputStream responseStream = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";

                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;

                String base64Data;

                try {
                    // FileInputStream fileInputStream = new FileInputStream(new File(filePath));
                    InputStream fileInputStream = appContext.getContentResolver().openInputStream(fileUri);
                    String contentType = appContext.getContentResolver().getType(fileUri);
                    if (contentType == null)
                        //if we are unable to get the contentType from above, then we set it to
                        //an arbitrary binary file
                        contentType = "application/octet-stream";

                    URL url = new URL(uploadUrl);
                    connection = (HttpURLConnection) url.openConnection();

                    // Allow Inputs & Outputs.
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);

                    // Set HTTP method to POST.
                    connection.setRequestMethod("POST");

                    //set the headers of the request
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    //write the file to the output stream
                    outputStream = new DataOutputStream(connection.getOutputStream());
                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    outputStream.writeBytes(
                            "Content-Disposition: form-data; name=\"file\";filename=\"" + filename + "\"" + lineEnd);
                    outputStream.writeBytes("Content-Type: " + contentType + lineEnd);
                    outputStream.writeBytes(lineEnd);

                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // Read file
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        outputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }
                    outputStream.writeBytes(lineEnd + lineEnd);

                    //add the filename field. 'filename' field is common to all upload requests
                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    outputStream.writeBytes("Content-Disposition: form-data; name=\"filename\"" + lineEnd + lineEnd
                            + filename + lineEnd);

                    //FOR VIDEOS, WE WANT TO RETRIEVE A FRAME AS WELL
                    if (mediaType == 2) {
                        Bitmap posterImage = MyMediaPickerModule.getVideoFrame(fileUri, appContext);
                        //write poster_image too if available
                        if (posterImage != null) {
                            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                            outputStream.writeBytes("Content-Disposition: form-data; name=\"poster_image_file\";filename=\""
                                            + filename + "\"" + lineEnd);
                            outputStream.writeBytes("Content-Type: image/png" + lineEnd + lineEnd);

                            //convert Bitmap to InputStream
                            ByteArrayOutputStream os = new ByteArrayOutputStream();
                            posterImage.compress(Bitmap.CompressFormat.JPEG, 50, os);
                            InputStream posterImageStream = new ByteArrayInputStream(os.toByteArray());

                            bufferSize = Math.min(posterImageStream.available(), maxBufferSize);
                            buffer = new byte[bufferSize];
                            // Read image
                            bytesRead = posterImageStream.read(buffer, 0, bufferSize);
                            while (bytesRead > 0) {
                                outputStream.write(buffer, 0, bufferSize);
                                bufferSize = Math.min(posterImageStream.available(), maxBufferSize);
                                bytesRead = posterImageStream.read(buffer, 0, bufferSize);
                            }
                            posterImageStream.close();
                            posterImage.recycle();
                            outputStream.writeBytes(lineEnd + lineEnd);
                        }
                    }

                    //add any extra fields
                    int i;
                    for (i = 0; i < extraFields.size(); i++) {
                        ReadableArray field = extraFields.getArray(i);

                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + field.getString(0) + "\""
                                + lineEnd + lineEnd + field.getString(1) + lineEnd);
                    }

                    //write the closing boundary
                    outputStream.writeBytes(lineEnd);
                    outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    fileInputStream.close();

                    //check if no error occurred with the request            
                    int responseCode = connection.getResponseCode();
                    if (responseCode / 100 != 2) { //if the status code is not 2XX
                        threadMessage("SERVER ERROR: " + Integer.toString(responseCode), 0);
                    } else {
                        //get the response from the server as json string
                        responseStream = new DataInputStream(connection.getInputStream());
                        BufferedReader br = null;
                        StringBuilder sb = new StringBuilder();
                        String line;
                        try {
                            br = new BufferedReader(new InputStreamReader(responseStream));
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                        } catch (IOException e) {
                            // Log.e(TAG, "File Upload Response: " + e.getMessage());
                            // return serverResponse;
                            // send an error message from this thread to caller class
                            threadMessage(e.toString(), 0);
                        } finally {
                            if (br != null) {
                                try {
                                    br.close();
                                } catch (IOException e) {
                                    // Log.e(TAG, "File Upload Response: " + e.getMessage());
                                    // return serverResponse;
                                    // send an error message from this thread to caller class
                                    threadMessage(e.toString(), 0);
                                }
                            }
                        }

                        //format response to return
                        JSONObject jsonObject = new JSONObject(sb.toString());
                        String fileUrl = jsonObject.getString("url");
                        String posterUrl = null; //not always available
                        try {
                            posterUrl = jsonObject.getString("poster_url");
                        } catch (Exception e) {
                        }

                        serverResponse = "{\"response_code\": " + responseCode + ", \"filename\": \"" + filename
                                + "\", \"url\": \"" + fileUrl + "\", \"poster_url\": \"" + posterUrl + "\"}";

                        //send success message from this thread to calling class
                        threadMessage(serverResponse, 1);
                    }

                    fileInputStream.close();
                    outputStream.flush();
                    outputStream.close();
                    connection.disconnect();
                    base64Data = null;

                } catch (FileNotFoundException filex) {
                    // Log.e(TAG, "uploadFile: " + filex.getMessage());
                    // return serverResponse;
                    // send an error message from this thread to caller class
                    threadMessage(filex.toString(), 0);
                } catch (Exception ex) {
                    // Log.e(TAG, "uploadFile: " + ex.getMessage());
                    // return serverResponse;
                    // send an error message from this thread to caller class
                    threadMessage(ex.toString(), 0);
                }
            }

            private void threadMessage(String msg, int value) {
                Message msgObj = Message.obtain(handler, value);
                Bundle bundle = new Bundle();
                bundle.putString("message", msg);
                msgObj.setData(bundle);
                handler.sendMessage(msgObj);
            }

            // Define the Handler that receives messages from the thread and update the progress
            private final Handler handler = new Handler() {

                public void handleMessage(Message msg) {
                    Bundle bundle = msg.getData();
                    String message = bundle.getString("message");
                    if (msg.what == 1) {
                        promise.resolve(message);
                    } else {
                        promise.reject(message);
                    }
                }
            };

        });

        // Start Thread
        background.start(); //After call start method thread called run Method
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
}