package com.funnyornot.mycontacts;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.Arguments;

import java.util.List;
import java.util.ArrayList;

// these classes are required for reading contacts
import android.database.Cursor;
import android.provider.ContactsContract;


public class MyContactsModule extends ReactContextBaseJavaModule {

    private static final String CONTACT_NAME = "display_name";
    private ReactApplicationContext appContext;


  public MyContactsModule(ReactApplicationContext reactContext) {
    super(reactContext);
    appContext = reactContext;
  }


  @ReactMethod
  //get the list of contacts of the user from the contacts database
  public void readContacts(Promise promise) {
        WritableArray phoneContacts = Arguments.createArray();

        String[] columns = new String[]{
                CONTACT_NAME, 
                ContactsContract.CommonDataKinds.Phone.NUMBER,
        }; //the columns in the contacts db we need

        List<String> phoneNumbers = new ArrayList<>();

        try {
                Cursor phones = appContext.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, columns, null, null, CONTACT_NAME);

                if (phones != null && phones.getCount() > 0) {
                    while (phones.moveToNext()) {
                        String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).trim();

                        phoneNumber = phoneNumber.replaceAll("\\s+","");  //remove spaces

                        if (!phoneNumbers.contains(phoneNumber)) { //attempt to remove duplicates
                            phoneNumbers.add(phoneNumber);
                            String name = phones.getString(phones.getColumnIndex(CONTACT_NAME)).trim();

                            WritableMap contact = Arguments.createMap();
                            contact.putString("name", name);
                            contact.putString("phone", phoneNumber);

                            phoneContacts.pushMap(contact);
                        }
                    }
                }

                promise.resolve(phoneContacts);
        }
        catch (Exception e) {
            promise.reject(e);
        }


  }


   @Override
   public String getName() {
     return "MyContacts";
   }

}