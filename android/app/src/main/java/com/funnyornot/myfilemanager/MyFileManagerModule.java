package com.funnyornot.myfilemanager;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

import java.util.List;
import java.util.ArrayList;

import android.os.Environment;
import java.io.File;
import java.lang.String;

import android.os.Handler;
import android.os.Message;
import java.lang.Thread;
import java.lang.Runnable;

import java.net.URL;
import java.net.HttpURLConnection;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;

import android.os.Message;
import android.os.Bundle;

import java.util.Arrays;
import java.util.Comparator;
import android.util.Log;
import java.lang.Long;

import android.database.Cursor;
import android.provider.MediaStore;
import android.content.ContentValues;
import android.net.Uri;



public class MyFileManagerModule extends ReactContextBaseJavaModule {

    private static final String CONTACT_NAME = "display_name";
    private ReactApplicationContext appContext;

    private final String storageLocation = "funnyornot";
    private final int media_cache_size = 20;

    public MyFileManagerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        appContext = reactContext;
    }

    @ReactMethod
    //attempts to find the funnyornot folder
    // if it does not exist, a new one is created....
    public void getOrCreateFunnyFolder(Promise promise) {
        try {
            File f = new File(Environment.getExternalStorageDirectory(), "funnyornot");
            if (!f.exists()) {
                f.mkdirs();
            }
            f = new File(Environment.getExternalStorageDirectory(), "funnyornot/.nomedia");
            f.createNewFile();
            promise.resolve("FUNNYORNOT FOLDER CREATED SUCCESFULLY");
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    public void reCreateFunnyFolder() {
        try {
            File f = new File(Environment.getExternalStorageDirectory(), "funnyornot");
            if (!f.exists()) 
                f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), "funnyornot/.nomedia");
            if (!f.exists())
                f.createNewFile();
        } catch (Exception e) {}
    }

    @ReactMethod
    public void isStorageAvailable(Promise promise) {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
            promise.resolve("STORAGE AVAILABLE");
        else
            promise.reject("STORAGE NOT-AVAILABLE");
    }

    /**
    *checks if a media file is available on disk, if so return the local url
    otherwise download it on a background thread, then return url
    */
    @ReactMethod
    public void asyncDownload(final String media_url, final String filename, final String filetype, final Promise promise) {

        reCreateFunnyFolder();
        final String fileReference = "funnyornot/" + filename;
        String state = Environment.getExternalStorageState();

        final File file = new File(Environment.getExternalStorageDirectory(), fileReference);

        //if the file already exists on disk OR there is no available storage medium OR 
        if (file.exists()) {
            promise.resolve(file.toURI().toString());
            // promise.resolve(getVideoContentUri(file).toString());
        } else if (!Environment.MEDIA_MOUNTED.equals(state)) { //if there is no storage medium
            promise.reject("NO-STORAGE-MEDIUM");
        } else {
            // Create Inner Thread Class
            Thread background = new Thread(new Runnable() {
                // After call for background.start this run method call
                //see: http://androidexample.com/Thread_With_Handlers_-_Android_Example/index.php?view=article_discription&aid=58
                // and see: http://www.techotopia.com/index.php/A_Basic_Overview_of_Android_Threads_and_Thread_handlers
                // and http://stackoverflow.com/questions/13954611/android-when-should-i-use-a-handler-and-when-should-i-use-a-thread
                public void run() {
                    try {
                        URL url = new URL(media_url);
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            File f = new File(Environment.getExternalStorageDirectory(), fileReference);
                            f.createNewFile();

                            InputStream is = connection.getInputStream();
                            BufferedInputStream bufferinstream = new BufferedInputStream(is);
                            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                            //We create an array of bytes
                            byte[] data = new byte[50];
                            int current = 0;
                            while((current = bufferinstream.read(data,0,data.length)) != -1){
                                buffer.write(data,0,current);
                            }

                            FileOutputStream fos = new FileOutputStream(file);
                            fos.write(buffer.toByteArray());

                            fos.flush();
                            fos.close();
                            is.close();

                            handler.sendEmptyMessage(1);
                        }
                        else{
                            threadMessage("CONNECTION FAILED", 0);
                        }
                    } catch (Exception e) {
                        // send an error message from this thread to caller class
                        threadMessage(e.toString(), 0);
                    }
                }

                private void threadMessage(String msg, int value) {
                    Message msgObj = Message.obtain(handler, value);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", msg);
                    msgObj.setData(bundle);
                    handler.sendMessage(msgObj);
                }

                // Define the Handler that receives messages from the thread and update the progress
                private final Handler handler = new Handler() {

                    public void handleMessage(Message msg) {
                        if (msg.what == 1) {
                            // if (filetype == "VIDEO")
                                // promise.resolve(getVideoContentUri(file).toString());
                            // else
                                promise.resolve(file.toURI().toString());
                        } else {
                            Bundle bundle = msg.getData();
                            String message = bundle.getString("message");
                            promise.reject(message);
                        }
                    }
                };

            });

            // Start Thread
            background.start(); //After call start method thread called run Method
        }
    }

    @ReactMethod
    public void cleanMediaCache(Promise promise) {
        try {
            //get all files in the funnyornot folder
            String path = Environment.getExternalStorageDirectory().toString() + "/" + storageLocation;
            File f = new File(path);
            File allFiles[] = f.listFiles();

            Arrays.sort(allFiles, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    if (f1.lastModified() > f2.lastModified())
                        return +1;
                    else
                        return -1;
                }
            });

            //delete the oldest files, (till we only have the allowed max)
            //start from 1 not 0, cos 0 will be for the default .nomedia file
            for (int i = 1; i < allFiles.length - media_cache_size; i++) {
                // Log.i("DELETING:", Long.toString(allFiles[i].lastModified()));
                allFiles[i].delete();
            }

            promise.resolve("CLEANUP COMPLETE");
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    public Uri getVideoContentUri(File file) {
        String filePath = file.getAbsolutePath();
        Cursor cursor = appContext.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Video.Media._ID },
                MediaStore.Video.Media.DATA + "=? ",
                new String[] { filePath }, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/videos/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (file.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Video.Media.DATA, filePath);
                return appContext.getContentResolver().insert(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }


    @ReactMethod
    public void fileExists(final String filename, final Promise promise) {
        final String fileReference = "funnyornot/" + filename;
        final File file = new File(Environment.getExternalStorageDirectory(), fileReference);
        
        if (file.exists()) {
            promise.resolve(file.toURI().toString());
        }
        else {
            promise.resolve("DOES-NOT-EXIST");
        }

    }

    @Override
    public String getName() {
        return "MyFileManager";
    }

}