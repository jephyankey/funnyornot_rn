package com.funnyornot.videoplayerview;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.funnyornot.videoplayerview.CustomVideoView;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import android.widget.Toast;

public class VideoPlayerViewManager extends SimpleViewManager<CustomVideoView> {
    private final static String name = "CustomVideoPlayer"; //the name of the view in js
    private CustomVideoView customVideoView;

    /**{@inheritDoc}**/
    @Override
    public String getName() {
        return name;
    }

    @Override
    protected CustomVideoView createViewInstance(ThemedReactContext themedReactContext) {
        customVideoView = new CustomVideoView(themedReactContext);
        return customVideoView;
    }

    /**Set the url of the video to be loaded and played when {@link CustomVideoView#startPlayback() startPlayback()} is called
     * @param source String: The url of the video**/
    @ReactProp(name = "source")
    public void setDataUrl(CustomVideoView view, String source) throws IOException {
        if(source.startsWith("file:")){
            //file url was passed
            File file = new File(URI.create(source));
            Uri videoContentUri = getVideoContentUri(view.getContext(), file);
            if(videoContentUri != null){
                view.setVideoSource(videoContentUri);
            }
            else{
                customVideoView.onError("loadingFile", "NULL CONTENT URI");
            }
        }
        else{
            view.setVideoSource(source);
        }
    }

    public static Uri getVideoContentUri(Context context, File videoFile) {
        String filePath = videoFile.getAbsolutePath();
        Uri returnUri = null;

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Video.Media._ID },
                MediaStore.Video.Media.DATA + "=? ",
                new String[] { filePath }, null);

        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            //Uri baseUri = Uri.parse("content://media/external/video/media");
            returnUri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (videoFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Video.Media.DATA, filePath);
                returnUri = context.getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
            }
        }
        try{
            cursor.close();
        }
        catch (NullPointerException exc){}

        return returnUri;
    }

//    /**Set the URI of the video to be loaded and played when {@link CustomVideoView#startPlayback() startPlayback()} is called
//     * @param uri Uri: The uri of the video**/
    // @ReactProp(name = "URI")
    // public void setDataURI(CustomVideoView view, @NonNull Uri uri) throws IOException {
    //     view.setVideoSource(uri);
    // }

    /**Causes the video player to switch playback states
     * @param state String: The playback state to put the player in. One of PLAY, PAUSE, STOP**/
    @ReactProp(name = "state")
    public void setState(CustomVideoView view, String state) throws IOException {
        String state_ = state.toUpperCase();
        switch (state_){
            case "PLAY":
                view.startPlayback();
                break;
            case "PAUSE":
                view.pausePlayback();
                break;
            case "STOP":
                view.stopPlayback();
                break;
        }
    }

    /**Pause the playback of the video
     * @param value boolean: ignored!**/
    @ReactProp(name = "looping", defaultBoolean = true)
    public void setLooping(CustomVideoView view, boolean value) throws IOException {
        view.setVideoLooping(value);
    }

    /**Pause the playback of the video
     * @param value boolean: ignored!**/
    @ReactProp(name = "release", defaultBoolean = false)
    public void setRelease(CustomVideoView view, boolean value) throws IOException {
        if (value == true)
            view.release();
    }
}