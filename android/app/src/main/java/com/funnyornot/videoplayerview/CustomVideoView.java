package com.funnyornot.videoplayerview;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.io.IOException;

import static android.media.MediaPlayer.MEDIA_ERROR_UNKNOWN;


/**A custom video view that uses {@linkplain android.view.SurfaceView} as the video renderer
 * and handles playback with android's internal {@linkplain android.media.MediaPlayer media player}
 * More info at https://developer.android.com/reference/android/view/SurfaceView.html
 *              https://developer.android.com/guide/topics/media/mediaplayer.html
 *              https://developer.android.com/reference/android/media/MediaPlayer.html**/
public class CustomVideoView extends TextureView implements TextureView.SurfaceTextureListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener{
    final static int MEDIA_ERROR_IO = 0xfffffc14;
    final static int MEDIA_ERROR_MALFORMED = 0xfffffc11;
    final static int MEDIA_ERROR_TIMED_OUT = 0xffffff92;
    final static int MEDIA_ERROR_UNSUPPORTED = 0xfffffc0e;

    final String ON_PREPARED = "onPrepared";
    final String ON_START = "onStart";
    final String ON_STOP = "onStop";
    final String ON_PAUSE = "onPause";
    final String ON_ERROR = "onError";

    private final Object semaphore = new Object();
    private MediaPlayer mediaPlayer;  //the underlying object that does all the magic of actually playing the video
    private boolean isMediaPlayerPrepared;   //used to determine if the player is ready to play
    private boolean isSurfacePrepared;  //indicates the surface view's surface is ready to render
    private boolean loopingEnabled = false;

    public CustomVideoView(Context context){
        super(context);
        initMediaPlayer();
    }

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initMediaPlayer();
    }

    public CustomVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initMediaPlayer();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomVideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initMediaPlayer();
    }

    /**Initialize the underlying {@link android.media.MediaPlayer media player}**/
    private void initMediaPlayer(){
        this.mediaPlayer = new MediaPlayer();
//        this.mediaPlayer.setLooping(true);
        this.mediaPlayer.setOnPreparedListener(this);
        this.mediaPlayer.setScreenOnWhilePlaying(false);
        this.mediaPlayer.setOnErrorListener(this);
        this.setSurfaceTextureListener(this);
        this.mediaPlayer.setOnCompletionListener(this);
    }

    /**
     * Sets the data source for the video player from a content uri.
     * This should be the content uri of a local file.
     * This function has similar behaviour as {@link android.media.MediaPlayer#setDataSource(Context, Uri) setDataSource()} in {@linkplain android.media.MediaPlayer}
     * @param uri Uri: The Uri of the video file to be played.
     * @exception IllegalStateException If it is called in an invalid state
     * @exception IllegalArgumentException
     * @exception IOException
     * @exception SecurityException
     **/
    public void setVideoSource(Uri uri) throws IllegalStateException, IllegalArgumentException, IOException, SecurityException{
        this.mediaPlayer.setDataSource(getContext(), uri);
        this.preparePlayer();
    }

    /**
     * Sets the data source for the video player from a content uri.
     * This should be the content uri of a local file.
     * This function has similar behaviour as {@link android.media.MediaPlayer#setDataSource(Context, Uri) setDataSource()} in {@linkplain android.media.MediaPlayer}
     * @param url String: The url of the video to be played.
     * @exception IllegalStateException If it is called in an invalid state
     * @exception IllegalArgumentException
     * @exception IOException
     * @exception SecurityException
     **/
    public void setVideoSource(String url) throws IllegalStateException, IllegalArgumentException, IOException, SecurityException{
        this.mediaPlayer.setDataSource(url);
        this.preparePlayer();
    }

    public void preparePlayer(){
        this.isMediaPlayerPrepared = false;
        this.mediaPlayer.prepareAsync();
    }

    /**meda.
     * Starts or resumes playback. If playback had previously been paused, playback will continue from where it was paused.
     * If playback had been stopped, or never started before, playback will start at the beginning
     * @exception IllegalStateException If called in an invalid state
     * @exception IOException If the video data source could not be read from
     * **/
    public void startPlayback(){
        try{
            if(!mediaPlayer.isPlaying())
                this.mediaPlayer.start();
            onStart();  //raise an event that the video playback has started.
        }
        catch (IllegalStateException exc){
            onError("startPlayback", exc.getMessage()); //raise an event that an error occurred
        }
    }

    /**
     * Stops playback after playback has been stopped or paused.
     * @exception IllegalStateException If called in an invalid state**/
    public void stopPlayback(){
        try{
            this.mediaPlayer.stop();
            onStop();  //raise an event that the video playback has stopped
        }
        catch (IllegalStateException exc){
            onError("stopPlayback", exc.getMessage());
        }
    }

    /**
     * Pauses playback. Call start() to resume.
     * @exception IllegalStateException If called in an invalid state**/
    public void pausePlayback(){
        try {
            this.mediaPlayer.pause();
            onPause();  //raise an event that the video playback has paused
        }
        catch (IllegalStateException exc){
            onError("pausePlayback", exc.getMessage());
        }
    }

    /**
     * Sets the player to be looping or non-looping.
     * @param videoLooping boolean: whether to loop or not**/
    public void setVideoLooping(boolean videoLooping){
        this.loopingEnabled = videoLooping;
        //this.mediaPlayer.setLooping(videoLooping);
    }

    /**
     * Returns true if the underlying {@link android.media.MediaPlayer media player} has been prepared
     * **/
    public boolean isMediaPlayerPrepared(){
        return  this.isMediaPlayerPrepared;
    }

    /**
     * Calls {@link MediaPlayer#release() release()} on the underlying {@link android.media.MediaPlayer media player}
     * <p>
     *     <strong>Docs below copied from {@linkplain android.media.MediaPlayer#release()}</strong>
     * </p>
     * Releases resources associated with the underlying media player. It is considered good practice to call this method when you're done using the MediaPlayer.
     * In particular, whenever an Activity of an application is paused (its onPause() method is called), or stopped (its onStop() method is called),
     * this method should be invoked to release the MediaPlayer object, unless the application has a special need to keep the object around.
     * In addition to unnecessary resources (such as memory and instances of codecs) being held, failure to call this method immediately
     * if a MediaPlayer object is no longer needed may also lead to continuous battery consumption for mobile devices, and playback failure for other applications
     * if no multiple instances of the same codec are supported on a device. Even if multiple instances of the same codec are supported, some performance degradation
     * may be expected when unnecessary multiple instances are used at the same time. **/
    public void release(){
        try {
            this.mediaPlayer.stop();
        }
        catch(IllegalStateException illegalStateException){
            onError("release", illegalStateException.getMessage());
        }

        this.mediaPlayer.release();
    }

    @Override
    protected void finalize() throws Throwable {
        this.release();
        super.finalize();
    }

    /**
     * Called when the underlying {@link android.media.MediaPlayer media player} has been prepared and can now start playback
     * This occurs after a call to {@link CustomVideoView#startPlayback() startPlayback()}**/
    @Override
    public void onPrepared(MediaPlayer mp) {
        this.isMediaPlayerPrepared = true;
        synchronized (semaphore){
            if(isSurfacePrepared){
                onPrepared();   //raise an event that the video has been prepared
                startPlayback();
            }
        }
    }

    public void onPrepared() {
        WritableMap eventArgs = Arguments.createMap();
        eventArgs.putString("event", ON_PREPARED);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent( getId(), "topChange", eventArgs);
    }

    public void onPause(){
        WritableMap eventArgs = Arguments.createMap();
        eventArgs.putString("event", ON_PAUSE);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent( getId(), "topChange", eventArgs);
    }

    public void onStart(){
        WritableMap eventArgs = Arguments.createMap();
        eventArgs.putString("event", ON_START);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent( getId(), "topChange", eventArgs);
    }

    public void onStop(){
        WritableMap eventArgs = Arguments.createMap();
        eventArgs.putString("event", ON_STOP);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent( getId(), "topChange", eventArgs);
    }

    public void onError(String initiatedAction, String errorMessage){
        WritableMap eventArgs = Arguments.createMap();
        eventArgs.putString("event", ON_ERROR);
        eventArgs.putString("initiated_action", initiatedAction);
        eventArgs.putString("message", errorMessage);
        ReactContext reactContext = (ReactContext)getContext();
        reactContext.getJSModule(RCTEventEmitter.class).receiveEvent( getId(), "topChange", eventArgs);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        CustomVideoView.this.isSurfacePrepared = true;
        CustomVideoView.this.mediaPlayer.setSurface(new Surface(surface));
        synchronized (semaphore){
            if(isMediaPlayerPrepared){
                onPrepared();   //raise an event that the video has been prepared
                startPlayback();
            }
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {}

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        CustomVideoView.this.isSurfacePrepared = false;
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {}

    //implementation of MediaPlayer.OnErrorListener
    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        String action, message = "";
        action = what == MEDIA_ERROR_UNKNOWN ? "UNKNOWN ERROR" : "SERVER DIED ERROR";
        switch ((extra)) {
            case MEDIA_ERROR_IO:
                message = "MEDIA IO ERROR";
                break;
            case MEDIA_ERROR_MALFORMED:
                message = "MEDIA MALFORMED ERROR";
                break;
            case MEDIA_ERROR_TIMED_OUT:
                message = "MEDIA TIMED OUT ERROR";
                break;
            case MEDIA_ERROR_UNSUPPORTED:
                message = "MEDIA UNSUPPORTED ERROR";
                break;
            case -2147483648: //low level system error
                message = "SYSTEM MEDIA ERROR";
                break;
        }
        onError(action, message);
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        //restart playback if looping has been enabled
        if(loopingEnabled)
            this.startPlayback();
    }
}