package com.funnyornot.mysharer;
import android.util.Log;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReactContext;

import android.content.Intent;
import android.app.Activity;
import android.widget.Toast;
import android.net.Uri;
import com.facebook.react.modules.core.DeviceEventManagerModule;

//android
import android.app.Activity;
import android.content.Intent;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;



public class MySharerModule extends ReactContextBaseJavaModule implements ActivityEventListener {

    private ReactApplicationContext appContext;

    public MySharerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(this);
        appContext = reactContext;
    }

    @Override
    public void onActivityResult(Activity activity, final int requestCode, final int resultCode, final Intent intent) {
    }

    @Override
    public String getName() {
        return "MySharer";
    }

    @Override
    public void onNewIntent(Intent intent) {
        // Log.i("NEW INTENT", "NEW INTENT");
        // // Get intent, action and MIME type
        // String action = intent.getAction();
        // String type = intent.getType();

        // MySharerModule sharer = new MySharerModule(getReactApplicationContext());

        // if (Intent.ACTION_SEND.equals(action) && type != null) {
        //     if ("text/plain".equals(type)) {
        //         handleSendText(intent); // Handle text being sent
        //     } else if (type.startsWith("image/")) {
        //         handleSendImage(intent); // Handle single image being sent
        //     } else if (type.startsWith("video/")) {
        //         handleSendVideo(intent); // Handle single video being sent
        //     }
        // } 
    }

    @ReactMethod
    public void shareText(String body){
        Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            return;
        }

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

        if (sharingIntent.resolveActivity(appContext.getPackageManager()) != null)
            currentActivity.startActivity(Intent.createChooser(sharingIntent, "Share using"));
        else {
            Toast.makeText(appContext, "No app found on your phone which can perform this action", Toast.LENGTH_SHORT).show();
        }
    }

    @ReactMethod
    public void sendVideoIntent(String uri){
        Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            return;
        }

        Uri furi = Uri.parse(uri);

        Intent intent = new Intent(Intent.ACTION_VIEW, furi);
        intent.setDataAndType(furi, "video/*");

        if (intent.resolveActivity(appContext.getPackageManager()) != null)
            currentActivity.startActivity(Intent.createChooser(intent, "Play using"));
        else {
            Toast.makeText(appContext, "No app found on your phone which can play the video", Toast.LENGTH_SHORT).show();
        }
    }

    @ReactMethod
    public void sendAudioIntent(String uri){
        Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            return;
        }

        Uri furi = Uri.parse(uri);

        Intent intent = new Intent(Intent.ACTION_VIEW, furi);
        intent.setDataAndType(furi, "audio/*");

        if (intent.resolveActivity(appContext.getPackageManager()) != null)
            currentActivity.startActivity(Intent.createChooser(intent, "Play using"));
        else {
            Toast.makeText(appContext, "No app found on your phone which can play the video", Toast.LENGTH_SHORT).show();
        }
    }


    // public void handleSendText(Intent intent) {
    //     String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
    //     if (sharedText != null) {
    //         // Update UI to reflect text being shared
    //         WritableMap data = Arguments.createMap();
    //         data.putString("intentType", "image");
    //         data.putString("text", sharedText);
    //         sendEvent(appContext, "ReceivedIntent", data);
    //     }
    // }

    // public void handleSendImage(Intent intent) {
    //     Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
    //     if (imageUri != null) {
    //         WritableMap data = Arguments.createMap();
    //         data.putString("intentType", "image");
    //         data.putString("uri", imageUri.toString());
    //         sendEvent(appContext, "ReceivedIntent", data);
    //     }
    // }

    // public void handleSendVideo(Intent intent) {
    //     Uri videoUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
    //     if (videoUri != null) {
    //         WritableMap data = Arguments.createMap();
    //         data.putString("intentType", "video");
    //         data.putString("uri", videoUri.toString());
    //         sendEvent(appContext, "ReceivedIntent", data);
    //     }
    // }

    // private void sendEvent(ReactContext reactContext, String eventName, WritableMap params) {
    //     reactContext
    //         .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
    //         .emit(eventName, params);
    // }

}

 